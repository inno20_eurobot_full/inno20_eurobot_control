#include <behavior_tree/wrap_nodes/actions/flag_command.h>

BT::PortsList FlagCommand::providedPorts() {
    return {
        BT::InputPort<std::string>("goal")
    };
}

bool FlagCommand::sendGoal(GoalType& goal){
    auto value = getInput<std::string>("goal").value();

    if( value == "RAISE" ){
        goal.goal = goal.GOAL_RAISE;
        return true;
    }

    if( value == "LOWER" ){
        goal.goal = goal.GOAL_LOWER;
        return true;
    }
        
    ROS_ERROR("FlagCommand has incorrect port value '%s'", value.c_str());
    return false;
}

BT::NodeStatus FlagCommand::onResult( const ResultType& res)
{
    if (res.res == true)
        return BT::NodeStatus::SUCCESS;
    return BT::NodeStatus::FAILURE;
}

BT::NodeStatus FlagCommand::onFailedRequest(FailureCause failure)
{
    ROS_ERROR("FlagCommand request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}

void FlagCommand::halt()
{
    if( status() == BT::NodeStatus::RUNNING ) {
        ROS_WARN("FlagCommand aborted(halted)");
        BaseClass::halt();
    }
}