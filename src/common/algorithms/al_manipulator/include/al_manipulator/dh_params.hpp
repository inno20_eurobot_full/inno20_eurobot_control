#pragma once

#include <ros/ros.h>

#include <string>
#include <vector>


struct DHParam
{
    double distance_a;
    double angle_alpha;
	double distance_d;
    double angle_theta_shift;
};

class DHParams: public std::vector<DHParam>
{
public:
    static bool load_from_ROS_params(const ros::NodeHandle& nh, std::string ns, DHParams* res);
};