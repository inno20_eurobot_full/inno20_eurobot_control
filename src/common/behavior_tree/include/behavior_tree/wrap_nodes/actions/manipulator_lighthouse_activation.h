#pragma once

#include <ros/ros.h>
#include <behavior_tree/node_template/bt_action_node.h>
#include <ac_manipulator_lighthouse_activation/LighthouseActivationAction.h>

#include <string>

class LighthouseActivation : public BT::RosActionNode<ac_manipulator_lighthouse_activation::LighthouseActivationAction>
{
public:
    LighthouseActivation( ros::NodeHandle& handle, const std::string& name, const std::string& server_name, const BT::NodeConfiguration & conf)
        :RosActionNode<ac_manipulator_lighthouse_activation::LighthouseActivationAction>(handle, name, server_name, conf) {}

    static BT::PortsList providedPorts();

    bool sendGoal(GoalType& goal) override;

    BT::NodeStatus onResult( const ResultType& res) override;
    
    BT::NodeStatus onFailedRequest(FailureCause failure) override;
    
    void halt() override;
};
