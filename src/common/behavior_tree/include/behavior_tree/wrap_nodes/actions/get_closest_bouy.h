#pragma once

#include <ros/ros.h>
#include <behavior_tree/node_template/bt_condition_node.h>
#include <ac_get_closest_buoy/GetClosestBuoy.h>

#include <string>

class GetClosestBuoy: public BT::RosServiceNode<ac_get_closest_buoy::GetClosestBuoy>
{
public:
    GetClosestBuoy( ros::NodeHandle& handle, const std::string& node_name, const std::string& server_name, const BT::NodeConfiguration & conf)
        :RosServiceNode<ac_get_closest_buoy::GetClosestBuoy>(handle, node_name, server_name, conf) {}

    static BT::PortsList providedPorts();

    void sendRequest(RequestType& request) override;

    BT::NodeStatus onResponse(const ResponseType& rep) override;

    BT::NodeStatus onFailedRequest(RosServiceNode::FailureCause failure) override;
};