#! /usr/bin/env python
from __future__ import print_function
import rospy
import sys

# Brings in the SimpleActionClient
import actionlib

# goal message and the result message.
import ct_manipulator_gripper.msg


# Функция для обработки сигнала Обратной связи (Action Feadback)
def feedback_processing(fb):
    print("Server:", fb)


def example_client(name, activities):
    client = actionlib.SimpleActionClient(
        "/robot1/control/manipulator_gripper/gripper_control",
        ct_manipulator_gripper.msg.GripperControlAction,
    )

    print(name, ": wait for server")
    client.wait_for_server()  # wait until the action server has been started and is listening for goals
    print(name, ": connected")

    # creates a goal to send to the action server
    goal = ct_manipulator_gripper.msg.GripperControlGoal(activities)
    client.send_goal(goal, feedback_cb=feedback_processing)
    client.wait_for_result()  # wait for server to finish the action
    return client.get_result()  # print out the result


if __name__ == "__main__":
    name = "action_client"
    rospy.init_node(name)
    closing = (len(sys.argv) > 1) and (sys.argv[1] == "1")
    result = example_client(name, closing)
    print(name, ": result = ", result.grabbed)
