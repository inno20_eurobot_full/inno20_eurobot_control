#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Возвращает координаты бижайшего буйка, если он есть, используя данные с realsense
"""

import rospy
import math

from eurobot.core import PathResolver
from geometry_msgs.msg import PoseArray, Pose

from ac_get_closest_buoy.srv import (
    GetClosestBuoy,
    GetClosestBuoyRequest,
    GetClosestBuoyResponse,
)


class GetClosestBuoyNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        self.poseArr = PoseArray()
        resolve_path = PathResolver()

        self.buoy_topic_name = resolve_path(rospy.get_param("buoy_poses"))

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.loginfo("Main for action 'get_closest_buoy' is loaded")
        rospy.Subscriber(self.buoy_topic_name, PoseArray, self.update_poses)
        rospy.Service("command", GetClosestBuoy, self.server_callback)

    def update_poses(self, poseArr: PoseArray):
        """Update current buoy positions"""
        self.poseArr = poseArr

    def square_dist(self, pos: Pose) -> float:
        """Get square distance between orgigin and pos1"""
        pos1 = pos.position
        return math.sqrt(pos1.x ** 2 + pos1.y ** 2 + pos1.z ** 2)

    def server_callback(self, req: GetClosestBuoyRequest) -> GetClosestBuoyResponse:
        """Callback for server."""
        answer = GetClosestBuoyResponse()
        answer.done = False

        min_dist = 10  # in meters
        for b_pose in self.poseArr.poses:
            answer.done = True
            cur_dist = self.square_dist(b_pose)
            if cur_dist < min_dist:
                answer.pos.pose = b_pose
                min_dist = cur_dist

        return answer


def main():
    """Main entrypoint."""
    rospy.init_node("get_closest_buoy", anonymous=True)

    main = GetClosestBuoyNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
