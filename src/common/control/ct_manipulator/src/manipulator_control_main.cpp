#include <ct_manipulator/manipulator_p2p.h>


int main(int argc, char** argv)
{
	// initialize ros
	ros::init(argc, argv, "manipulator_control");

	if (argc <= 1)
	{
		ROS_WARN("No motors specified for manipulator_control");
		return 0;
	}

	// create list of motors
	std::stringstream ss;
	std::vector<std::string> motor_list;
	motor_list.reserve(argc-1);
	for (int i = 1; i < argc; i++)
	{
		ss << argv[i] << ' ';
		motor_list.push_back(argv[i]);
	}
	ROS_INFO("Start manipulator_control with motors: '%s'", ss.str().c_str());


	ros::NodeHandle nh;

	std::string control_type;
	ROS_ASSERT_MSG(nh.getParam("type", control_type), "Parameter 'type' not found");

	// start program
	if (control_type == "p2p")
	{
		ManipulatorP2P main;
		main.start_node(nh, motor_list);
		main.spin();
	}
	else
	{
		ROS_FATAL("Unknown control type '%s'", control_type.c_str());
		return 1;
	}

	return 0;
}