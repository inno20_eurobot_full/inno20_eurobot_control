#!/usr/bin/env python3
import rospy
import sys
import actionlib

from ac_move_base_to.msg import MoveBaseToAction, MoveBaseToGoal
from geometry_msgs.msg import Vector3, Quaternion


def _quat_from_yaw(yaw):
    from math import sin, cos
    return (0.0, 0.0, sin(yaw * 0.5), cos(yaw * 0.5))


def main():
    """Main entrypoint."""
    client = actionlib.SimpleActionClient(
        "/robot1/actions/move_base_to/command",
        MoveBaseToAction,
    )

    if len(sys.argv) < 3:
        print("requared 3 args: x, y, yaw")
        return

    x = float(sys.argv[1])
    y = float(sys.argv[2])
    angle = float(sys.argv[3])

    print("wait_for_server")
    client.wait_for_server()

    print("send_goal")
    goal = MoveBaseToGoal()
    goal.pos.header.frame_id = "map"
    goal.pos.header.stamp = rospy.Time.now()
    goal.pos.pose.position = Vector3(x, y, 0)
    goal.pos.pose.orientation = Quaternion(*_quat_from_yaw(angle))

    client.send_goal(goal)

    print("wait_for_result")
    client.wait_for_result()

    print(f"Result: {client.get_result()}")


if __name__ == "__main__":
    rospy.init_node("move_to")
    main()
