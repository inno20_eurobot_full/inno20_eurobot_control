#!/usr/bin/env python3

import rospy
from geometry_msgs.msg import Twist, Vector3
from sensor_msgs.msg import Joy


class VelsSwitch:
    """Class for control real robot base via ROS."""

    def __init__(self):
        """Create node, read parameters, subscribe for topics."""
        rospy.init_node("set_vels")

        self._last_received = rospy.get_time()
        self._timeout = rospy.get_param("~cmd_vel_timeout", 0.5)
        self._rate = rospy.get_param("~rate", 100)

        self._prev = 0

        self.sub = rospy.Subscriber("joy", Joy, self.joy_callback)
        self.pub = rospy.Publisher("cmd_vel", Twist, queue_size=1)

    def joy_callback(self, joy: Joy):
        """Callback for input commands."""
        self._last_received = rospy.get_time()

        vel = 0
        if joy.buttons[0]:
            vel += 1
        if joy.buttons[1]:
            vel += 2
        if joy.buttons[2]:
            vel += 3
        if joy.buttons[3]:
            vel += 4
        if self._prev != 0 or vel != 0:
            print(f"Sending velocity x={vel}")
            self.pub.publish(Twist(linear=Vector3(vel, 0, 0), angular=Vector3(0, 0, 0)))
            self._prev = vel

    def run(self):
        """Main loop."""
        rate = rospy.Rate(self._rate)

        while not rospy.is_shutdown():
            if rospy.get_time() - self._last_received >= self._timeout:
                self.pub.publish(Twist(linear=Vector3(0, 0, 0), angular=Vector3(0, 0, 0)))

            rate.sleep()


if __name__ == "__main__":
    vels = VelsSwitch()
    vels.run()
