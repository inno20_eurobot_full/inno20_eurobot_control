#!/usr/bin/env python3
import rospy

from ac_lighthouse_command.srv import LighthouseCommand


def main():
    """Main entrypoint."""
    client = rospy.ServiceProxy(
        "/robot1/actions/lighthouse_command/command",
        LighthouseCommand
    )

    print("send_goal")
    result = client(0)
    print(f"Result: {result}")


if __name__ == "__main__":
    rospy.init_node("refine_buoy_pos")
    main()
