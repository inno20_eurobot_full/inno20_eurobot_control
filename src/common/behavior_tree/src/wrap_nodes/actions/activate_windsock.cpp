#include <behavior_tree/wrap_nodes/actions/activate_windsock.h>

BT::PortsList ActivateWindsock::providedPorts()
{
    return {
        BT::InputPort<std::string>("side")
    };
}

bool ActivateWindsock::sendGoal(GoalType& goal)
{
    auto value = getInput<std::string>("side").value();

    if( value == "SIDE_YELLOW" ){
        goal.side = goal.SIDE_YELLOW;
        return true;
    }

    if( value == "SIDE_BLUE" ){
        goal.side = goal.SIDE_BLUE;
        return true;
    }

    ROS_ERROR("ActivateWindsock has incorrect port value '%s'", value.c_str());
    return false;
}

BT::NodeStatus ActivateWindsock::onResult( const ResultType& res)
{
    // ROS_DEBUG("ActivateWindsock onResult");
    if (res.res == true)
        return BT::NodeStatus::SUCCESS;
    return BT::NodeStatus::FAILURE;
}

BT::NodeStatus ActivateWindsock::onFailedRequest(FailureCause failure)
{
    ROS_ERROR("ActivateWindsock request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}

void ActivateWindsock::halt()
{
    if( status() == BT::NodeStatus::RUNNING ) {
        ROS_WARN("ActivateWindsock aborted(halted)");
        BaseClass::halt();
    }
}