#!/usr/bin/env python3

import rospy
from al_manipulator.srv import ManipulatorFK, ManipulatorIK
from sensor_msgs.msg import JointState
import random
import math
import numpy as np


MOTOR_NAMES = ["motor0", "motor1", "motor2", "motor3", "motor4"]


def main():
    """Main entrypoint."""
    rospy.init_node("test_ik", anonymous=True)

    rospy.wait_for_service("/robot1/algorithms/manipulator_fk/calc_fk")
    rospy.wait_for_service("/robot1/algorithms/manipulator_ik/calc_ik")

    calc_fk = rospy.ServiceProxy(
        "/robot1/algorithms/manipulator_fk/calc_fk", ManipulatorFK
    )
    calc_ik = rospy.ServiceProxy(
        "/robot1/algorithms/manipulator_ik/calc_ik", ManipulatorIK
    )

    for i in range(10000):
        q = [random.random() * math.pi * 2 - math.pi for i in range(5)]
        # q = [0.2, -0.1, 0.8, 0.2, -1.14]
        # q = [0.0, 0.0, 2.2, 0.0, 1.3]
        # q = [0.39636493939733297, 1.6447427648891644, -3.139400974357827, 0.46849762440179754, 2.072830768136469]
        H = calc_fk(JointState(name=MOTOR_NAMES, position=q)).res

        qs = [i.position for i in calc_ik(H).res]
        assert len(qs) in [2, 4], "Количество решений не 2 и не 4"
        assert (
            (np.array(qs) - np.array(q)).max(axis=-1) < 1e-6
        ).any(), ("Исходной позиции нет в IK: %.12f" % (np.array(qs) - np.array(q)).max(axis=-1))

        for qsi in qs:
            cH = calc_fk(JointState(name=MOTOR_NAMES, position=qsi)).res
            assert (np.array(cH.data) - np.array(H.data) < 1e-10).all()


if __name__ == "__main__":
    main()
