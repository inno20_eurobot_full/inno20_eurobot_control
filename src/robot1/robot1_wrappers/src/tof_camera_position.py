#!/usr/bin/env python3

"""Модуль рассчитывает позиию realsence."""

import rospy
import math
import tf

from eurobot.core import PathResolver
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Vector3, Quaternion, TransformStamped


class TOF_CameraPosition:
    """Main class."""

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self.tof_camera_pos = rospy.get_param(
            self.resolve_path("~/description/general/tof_camera_pos")
        )
        self.motor0_height = rospy.get_param(
            self.resolve_path("~/description/general/manipulator_base_height")
        )

        self.motor0_name = rospy.get_param("motor0_name")
        self.motor0_topic = self.resolve_path(rospy.get_param("motor0_topic"))

        self.output_topic = rospy.Publisher("position", TransformStamped, queue_size=1)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.Subscriber(self.motor0_topic, JointState, self.joint_state_callback)

    def process_camera_position(self, motor0_position):
        """Вызывется каждый раз при получении позиции мотора."""
        transform = TransformStamped()

        """ rotation matrix """
        """    cos -sin     """
        """    sin  cos     """
        start_pos = Vector3(self.tof_camera_pos['x'], self.tof_camera_pos['y'], self.tof_camera_pos['z'])

        newX = start_pos.x * math.cos(motor0_position) - start_pos.y * math.sin(motor0_position)
        newY = start_pos.x * math.sin(motor0_position) + start_pos.y * math.cos(motor0_position)
        newZ = start_pos.z + self.motor0_height

        transform.transform.translation = Vector3(newX, newY, newZ)

        rotX = self.tof_camera_pos['rotx']
        rotY = self.tof_camera_pos['roty']
        rotZ = self.tof_camera_pos['rotz'] + motor0_position

        """changes the coordinate axes because the realsense has a different coordinate system."""
        rotX -= math.pi / 2
        rotY -= 0
        rotZ -= math.pi / 2

        quat = tf.transformations.quaternion_from_euler(rotX, rotY, rotZ)
        transform.transform.rotation = Quaternion(quat[0], quat[1], quat[2], quat[3])

        self.output_topic.publish(transform)
        pass

    def joint_state_callback(self, msg: JointState):
        """Callback for control topic."""
        N = len(msg.name)
        if len(msg.velocity) != N or len(msg.position) != N:
            rospy.logwarn("Incorrect control message. Ignoring")
            return

        if self.motor0_name not in msg.name:
            return

        i = msg.name.index(self.motor0_name)
        self.process_camera_position(msg.position[i])


def main():
    """Main entrypoint."""
    rospy.init_node("tof_camera_position", anonymous=True)

    main = TOF_CameraPosition()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
