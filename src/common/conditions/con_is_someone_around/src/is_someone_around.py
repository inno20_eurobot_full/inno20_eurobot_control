#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Есть ли вражеский робот рядом?
"""

import rospy
import math

import tf2_ros

from con_is_someone_around.srv import (
    IsSomeoneAround,
    IsSomeoneAroundRequest,
    IsSomeoneAroundResponse,
)


class IsSomeoneAroundNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        self.robot_no = int(rospy.get_param("aruco_no"))
        self.robot_frame = rospy.get_param("robot_frame")
        self.other_frames = rospy.get_param("other_frames")

        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.loginfo("Main for condition 'is_someone_around' is loaded")
        self._server = rospy.Service("command", IsSomeoneAround, self.server_callback)

    def server_callback(self, req: IsSomeoneAroundRequest) -> IsSomeoneAroundResponse:
        """Callback for server."""
        for i in range(0, 10):
            if i + 1 == self.robot_no:
                continue
            other_frame = self.other_frames[i]

            try:
                trans = self.tfBuffer.lookup_transform(
                    self.robot_frame, other_frame, rospy.Time(0)  # , rospy.Duration(1)
                )
            except (
                tf2_ros.LookupException,
                tf2_ros.ConnectivityException,
                tf2_ros.ExtrapolationException,
            ):
                continue
            rospy.logwarn("good")
            x = trans.transform.translation.x
            y = trans.transform.translation.y
            distance = math.sqrt(x ** 2 + y ** 2)
            rospy.logwarn(
                f" our aruco:{self.robot_no} frame:{self.robot_frame}, other aruco:{i+1} "
                f"frame:{other_frame}, distance:{distance} min_dist:{req.dist}"
            )
            if distance < req.dist and trans.header.stamp > (
                rospy.Time.now() - rospy.Duration(1)
            ):
                return IsSomeoneAroundResponse(True)
        rospy.logwarn("bad")
        return IsSomeoneAroundResponse(False)


def main():
    """Main entrypoint."""
    rospy.init_node("is_someone_around", anonymous=True)

    main = IsSomeoneAroundNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
