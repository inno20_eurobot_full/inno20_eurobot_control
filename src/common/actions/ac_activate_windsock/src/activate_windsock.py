#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Активировать windsock (повернуть манипулятором так, чтобы повернуть windsock)
"""

import rospy
from eurobot.core import PathResolver

import actionlib
import ac_activate_windsock.msg
from sensor_msgs.msg import JointState

# goal message and the result message.
from ct_manipulator.msg import ManipulatorP2PGoal, ManipulatorP2PAction


class ActivateWindsock:
    """Wrapper for action."""
    Action = ac_activate_windsock.msg.ActivateWindsockAction
    Feedback = ac_activate_windsock.msg.ActivateWindsockFeedback
    Goal = ac_activate_windsock.msg.ActivateWindsockGoal
    Result = ac_activate_windsock.msg.ActivateWindsockResult


class ActivateWindsockNode:
    """Main class."""

    _result = ActivateWindsock.Result()

    def __init__(self):
        """Create main class."""
        resolve_path = PathResolver()

        p2p_name = resolve_path(rospy.get_param("p2p_action"))
        motor_base = rospy.get_param("motor_base")
        motor_names = rospy.get_param("motor_names")
        motor_manip = rospy.get_param("motor_manip")
        right_pos = rospy.get_param("right_pos")
        left_pos = rospy.get_param("left_pos")
        finish_pos = rospy.get_param("finish_pos")

        self.state_left_up = JointState(name=motor_base, position=left_pos)
        self.state_right_up = JointState(name=motor_base, position=right_pos)
        self.state_right_down = JointState(name=motor_names, position=right_pos)
        self.state_left_down = JointState(name=motor_names, position=left_pos)
        self.state_final_up = JointState(name=motor_manip, position=finish_pos)

        self.call_p2p = actionlib.SimpleActionClient(p2p_name, ManipulatorP2PAction)

        self._action_server = actionlib.SimpleActionServer(
            "command",
            ActivateWindsock.Action,
            execute_cb=self.action_callback,
            auto_start=False)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.loginfo("Main for action 'activate_windsock' is loaded")
        self._action_server.start()

    def feedback_processing(self, fb):
        """Processing the feedback from actions"""
        # print("Server feedback:", fb)
        pass

    def action_callback(self, goal: ActivateWindsock.Goal):
        """Callback for action."""
        self._result.res = False
        while True:
            if goal.side == goal.SIDE_YELLOW:
                first_act = self.state_left_up
                second_act = self.state_left_down
                third_act = self.state_right_down
            elif goal.side == goal.SIDE_BLUE:
                first_act = self.state_right_up
                second_act = self.state_right_down
                third_act = self.state_left_down
            else:
                rospy.logwarn("activate_windsock: incorrect command")
                break

            # only rotate base
            self.call_p2p.send_goal(
                ManipulatorP2PGoal(first_act),
                feedback_cb=self.feedback_processing
            )
            self.call_p2p.wait_for_result()
            if not self.call_p2p.get_result().done:
                break

            # lower manipulator
            self.call_p2p.send_goal(
                ManipulatorP2PGoal(second_act),
                feedback_cb=self.feedback_processing
            )
            self.call_p2p.wait_for_result()
            if not self.call_p2p.get_result().done:
                break

            # only rotate base
            self.call_p2p.send_goal(
                ManipulatorP2PGoal(third_act),
                feedback_cb=self.feedback_processing
            )
            self.call_p2p.wait_for_result()
            if not self.call_p2p.get_result().done:
                break

            # raise manipulator
            self.call_p2p.send_goal(
                ManipulatorP2PGoal(self.state_final_up),
                feedback_cb=self.feedback_processing
            )
            self.call_p2p.wait_for_result()
            if not self.call_p2p.get_result().done:
                break

            self._result.res = True
            break
        self._action_server.set_succeeded(self._result)


def main():
    """Main entrypoint."""
    rospy.init_node("activate_windsock", anonymous=True)

    main = ActivateWindsockNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
