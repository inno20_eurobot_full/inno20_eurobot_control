#!/usr/bin/env python3
import rospy

from geometry_msgs.msg import TransformStamped
import tf2_ros
import tf


def main():
    """Main entrypoint."""
    broadcaster = tf2_ros.StaticTransformBroadcaster()
    static_transformStamped = TransformStamped()

    static_transformStamped.header.stamp = rospy.Time.now()
    static_transformStamped.header.frame_id = "map"
    static_transformStamped.child_frame_id = "base_link"

    static_transformStamped.transform.translation.x = 1
    static_transformStamped.transform.translation.y = 1.5
    static_transformStamped.transform.translation.z = 0

    quat = tf.transformations.quaternion_from_euler(0, 0, 0)
    static_transformStamped.transform.rotation.x = quat[0]
    static_transformStamped.transform.rotation.y = quat[1]
    static_transformStamped.transform.rotation.z = quat[2]
    static_transformStamped.transform.rotation.w = quat[3]

    broadcaster.sendTransform(static_transformStamped)
    rospy.spin()


if __name__ == "__main__":
    rospy.init_node("mock_robot_transform")
    main()
