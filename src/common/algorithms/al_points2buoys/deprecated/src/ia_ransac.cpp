#include <al_points2buoys/ia_ransac.hpp>

vectorPCLpointsPtr IA_RANSAC::splitPointCloud(const PCLPoints::Ptr points) const{
    vectorPCLpointsPtr pointArr;
    /* TODO 
        Нужно разделить облако точек на предворительные части, в которых есть стаканчики.
    */
    if(points->size() > 10)
        pointArr.push_back(points);
    return pointArr;
}

geometry_msgs::PoseArray::Ptr IA_RANSAC::calculatePoses(const vectorPCLpointsPtr& pointsArr) const{
    geometry_msgs::PoseArray::Ptr poses(new geometry_msgs::PoseArray);
    for(const auto& points_ :pointsArr){
        FeatureCloud target(points_);
        FeatureCloud cloud(buoy_cloud);

        pcl::SampleConsensusInitialAlignment<PointT, PointT, pcl::FPFHSignature33> sac_ia_;
        sac_ia_.setInputTarget (target.points);
        sac_ia_.setTargetFeatures (target.features);
        sac_ia_.setInputSource (cloud.points);
        sac_ia_.setSourceFeatures (cloud.features);
        sac_ia_.setMinSampleDistance (0.05f);
        sac_ia_.setMaxCorrespondenceDistance (0.02f*0.02f);
        sac_ia_.setMaximumIterations (600);

        PCLPoints registration_output;
        sac_ia_.align (registration_output);
        
        float score = sac_ia_.getFitnessScore(0.01f*0.01f);
        Eigen::Matrix4f transform = sac_ia_.getFinalTransformation();

         // Print the alignment fitness score (values less than 0.00002 are good)
        ROS_INFO("Best fitness score: %f\n", score);

        // Print the rotation matrix and translation vector
        Eigen::Matrix3f rotation = transform.block<3,3>(0, 0);
        Eigen::Vector3f translation = transform.block<3,1>(0, 3);
        ROS_INFO("    | %6.3f %6.3f %6.3f |", rotation (0,0), rotation (0,1), rotation (0,2));
        ROS_INFO("R = | %6.3f %6.3f %6.3f |", rotation (1,0), rotation (1,1), rotation (1,2));
        ROS_INFO("    | %6.3f %6.3f %6.3f |", rotation (2,0), rotation (2,1), rotation (2,2));
        ROS_INFO("t = < %0.3f, %0.3f, %0.3f >", translation (0), translation (1), translation (2));

        PCLPoints bouy_trans;
        pcl::transformPointCloud (*buoy_cloud, bouy_trans, transform);

        // publish rotated bouy
        sensor_msgs::PointCloud2 OUT_PC2;
        pcl::toROSMsg(bouy_trans,OUT_PC2);
        buoy_cloud_pub.publish(OUT_PC2);

        break;
    }
    return poses;
}

geometry_msgs::PoseArray::Ptr IA_RANSAC::findBouy(const PCLPoints::Ptr pointCloud){
    PCLPoints::Ptr points = removeFarPoints(pointCloud);
    PCLPoints::Ptr sparse_points = downSampling(points);
    vectorPCLpointsPtr pointsArr = splitPointCloud(sparse_points);
    return calculatePoses(pointsArr);
}

FeatureCloud::FeatureCloud(PCLPoints::Ptr points_){
    points = points_;
    normals = SurfaceNormals::Ptr (new SurfaceNormals);
    features = LocalFeatures::Ptr (new LocalFeatures);

    SearchMethod::Ptr search_method(new SearchMethod);

    pcl::NormalEstimation<PointT, pcl::Normal> norm_est;
    norm_est.setInputCloud(points);
    norm_est.setSearchMethod(search_method);
    norm_est.setRadiusSearch(0.02f);
    norm_est.compute(*normals);

    pcl::FPFHEstimation<PointT, pcl::Normal, pcl::FPFHSignature33> fpfh_est;
    fpfh_est.setInputCloud (points);
    fpfh_est.setInputNormals (normals);
    fpfh_est.setSearchMethod (search_method);
    fpfh_est.setRadiusSearch (0.02f);
    fpfh_est.compute (*features);
}