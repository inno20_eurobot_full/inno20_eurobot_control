#!/usr/bin/env python3

"""Модуль для работы с GPIO"""

import rospy

from math import pi

from std_msgs.msg import Float32

from dr_jetson_gpio.srv import IsButtonActivated, IsButtonActivatedRequest, IsButtonActivatedResponse

import Jetson.GPIO as GPIO


class JetsonGPIO:
    """Main class."""

    def __init__(self):
        """Create main class."""
        GPIO.setmode(GPIO.BOARD)
        self.button_port = rospy.get_param("button_port")
        self.motor_port = rospy.get_param("motor_port")
        self.pwm_freq = rospy.get_param("pwm_freq")
        self.motor_min = rospy.get_param("motor_min")
        self.motor_max = rospy.get_param("motor_max")

        GPIO.setup(self.button_port, GPIO.IN)
        GPIO.setup(self.motor_port, GPIO.OUT, initial=GPIO.HIGH)

        self.motor_pwm = GPIO.PWM(self.motor_port, self.pwm_freq)

    def close(self):
        """Close GPIO"""
        self.motor_pwm.stop()
        GPIO.cleanup()

    def start(self):
        """Subscribe to all neaded topics."""
        self.motor_pwm.start(self.motor_min)
        rospy.Subscriber("flag_command", Float32, self.flag_callback)
        self._server = rospy.Service("get_state", IsButtonActivated, self.button_callback)

    def flag_callback(self, msg: Float32):
        """Управление флагом: на вход приходит угол мотора от 0 до pi/2"""
        pi_2 = pi / 2
        if msg.data < 0:
            msg.data = 0
        if msg.data > pi_2:
            msg.data = pi_2
        value = msg.data / pi_2 * (self.motor_max - self.motor_min) + self.motor_min
        rospy.logdebug(f"getMSG: {msg}, value: {value}")
        self.motor_pwm.ChangeDutyCycle(value)

    def button_callback(self, msg: IsButtonActivatedRequest) -> IsButtonActivatedResponse:
        """Srvice callback"""
        if GPIO.input(self.button_port) is GPIO.HIGH:
            return IsButtonActivatedResponse(True)
        return IsButtonActivatedResponse(False)

    def spin(self):
        """Spin"""
        rospy.spin()


def main():
    """Main entrypoint."""
    rospy.init_node("jetson_gpio", anonymous=True)

    main = JetsonGPIO()
    main.start()
    main.spin()
    main.close()


if __name__ == "__main__":
    main()
