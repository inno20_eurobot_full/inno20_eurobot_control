#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Подготовить манипулятор к захвату буйка или его установки (пододвинуть манипулятор так,
чтобы он смог захватить буй или поставить его)
"""

import rospy
from eurobot.core import PathResolver

import actionlib
import ac_prepare_manipulator_for_take_buoy.msg

# goal message and the result message.
from ct_manipulator.msg import ManipulatorP2PGoal, ManipulatorP2PAction
from al_manipulator.srv import ManipulatorAdvancedIK
from al_manipulator.msg import ManipulatorEndPos


class PrepareManipulatorForTakeBuoy:
    """Wrapper for action."""
    Action = ac_prepare_manipulator_for_take_buoy.msg.PrepareManipulatorForTakeBuoyAction
    Feedback = ac_prepare_manipulator_for_take_buoy.msg.PrepareManipulatorForTakeBuoyFeedback
    Goal = ac_prepare_manipulator_for_take_buoy.msg.PrepareManipulatorForTakeBuoyGoal
    Result = ac_prepare_manipulator_for_take_buoy.msg.PrepareManipulatorForTakeBuoyResult


class PrepareManipulatorForTakeBuoyNode:
    """Main class."""

    _result = PrepareManipulatorForTakeBuoy.Result()

    def __init__(self):
        """Create main class."""
        resolve_path = PathResolver()

        advik_name = resolve_path(rospy.get_param("advik_service"))
        p2p_name = resolve_path(rospy.get_param("p2p_action"))
        self.height_high = float(rospy.get_param("level_over_the_buoy"))
        self.height_low = float(rospy.get_param("buoy_level"))
        self.pitch = float(rospy.get_param("pitch_angle"))

        self.calc_advik = rospy.ServiceProxy(advik_name, ManipulatorAdvancedIK)
        self.call_p2p = actionlib.SimpleActionClient(p2p_name, ManipulatorP2PAction)

        self._action_server = actionlib.SimpleActionServer(
            "command",
            PrepareManipulatorForTakeBuoy.Action,
            execute_cb=self.action_callback,
            auto_start=False)

        self.calc_advik.wait_for_service()

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.loginfo("Main for action 'prepare_manipulator_for_take_buoy' is loaded")
        self._action_server.start()

    def feedback_processing(self, fb):
        """Processing the feedback from actions"""
        # print("Server feedback:", fb)
        pass

    def action_callback(self, goal: PrepareManipulatorForTakeBuoy.Goal):
        """Moves to the buoy position"""
        self._result.res = False
        while True:
            # ---move into position over the buoy---
            rospy.loginfo("move into position over the buoy")

            b_pos = goal.buoy_pos.pose.position
            b_pos.z = self.height_high  # level over the buoy

            states = self.calc_advik(ManipulatorEndPos(x=b_pos.x, y=b_pos.y, z=b_pos.z, roll=0, pitch=self.pitch)).res
            if not states:
                rospy.logwarn("Error: position not reachable")
                break

            self.call_p2p.send_goal(
                ManipulatorP2PGoal(states[0]), feedback_cb=self.feedback_processing
            )
            self.call_p2p.wait_for_result()  # wait for p2p to finish the action
            if self.call_p2p.get_result() is False:
                rospy.logwarn("Error: p2p could not get to the point")
                break
            rospy.sleep(.3)

            # ---go down to the buoy---
            rospy.loginfo("go down to the buoy")

            b_pos.z = self.height_low
            states = self.calc_advik(ManipulatorEndPos(x=b_pos.x, y=b_pos.y, z=b_pos.z, roll=0, pitch=self.pitch)).res
            if not states:
                rospy.logwarn("Error: position not reachable")
                break

            self.call_p2p.send_goal(
                ManipulatorP2PGoal(states[0]), feedback_cb=self.feedback_processing
            )
            self.call_p2p.wait_for_result()  # wait for p2p to finish the action
            if self.call_p2p.get_result() is False:
                rospy.logwarn("Error: p2p could not get to the point")
                break

            # --- finish ---
            rospy.loginfo("finish")
            self._result.res = True
            break
        # After completion we inform the client about the end of the work
        self._action_server.set_succeeded(self._result)


def main():
    """Main entrypoint."""
    rospy.init_node("prepare_manipulator_for_take_buoy", anonymous=True)

    main = PrepareManipulatorForTakeBuoyNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
