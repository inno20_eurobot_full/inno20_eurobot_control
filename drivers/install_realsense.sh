#!/bin/bash

if [ -d /etc/udev/rules.d ]; then
    sudo cp 60-librealsense2-udev-rules.rules /etc/udev/rules.d/
    echo "Complete"
else
    echo "Automatic installation is not possible"
fi

