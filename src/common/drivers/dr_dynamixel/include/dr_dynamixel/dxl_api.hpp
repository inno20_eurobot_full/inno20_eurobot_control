#pragma once

#define DXL_TX_ERR(err, packetHandler, func, ...) { \
    int dxl_comm_result = packetHandler->func(__VA_ARGS__); \
    if (dxl_comm_result != COMM_SUCCESS) err = packetHandler->getTxRxResult(dxl_comm_result); \
}

#define DXL_TXRX_ERR(err, packetHandler, func, ...) { \
    uint8_t dxl_error; \
    int dxl_comm_result = packetHandler->func(__VA_ARGS__, &dxl_error); \
    if (dxl_comm_result != COMM_SUCCESS) err = packetHandler->getTxRxResult(dxl_comm_result); \
    else if (dxl_error != 0) err = packetHandler->getRxPacketError(dxl_error); \
}

#define MX28_MODE_VEL_CONTROL 1
#define MX28_MODE_PWM_CONTROL 16

#define MX28_VEL_CLAMP 1023
#define MX28_PWM_CLAMP 885

#define MX28_PTS_TO_POS 0.001534355386
#define MX28_PTS_TO_VEL 0.02398113102
#define MX28_PTS_TO_PWM 0.001129943503

#define MX28_OPERATING_MODE 11
#define MX28_TORQUE_ENABLE 64
#define MX28_HARDWARE_ERROR_STATUS 70
#define MX28_GOAL_PWM 100
#define MX28_GOAL_VELOCITY 104
#define MX28_PRESENT_VELOCITY 128
#define MX28_PRESENT_POSITION 132