#!/bin/bash

set -e

realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

DOCKER_IMAGE="markovvn1/ros_pipeline_eurobot2021"
HOME_PATH="$(dirname "$(realpath "$0")")"


docker pull $DOCKER_IMAGE || echo -e "\e[0;33mWarning: failed to update docker image\e[0m"
if [ "$1" == "full" ]; then
	args="${@:2}"
	docker run --rm -it -e CONTINUE_ON_STAGE_FAIL=true -e CATKIN_BUILD_ARGS="$args" -v "$HOME_PATH/src:/root/catkin_ws/src/src:ro" $DOCKER_IMAGE
else
	args="${@:1}"
	docker run --rm -it -e CATKIN_BUILD_ARGS="$args" -v "$HOME_PATH/src:/root/catkin_ws/src/src:ro" $DOCKER_IMAGE
fi