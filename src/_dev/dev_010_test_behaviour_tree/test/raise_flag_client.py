#! /usr/bin/env python
from __future__ import print_function
import rospy
import sys

# Brings in the SimpleActionClient
import actionlib

# goal message and the result message.
import robot1_actions.msg


# Функция для обработки сигнала Обратной связи (Action Feadback)
def feedback_processing(fb):
    print("Server:", fb)


def example_client(activities):
    client = actionlib.SimpleActionClient(
        "/robot1/actions/raise_flag/raise_flag", robot1_actions.msg.RaiseFlagAction
    )

    print("wait")
    client.wait_for_server()  # wait until the action server has been started and is listening for goals
    print("connected")

    # creates a goal to send to the action server
    do_it = (
        True
        if (activities == "1" or activities == "T" or activities == "True")
        else False
    )
    goal = robot1_actions.msg.RaiseFlagGoal(do_it)
    client.send_goal(goal, feedback_cb=feedback_processing)
    client.wait_for_result()  # wait for server to finish the action
    return client.get_result()  # print out the result


if __name__ == "__main__":
    rospy.init_node("raise_flag_client")
    activities = sys.argv[1] if (len(sys.argv) > 1) else False
    result = example_client(activities)
    print("Result:", result.done)
