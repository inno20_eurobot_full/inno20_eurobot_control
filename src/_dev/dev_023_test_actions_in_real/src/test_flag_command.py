#!/usr/bin/env python3
import rospy
import sys
import actionlib

from ac_flag_command.msg import FlagCommandAction, FlagCommandGoal


def main():
    """Main entrypoint."""
    client = actionlib.SimpleActionClient(
        "/robot1/actions/flag_command/command",
        FlagCommandAction,
    )

    print("wait_for_server")
    client.wait_for_server()

    print("send_goal")
    goal = FlagCommandGoal()
    goal.goal = int(sys.argv[1]) if len(sys.argv) > 1 else 0
    client.send_goal(goal)

    print("wait_for_result")
    client.wait_for_result()

    print(f"Result: {client.get_result()}")


if __name__ == "__main__":
    rospy.init_node("prepare_manipulator_for_drive")
    main()
