# world_description

В пакете храняться миры для проекта Eurobot2021

## World zoo

### empty

Пустое поле с настроенной освещенностю

![eurobot_2020_field](.gitlab/empty.png)

### field_only

Модель статической части поля

![eurobot_2020_buoy](.gitlab/field_only.png)

### field_with_buoys

Модель статической части поля c расположенными на нем буйками.

![eurobot_2020_buoy](.gitlab/field_with_buoys.png)