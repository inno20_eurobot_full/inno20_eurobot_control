#include <wp_joint_state_selector/joint_state_wrapper.hpp>

/*
Принцип работы программы.
Она подписывается на указанные топики. При получении сообщений данные сохраняюся и
контроллеры помечаютя как новые. Как только хотя бы PUSH_WHEN процентов контроллеров
будет обновлена, сообщение отправляется в топик
*/


int main(int argc, char** argv)
{
	// initialize ros
	ros::init(argc, argv, "joint_state_selector");

	if (argc <= 1)
	{
		ROS_WARN("No controllers specified for joint_state_selector");
		return 0;
	}

	std::stringstream ss;
	for (ssize_t i = 1; i < argc; i++)
		ss << argv[i] << ' ';
	ROS_INFO("Start joint_state_selector with controllers: '%s'", ss.str().c_str());

	// create list of controllers
	std::vector<std::string> controller_names;
	controller_names.reserve(argc-1);
	for (int i = 1; i < argc; i++)
		controller_names.push_back(argv[i]);


    ros::NodeHandle nh;

	// start program
    JointStateWrapper main;
    main.start_node(nh, controller_names);
    main.spin();

	return 0;
}