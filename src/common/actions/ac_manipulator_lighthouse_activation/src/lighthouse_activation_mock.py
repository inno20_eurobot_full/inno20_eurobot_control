#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Коснуться маяка для его активации
"""

import rospy
from eurobot.core import PathResolver

import actionlib
import ac_manipulator_lighthouse_activation.msg
import time


class LighthouseActivation:
    """Wrapper for action."""
    Action = ac_manipulator_lighthouse_activation.msg.LighthouseActivationAction
    Feedback = ac_manipulator_lighthouse_activation.msg.LighthouseActivationFeedback
    Goal = ac_manipulator_lighthouse_activation.msg.LighthouseActivationGoal
    Result = ac_manipulator_lighthouse_activation.msg.LighthouseActivationResult


class LighthouseActivationNode:
    """Main class."""

    _result = LighthouseActivation.Result()

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self._action_server = actionlib.SimpleActionServer(
            "command",
            LighthouseActivation.Action,
            execute_cb=self.action_callback,
            auto_start=False)
        self._action_server.start()

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.logwarn("Mock for action 'manipulator_lighthouse_activation' is loaded")

    def action_callback(self, goal: LighthouseActivation.Goal):
        """Callback for action."""
        time.sleep(4)
        self._result.res = True
        self._action_server.set_succeeded(self._result)


def main():
    """Main entrypoint."""
    rospy.init_node("manipulator_lighthouse_activation", anonymous=True)

    main = LighthouseActivationNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
