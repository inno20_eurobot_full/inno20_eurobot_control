#!/usr/bin/env python3

"""Модуль перессылки сообщения на маяк"""

from dr_lighthouse.srv import Command, CommandRequest, CommandResponse
import rospy

import requests


class Lighthouse_sender:
    """Main class."""

    def __init__(self):
        """Create main class."""
        self.ip = rospy.get_param("ip")
        self.port = rospy.get_param("port")

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.Service('command', Command, self.command_callback)

    def command_callback(self, msg: CommandRequest) -> CommandResponse:
        """Callback for topic."""
        try:
            r = requests.post(f"http://{self.ip}:{self.port}/task?command={msg.command}&use_motor={msg.use_motor}")
            r.raise_for_status()
            return CommandResponse(True)
        except requests.exceptions.HTTPError as err:
            rospy.logerr(f"Lighthouse driver: HTTPError:{err}")
        except requests.exceptions.Timeout:
            rospy.logerr("Lighthouse driver: request timeout")
        except requests.exceptions.TooManyRedirects:
            rospy.logerr("Lighthouse driver: bad URL")
        except requests.exceptions.RequestException as e:
            rospy.logerr(f"Lighthouse driver: error:{e}")

        return CommandResponse(False)


def main():
    """Main entrypoint."""
    rospy.init_node("lighthouse", anonymous=True)

    main = Lighthouse_sender()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
