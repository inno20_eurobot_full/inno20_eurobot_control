#pragma once

#include <ros/ros.h>
#include <behavior_tree/node_template/bt_condition_node.h>
#include <con_is_buoy_in_gripper/IsBuoyInGripper.h>

#include <string>

class IsBuoyInGripper: public BT::RosServiceNode<con_is_buoy_in_gripper::IsBuoyInGripper>
{
public:
    IsBuoyInGripper( ros::NodeHandle& handle, const std::string& node_name, const std::string& server_name, const BT::NodeConfiguration & conf)
        :RosServiceNode<con_is_buoy_in_gripper::IsBuoyInGripper>(handle, node_name, server_name, conf) {}

    static BT::PortsList providedPorts();

    void sendRequest(RequestType& request) override;

    BT::NodeStatus onResponse(const ResponseType& rep) override;

    BT::NodeStatus onFailedRequest(RosServiceNode::FailureCause failure) override;
};
