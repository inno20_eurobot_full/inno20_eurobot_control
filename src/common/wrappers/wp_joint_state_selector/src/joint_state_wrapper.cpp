#include <wp_joint_state_selector/joint_state_wrapper.hpp>

#include <functional>

void JointStateWrapper::start_node(ros::NodeHandle& nh, const std::vector<std::string>& controller_names)
{
	// get home_path param
	resolve_path.init(nh);
	nh.param("push_when", PUSH_WHEN, PUSH_WHEN);

	// initialize all controllers
	for (const std::string& name : controller_names) {

		const std::vector<std::string> params  = {"/input_name", "/input_topic", "/output_topic","/gear_mult"};

		bool flag = false;
		for (const auto& param: params)
			if (!nh.hasParam(name + param))
			{
				ROS_ERROR("Parameter '%s' for controller '%s' not found. Skiping controller", param.c_str(), name.c_str());
				flag = true; break;
			}
		if (flag) continue;

		MotorParam motor;
		nh.getParam(name + params[0], motor.input_name);
		nh.getParam(name + params[1], motor.input_topic);
		nh.getParam(name + params[2], motor.output_topic);
		nh.getParam(name + params[3], motor.gear_mult);
		nh.param(name + "/offset", motor.offset, 0.);
		motor.input_topic = resolve_path(motor.input_topic);
		motor.output_topic= resolve_path(motor.output_topic);
        motor.output_name = name;

		ROS_DEBUG("Controller '%s' will be loaded from topic '%s' as '%s' and publish to topic '%s' as '%s'", 
			name.c_str(), motor.input_topic.c_str(), motor.input_name.c_str(), motor.output_topic.c_str(), motor.output_name.c_str());

		// add new topics to dictionary
        subscribers[motor.input_topic];
        interdependence[motor.input_topic];

        publishers[motor.output_topic];
        packeges[motor.output_topic];

        // add new interdependence
        if (interdependence[motor.input_topic].count(motor.input_name))
        {
            ROS_ERROR("Duplicate controllers in input topic '%s' with name '%s'. Ignoring", motor.input_topic.c_str(), motor.input_name.c_str());
            continue;
        }   
        interdependence[motor.input_topic].emplace(motor.input_name, motor);

        // prepare for package
        if (packeges[motor.output_topic].count(motor.output_name))
        {
            ROS_ERROR("Duplicate controllers in output topic '%s' with name '%s'. Ignoring", motor.input_topic.c_str(), motor.input_name.c_str());
            continue;
        }   
        packeges[motor.output_topic][motor.output_name];
	}

	// Create subscribers
	for (auto & [topic_path, sub] : subscribers)
	{
		ROS_DEBUG("Subscribe to topic '%s'", topic_path.c_str());
		using CallbackFunc = boost::function<void(const sensor_msgs::JointState::ConstPtr&)>;
		CallbackFunc callback = boost::bind(&JointStateWrapper::callback, this, topic_path, boost::placeholders::_1);
		sub = nh.subscribe(topic_path, 1, callback) ;
	}
    // Create publishers
	for (auto & [topic_path, pub] : publishers)
	{
		ROS_DEBUG("Publish to topic '%s'", topic_path.c_str());
        pub = nh.advertise<sensor_msgs::JointState>(topic_path.c_str(), 1);
	}
}

void JointStateWrapper::callback(const TopicInputName& topic_name, const sensor_msgs::JointState::ConstPtr& msg)
{
	int n = msg->name.size();
	if (msg->position.size() != n || msg->velocity.size() != n || msg->effort.size() != n) 
    {
		ROS_ERROR("Received incorrect message. Skip"); 
        return;
    }

	for (int i = 0; i < n; i++)
	{
		if (interdependence[topic_name].count(msg->name[i]) == 0) continue;

        MotorParam motor_param = interdependence[topic_name][msg->name[i]];
		MotorState& motor_state = packeges[motor_param.output_topic][motor_param.output_name];

		motor_state.position  = (msg->position[i] - motor_param.offset) / motor_param.gear_mult;
		motor_state.velocity  = msg->velocity[i] / motor_param.gear_mult;
		motor_state.effort    = msg->effort[i] * motor_param.gear_mult;
		motor_state.is_update = true;
	}

    //decide to publish or not
    for (const auto& [topic_name, motors]: packeges){
        size_t n = 0;
        for (const auto& [out_name, state]: motors)
            n += state.is_update;
        // not enough new data
        if (n * 100 >= motors.size() * PUSH_WHEN)
            push_message(topic_name);
    }

}

void JointStateWrapper::push_message(const TopicOutputName& topic_name)
{   
    sensor_msgs::JointState msg;
    std::stringstream caster;

    for (auto& [out_name, state]: packeges[topic_name])
    {
        if (!state.is_update) continue;
        msg.name.push_back(out_name);
        msg.position.push_back(state.position);
        msg.velocity.push_back(state.velocity);
        msg.effort.push_back (state.effort);
        state.is_update = false;
    }

	publishers[topic_name].publish(msg);
}

void JointStateWrapper::spin()
{
	ros::spin();
}