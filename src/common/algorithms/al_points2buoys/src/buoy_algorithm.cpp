#include <al_points2buoys/buoy_algorithm.hpp>

BuoyAlgorithm::BuoyAlgorithm()
    :buoy_cloud(new PCLPoints)
{}

void BuoyAlgorithm::start_node(ros::NodeHandle& nh)
{
    // getting topic names from param
    resolve_path.init(nh);

    nh.getParam("points_topic_name", points_topic_name);
    nh.getParam("pos_topic_name",  pos_topic_name);
    nh.getParam("buoy_frame",  frame_name);

    points_topic_name = resolve_path(points_topic_name);
    pos_topic_name = resolve_path(pos_topic_name);
    frame_name = resolve_path(frame_name).substr(1);

    std::string package = ros::package::getPath("al_points2buoys");
    bouy_file_name = join_paths(package,"/model/buoy.obj");

    // connect topics ans service
    points_sub   = nh.subscribe<sensor_msgs::PointCloud2>(points_topic_name, 1, &BuoyAlgorithm::bouyCallback, this);
    buoy_pos_pub = nh.advertise<geometry_msgs::PoseArray>("buoy_positions", 1);
    buoy_pos_service = nh.advertiseService("points2buoy", &BuoyAlgorithm::bouyService, this);

    //Подписка на получение координат камеры
    position_sub = nh.subscribe<geometry_msgs::TransformStamped>(pos_topic_name, 1, &BuoyAlgorithm::positionCallback, this);

    // read OBJ file
    int loadCode = pcl::io::loadOBJFile(bouy_file_name,*buoy_cloud);
    ROS_ASSERT_MSG(loadCode >= 0, "Could not open file %s", bouy_file_name.c_str());

    // Задаю фрейм для корректного отображения
    buoy_cloud->header.frame_id = frame_name;
    buoy_cloud = downSampling(buoy_cloud);

    buoy_cloud_pub = nh.advertise<sensor_msgs::PointCloud2>("bouy_cloud", 1);

    //for testing
    // all_points_pub = nh.advertise<sensor_msgs::PointCloud2>("all_points", 1);
}

void BuoyAlgorithm::publish(const ros::Publisher& pub, const PCLPoints& point) const{
    sensor_msgs::PointCloud2 MSGSout;
    pcl::toROSMsg(point,MSGSout);
    MSGSout.header.frame_id = frame_name;
    pub.publish(MSGSout);
}
void BuoyAlgorithm::publish(const ros::Publisher& pub, const sensor_msgs::PointCloud2& points) const{
    sensor_msgs::PointCloud2 MSGSout (points);
    MSGSout.header.frame_id = frame_name;
    pub.publish(MSGSout);
}

void BuoyAlgorithm::getBuoyInPose(PCLPoints& points, const geometry_msgs::Pose& pose)const{
    Eigen::Affine3d transform(Eigen::Affine3d::Identity());
    transform.translation() << pose.position.x, pose.position.y, pose.position.z;
    transform.matrix().block(0,0,3,3) = Eigen::Quaterniond(pose.orientation.w, pose.orientation.x, pose.orientation.y, pose.orientation.z).toRotationMatrix();
    pcl::transformPointCloud(*buoy_cloud, points, transform);
}

void BuoyAlgorithm::positionCallback(const geometry_msgs::TransformStamped::ConstPtr& T){
    cam_pos.x = T->transform.translation.x;
    cam_pos.y = T->transform.translation.y;
    cam_pos.z = T->transform.translation.z;
}

PCLPoints::Ptr BuoyAlgorithm::cutPointCloud(const PCLPoints::Ptr inPoints, double xMin, double xMax, double yMin, double yMax, double zMin, double zMax){
    PCLPoints::Ptr PCLpoints( new PCLPoints(*inPoints)), outPoints(new PCLPoints());

    pcl::PassThrough<pcl::PointXYZ> pass;   // Create the filtering object
    pass.setInputCloud (PCLpoints);
    pass.setFilterFieldName ("z");          // remove according to the z-axis value
    pass.setFilterLimits (zMin, zMax);
    pass.filter (*outPoints);

    pass.setInputCloud (outPoints);
    pass.setFilterFieldName ("x");          // remove according to the x-axis value
    pass.setFilterLimits (xMin, xMax);
    pass.filter (*PCLpoints);

    pass.setInputCloud (PCLpoints);
    pass.setFilterFieldName ("y");          // remove according to the y-axis value
    pass.setFilterLimits (yMin, yMax);
    pass.filter (*outPoints);

    return outPoints;
}

PCLPoints::Ptr BuoyAlgorithm::removeFarPoints(const PCLPoints::Ptr inPoints){
    return cutPointCloud(inPoints,-0.5, 0.5, -0.5, 0.5, 0.01, 0.25); // save values from 0.01 to 0.25, because the buoy has height equals 0.125
}

PCLPoints::Ptr BuoyAlgorithm::downSampling(const PCLPoints::Ptr points, double size){
    PCLPoints::Ptr sparse_cloud( new PCLPoints);
    pcl::VoxelGrid<PointT> sor;
    sor.setInputCloud (points);
    sor.setLeafSize (size, size, size);
    sor.filter (*sparse_cloud);
    return sparse_cloud;
}

geometry_msgs::PoseArray::Ptr BuoyAlgorithm::findBouy(const sensor_msgs::PointCloud2& inPoints){
    PCLPoints::Ptr pclPoints( new PCLPoints());
    pcl::fromROSMsg(inPoints, *pclPoints);
    // publish(all_points_pub, inPoints);
    return findBouy(pclPoints);
}

void BuoyAlgorithm::bouyCallback(const sensor_msgs::PointCloud2::ConstPtr& pointCloud){
    geometry_msgs::PoseArray::Ptr poses = findBouy(*pointCloud);
    poses->header.frame_id = frame_name;
    buoy_pos_pub.publish(*poses);
}

bool BuoyAlgorithm::bouyService(al_points2buoys::Points2buoys::Request &request, al_points2buoys::Points2buoys::Response &response){
    geometry_msgs::PoseArray::Ptr poses = findBouy(request.points);
    poses->header.frame_id = frame_name;
    response.poses = *poses;
    return true;
}

void BuoyAlgorithm::spin()
{
    ros::spin();
}
