#include <cmath>
#include <eurobot/math/linalg.hpp>


Matrix::Matrix() : Matrix(0, 0) {}
Matrix::Matrix(int rows, int cols, double fill) : rows(rows), cols(cols), data(rows * cols, fill)
{
    if (cols < 0 || rows < 0)
        throw std::runtime_error("Matrix::Matrix(): cols < 0 || rows < 0");
}
Matrix::Matrix(int rows, int cols, const std::vector<double>& data) : rows(rows), cols(cols), data(data)
{
    if (cols < 0 || rows < 0)
        throw std::runtime_error("Matrix::Matrix(): cols < 0 || rows < 0");
    if (cols * rows != data.size())
        throw std::runtime_error("Matrix::Matrix(): cols * rows != data.size()");
}

Matrix Matrix::eye(int size)
{
    Matrix res = Matrix(size, size);
    for (int i = 0; i < size; i++) res.at(i, i) = 1;
    return res;
}

void Matrix::assert_yx(int y, int x) const
{
    if (x < 0 || x >= cols || y < 0 || y >= rows)
        throw std::runtime_error("Matrix::assert_yx(): x < 0 || x >= cols || y < 0 || y >= rows");
}

double& Matrix::at(int y, int x) { assert_yx(y, x); return data[cols * y + x]; }
double Matrix::at(int y, int x) const { assert_yx(y, x); return data[cols * y + x]; }

std::vector<double>& Matrix::get_data() { return data; }
std::vector<double> Matrix::get_data() const { return data; }

Matrix Matrix::minor(int y, int x) const
{
    Matrix res(rows-1, cols-1);
    for (int yi = 0; yi < res.rows; yi++)
        for (int xi = 0; xi < res.cols; xi++)
            res.at(yi, xi) = at(yi + (yi >= y), xi + (xi >= x));
    return res;
}

Vector Matrix::get_row(int y) const
{
    Vector res(cols);
    for (int x = 0; x < cols; x++) res.at(x) = at(y, x);
    return res;
}

Vector Matrix::get_column(int x) const
{
    Vector res(rows);
    for (int y = 0; y < rows; y++) res.at(y) = at(y, x);
    return res;
}

Matrix Matrix::transpose() const
{
    Matrix res(cols, rows);
    for (int y = 0; y < res.rows; y++)
        for (int x = 0; x < res.cols; x++)
            res.at(x, y) = at(y, x);
    return res;
}

Matrix Matrix::operator -() const
{
    Matrix res = *this;
    for (double& e : res.data) e = -e;
    return res;
}

Matrix Matrix::operator +(double a) const { Matrix res = *this; return res += a; }
Matrix Matrix::operator -(double a) const { Matrix res = *this; return res -= a; }
Matrix Matrix::operator *(double a) const { Matrix res = *this; return res *= a; }
Matrix Matrix::operator /(double a) const { Matrix res = *this; return res /= a; }

Matrix& Matrix::operator +=(double a)
{
    for (double& e : data) e += a;
    return *this;
}

Matrix& Matrix::operator -=(double a)
{
    for (double& e : data) e -= a;
    return *this;
}

Matrix& Matrix::operator *=(double a)
{
    for (double& e : data) e *= a;
    return *this;
}

Matrix& Matrix::operator /=(double a)
{
    for (double& e : data) e /= a;
    return *this;
}

Matrix Matrix::operator +(const Matrix& m) const { Matrix res = *this; return res += m; }
Matrix Matrix::operator -(const Matrix& m) const { Matrix res = *this; return res -= m; }
Matrix Matrix::operator *(const Matrix& m) const { Matrix res = *this; return res *= m; }


Matrix& Matrix::operator +=(const Matrix& m)
{
    if (cols != m.cols || rows != m.rows)
        throw std::runtime_error("Matrix::operator +=(): cols != m.cols || rows != m.rows");
    for (int i = 0; i < data.size(); i++) data[i] += m.data[i];
    return *this;
}

Matrix& Matrix::operator -=(const Matrix& m)
{
    if (cols != m.cols || rows != m.rows)
        throw std::runtime_error("Matrix::operator -=(): cols != m.cols || rows != m.rows");
    for (int i = 0; i < data.size(); i++) data[i] -= m.data[i];
    return *this;
}

Matrix& Matrix::operator *=(const Matrix& m)
{
    // C = A * B
    if (cols != m.rows)
        throw std::runtime_error("Matrix::operator *=(): cols != m.rows");

    const std::vector<double> A = data;
    const std::vector<double>& B = m.data;
    std::vector<double>& C = data = std::vector<double>(rows * m.cols);

    for (int y = 0; y < rows; y++)
        for (int x = 0; x < m.cols; x++)
        {
            double& r = C[m.cols * y + x];
            for (int i = 0; i < cols; i++)
                // at(y, i) * m.at(i, x)
                r += A[cols * y + i] * B[m.cols * i + x];
        }

    cols = m.cols;
    return *this;
}

std::ostream& operator <<(std::ostream& out, const Matrix& m)
{
    out << "[";
    for (int y = 0; y < m.rows; y++)
    {
        if (y != 0) out << ", ";
        out << "[";
        for (int x = 0; x < m.cols; x++)
        {
            if (x != 0) out << ", ";
            out << m.at(y, x);
        }
        out << "]";
    }
    return out << "]";
}





Matrix AffineMatrix4x4::RotateX(double a)
{
    Matrix res = Matrix::eye(4);
    res.data[5] = res.data[10] = std::cos(a);
    res.data[6] = -(res.data[9] = std::sin(a));
    return res;
}
Matrix AffineMatrix4x4::RotateY(double a)
{
    Matrix res = Matrix::eye(4);
    res.data[0] = res.data[10] = std::cos(a);
    res.data[8] = -(res.data[2] = std::sin(a));
    return res;
}
Matrix AffineMatrix4x4::RotateZ(double a)
{
    Matrix res = Matrix::eye(4);
    res.data[0] = res.data[5] = std::cos(a);
    res.data[1] = -(res.data[4] = std::sin(a));
    return res;
}

Matrix AffineMatrix4x4::TranslateXYZ(double x, double y, double z)
{
    Matrix res = Matrix::eye(4);
    res.data[3 + 4 * 0] = x;
    res.data[3 + 4 * 1] = y;
    res.data[3 + 4 * 2] = z;
    return res;
}