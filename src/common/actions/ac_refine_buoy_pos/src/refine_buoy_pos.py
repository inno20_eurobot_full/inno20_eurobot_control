#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Уточнить координаты буйка, используя данные с realsense
"""

import rospy
import math
import tf2_geometry_msgs as tf2_g
import tf2_ros

from eurobot.core import PathResolver
from geometry_msgs.msg import PoseArray, Pose

from ac_refine_buoy_pos.srv import RefineBuoyPos, RefineBuoyPosRequest, RefineBuoyPosResponse


class RefineBuoyPosNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        self.poseArr = PoseArray()
        resolve_path = PathResolver()

        self.in_buoy_frame = resolve_path(rospy.get_param("in_buoy_frame"))
        self.robot_frame = resolve_path(rospy.get_param("robot_frame"))
        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)

        self.min_distance = float(rospy.get_param("min_distance"))
        buoy_topic_name = resolve_path(rospy.get_param("buoy_poses"))
        rospy.Subscriber(buoy_topic_name, PoseArray, self.update_poses)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.loginfo("Main for action 'refine_buoy_pos' is loaded")
        self._server = rospy.Service("command", RefineBuoyPos, self.server_callback)

    def update_poses(self, poseArr: PoseArray):
        """Update current buoy positions"""
        self.poseArr = poseArr

    def dist(self, pos1: Pose, pos2: Pose) -> float:
        """Get distance between two poses"""
        pos1 = pos1.position
        pos2 = pos2.position
        return math.sqrt((pos1.x - pos2.x) ** 2 + (pos1.y - pos2.y) ** 2 + (pos1.z - pos2.z) ** 2)

    def server_callback(self, req: RefineBuoyPosRequest) -> RefineBuoyPosResponse:
        """Callback for server."""
        cur_pos = req.pos

        answer = RefineBuoyPosResponse()
        answer.done = False

        if req.pos.header.frame_id != self.in_buoy_frame:
            rospy.logwarn(f"Refine_buoy_pos: incorrect frame'{req.pos.header.frame_id}' need '{self.in_buoy_frame}'")
            return answer

        try:
            trans = self.tfBuffer.lookup_transform(
                self.in_buoy_frame, self.robot_frame, rospy.Time(0)
            )
            cur_pos = tf2_g.do_transform_pose(cur_pos, trans)
        except (
            tf2_ros.LookupException,
            tf2_ros.ConnectivityException,
            tf2_ros.ExtrapolationException,
        ):
            rospy.logwarn(f"Refine_buoy_pos: no transform from '{self.robot_frame}' to '{self.in_buoy_frame}'")
            return answer

        answer.pos = cur_pos

        min_dist = self.min_distance

        for b_pose in self.poseArr.poses:
            cur_dist = self.dist(cur_pos.pose, b_pose)
            if cur_dist < min_dist:
                answer.pos.pose = b_pose
                answer.done = True
                min_dist = cur_dist

        return answer


def main():
    """Main entrypoint."""
    rospy.init_node("refine_buoy_pos", anonymous=True)

    main = RefineBuoyPosNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
