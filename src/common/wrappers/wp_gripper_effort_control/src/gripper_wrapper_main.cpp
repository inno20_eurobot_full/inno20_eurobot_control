#include <string>

#include <wp_gripper_effort_control/simple_gripper_wrapper.hpp>

int main(int argc, char** argv)
{
	// initialize ros
	ros::init(argc, argv, "gripper_wrapper");

	ros::NodeHandle nh;

	// checking that the 'topic' is 'simple'
	SimpleGripperWrapper::check_param <std::string>(nh, "type", "simple");

	// start program
	SimpleGripperWrapper main(nh);
	main.start_node(nh);
	main.spin();

	return 0;
}