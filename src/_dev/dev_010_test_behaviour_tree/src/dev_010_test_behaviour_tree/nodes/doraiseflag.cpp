#include "dev_010_test_behaviour_tree/nodes/doraiseflag.h"

BT::PortsList DoRaiseFlag::providedPorts() {
  return {
    BT::InputPort<bool>("goal")
  };
}

bool DoRaiseFlag::sendGoal(GoalType& goal){
  bool value;
  if( getInput("goal", value ) ){
    ROS_INFO("RaiseFlagAction: Sending a request to %s the flag", value?"raise":"omit");
    goal.do_it = value;
    return true;
  }
  return false;
}

BT::NodeStatus DoRaiseFlag::onResult( const ResultType& res)
{
  ROS_INFO("RaiseFlagAction: work is done %ssuccessfully", res.done?"":"UN");
  return BT::NodeStatus::SUCCESS;
}

BT::NodeStatus DoRaiseFlag::onFailedRequest(FailureCause failure)
{
  ROS_ERROR("RaiseFlagAction request failed %d", static_cast<int>(failure));
  return BT::NodeStatus::FAILURE;
}

void DoRaiseFlag::halt()
{
  if( status() == BT::NodeStatus::RUNNING ) {
    ROS_WARN("RaiseFlagAction aborted(halted)");
    BaseClass::halt();
  }
}