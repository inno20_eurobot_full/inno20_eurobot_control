#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Включить/выключить питание робота (по правилу мы должны отключить питание моторов по окончании игры)
"""

import rospy
from eurobot.core import PathResolver

from ac_set_power_state.srv import SetPowerState, SetPowerStateRequest, SetPowerStateResponse


class SetPowerStateNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self._server = rospy.Service("command", SetPowerState, self.server_callback)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.logwarn("Mock for action 'set_power_state' is loaded")

    def server_callback(self, req: SetPowerStateRequest) -> SetPowerStateResponse:
        """Callback for server."""
        return SetPowerStateResponse(True)


def main():
    """Main entrypoint."""
    rospy.init_node("set_power_state", anonymous=True)

    main = SetPowerStateNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
