#!/usr/bin/env python3
import rospy
import sys
import actionlib

from ac_activate_windsock.msg import ActivateWindsockAction, ActivateWindsockGoal


def main():
    """Main entrypoint."""
    client = actionlib.SimpleActionClient(
        "/robot1/actions/activate_windsock/command",
        ActivateWindsockAction,
    )

    print("wait_for_server")
    client.wait_for_server()

    side_y = bool(int(sys.argv[1])) if len(sys.argv) > 1 else False
    goal = ActivateWindsockGoal()
    goal.side = goal.SIDE_YELLOW if side_y else goal.SIDE_BLUE

    print("send_goal")
    client.send_goal(goal)

    print("wait_for_result")
    client.wait_for_result()

    print(f"Result: {client.get_result()}")


if __name__ == "__main__":
    rospy.init_node("prepare_manipulator_for_drive")
    main()
