#!/usr/bin/env python3

import sys
import rospy
from al_manipulator.srv import ManipulatorAdvancedIK
from al_manipulator.msg import ManipulatorEndPos
from ct_manipulator.msg import ManipulatorP2PAction, ManipulatorP2PGoal

# Brings in the SimpleActionClient
import actionlib

MOTOR_NAMES = ["motor0", "motor1", "motor2", "motor3", "motor4"]


def feedback_processing(fb):
    """Callback for JointState"""
    print("p2p feedback:", fb)


def main():
    """Main entrypoint."""
    rospy.init_node("test_advik", anonymous=True)
    calc_advik = rospy.ServiceProxy(
        "/robot1/algorithms/manipulator_advik/calc_advik", ManipulatorAdvancedIK
    )
    calc_advik.wait_for_service()

    p2p = actionlib.SimpleActionClient(
        "/robot1/control/manipulator_p2p/command",
        ManipulatorP2PAction,
    )
    p2p.wait_for_server()

    x, y, z, r, p = 0.38, 0, 0.12, 0, 0
    if len(sys.argv) > 3:
        x = float(sys.argv[1])
        y = float(sys.argv[2])
        z = float(sys.argv[3])
    if len(sys.argv) > 4:
        r = float(sys.argv[4])
    if len(sys.argv) > 5:
        p = float(sys.argv[5])

    pos = ManipulatorEndPos(x=x, y=y, z=z, roll=r, pitch=p)

    ik = calc_advik(pos).res
    for sol in ik:
        print(sol.name)
        print(sol.position)
        print()

    if len(ik) > 0:
        p2p.send_goal(ManipulatorP2PGoal(ik[0]), feedback_cb=feedback_processing)
        p2p.wait_for_result()  # wait for server to finish the action
        print("test_H_in_ik", ": result = ", p2p.get_result().done)
    else:
        print("no solutions")


if __name__ == "__main__":
    main()
