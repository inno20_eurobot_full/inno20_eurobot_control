#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Активировать windsock (повернуть манипулятором так, чтобы повернуть windsock)
"""

import time
import rospy
from eurobot.core import PathResolver

import actionlib
import ac_activate_windsock.msg


class ActivateWindsock:
    """Wrapper for action."""
    Action = ac_activate_windsock.msg.ActivateWindsockAction
    Feedback = ac_activate_windsock.msg.ActivateWindsockFeedback
    Goal = ac_activate_windsock.msg.ActivateWindsockGoal
    Result = ac_activate_windsock.msg.ActivateWindsockResult


class ActivateWindsockNode:
    """Main class."""

    _result = ActivateWindsock.Result()

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self._action_server = actionlib.SimpleActionServer(
            "command",
            ActivateWindsock.Action,
            execute_cb=self.action_callback,
            auto_start=False)
        self._action_server.start()

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.logwarn("Mock for action 'activate_windsock' is loaded")

    def action_callback(self, goal: ActivateWindsock.Goal):
        """Callback for action."""
        time.sleep(4)
        self._result.res = True
        self._action_server.set_succeeded(self._result)


def main():
    """Main entrypoint."""
    rospy.init_node("activate_windsock", anonymous=True)

    main = ActivateWindsockNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
