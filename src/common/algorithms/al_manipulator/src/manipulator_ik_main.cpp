#include <al_manipulator/manipulator_ik.hpp>


/*
    Result always array with 0, 2 or 4 elements. Each
    element of output array is one of 4 cases:

             ●
            / \
         ┌─●   ●───x        ┌─●   ●───x
         |                  |  \ /
        ▐█▌                ▐█▌  ●
        ▐█▌                ▐█▌
            Case 0             Case 1


           ●─────x             ●─────x
            \                 /
             ●               ●
            /                 \
           ●─┐                 ●─┐
             |                   |
            ▐█▌                 ▐█▌
            ▐█▌                 ▐█▌
            Case 2             Case 3

    All computation are processed relative zero-position:

           M2  M3  M4
         ┌─●───●───●───x
         |
        ▐█▌
        ▐█▌ M1
*/

int main(int argc, char** argv)
{
	// initialize ros
	ros::init(argc, argv, "manipulator_ik");

	ros::NodeHandle nh;

	// start program
	ManipulatorIK main;
	if (!main.start_node(nh)) return -1;
	main.spin();

	return 0;
}