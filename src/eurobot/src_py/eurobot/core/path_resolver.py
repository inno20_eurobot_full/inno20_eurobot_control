"""Модуль для разрешения путей."""


from typing import Optional
import os.path

import rospy


class PathResolver:
    """Решатель путей"""

    def __init__(self, home_path: Optional[str] = None):
        """Создание решателя путей.

        Если home_path равен None, то путь возьмется автоматически из сервера параметров ROS
        """
        if home_path is not None:
            self._home_path = home_path
        elif rospy.has_param("home_path"):
            self._home_path = rospy.get_param("home_path")
        else:
            rospy.logwarn("Parameter 'home_path' not found. Either create this parameter or use an "
                          + "explicit path when creating the class")
            self._home_path = "/"

    def __call__(self, path: str):
        """Resolve path.

        If path starts from '~/' then replace in to home_path.
        """
        assert isinstance(path, str)

        if path[0] == "/":  # absolute path
            return path
        if path[:2] == "~/":
            return os.path.join(self._home_path, path[2:])
        return path
