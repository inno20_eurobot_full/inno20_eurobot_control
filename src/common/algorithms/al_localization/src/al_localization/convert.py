#!/usr/bin/env python3
"""Node that converts tf to different localization data sources"""

import rospy
from nav_msgs.msg import Odometry
from geometry_msgs.msg import PoseWithCovarianceStamped, TransformStamped
from eurobot.core import PathResolver
import tf2_ros
import math

config = None

odom_pose_covariance = [
    0.01, 0, 0, 0, 0, 0,
    0, 0.01, 0, 0, 0, 0,
    0, 0, 1000, 0, 0, 0,
    0, 0, 0, 1000, 0, 0,
    0, 0, 0, 0, 1000, 0,
    0, 0, 0, 0, 0, 0.04,
]

odom_twist_covariance = [
    0.01, 0, 0, 0, 0, 0,
    0, 1000, 0, 0, 0, 0,
    0, 0, 1000, 0, 0, 0,
    0, 0, 0, 1000, 0, 0,
    0, 0, 0, 0, 1000, 0,
    0, 0, 0, 0, 0, 0.04,
]

gps_covariance = [
    0.01, 0, 0, 0, 0, 0,
    0, 0.01, 0, 0, 0, 0,
    0, 0, 1000, 0, 0, 0,
    0, 0, 0, 1000, 0, 0,
    0, 0, 0, 0, 1000, 0,
    0, 0, 0, 0, 0, 0.04,
]


def _odom_callback(odom: Odometry):
    odom.header.stamp = rospy.Time.now()
    odom.header.frame_id = 'odom'
    odom.child_frame_id = 'base_link'

    odom.pose.covariance = odom_pose_covariance
    odom.twist.covariance = odom_twist_covariance
    odom_pub.publish(odom)

    frame = TransformStamped()

    frame.header = odom.header
    frame.header.frame_id = 'map'
    frame.child_frame_id = 'raw_odom'

    frame.transform.translation = odom.pose.pose.position
    frame.transform.rotation = odom.pose.pose.orientation

    out_bc.sendTransform(frame)


if __name__ == "__main__":
    rospy.init_node("localization_convert")

    tfbuf = tf2_ros.Buffer()
    tf = tf2_ros.TransformListener(tfbuf)

    out_bc = tf2_ros.TransformBroadcaster()

    aruco_no = rospy.get_param('aruco_no')

    resolve_path = PathResolver()

    odom_sub = rospy.Subscriber(resolve_path('~/driver/maxon_base_1/odom'), Odometry, _odom_callback)
    odom_pub = rospy.Publisher('input/odom', Odometry, queue_size=1)
    gps_pub = rospy.Publisher('input/gps', PoseWithCovarianceStamped, queue_size=1)

    rate = rospy.Rate(30)
    while not rospy.is_shutdown():
        try:
            gps: tf2_ros.TransformStamped = tfbuf.lookup_transform(
                'map', f'fiducial_{aruco_no}', rospy.Time.now(), rospy.Duration(1))
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            continue

        frame = PoseWithCovarianceStamped()

        frame.header = gps.header
        frame.header.frame_id = 'map'

        frame.pose.pose.position.x = gps.transform.translation.x
        frame.pose.pose.position.y = gps.transform.translation.y
        q = gps.transform.rotation
        q.x = q.y = 0
        qm = math.sqrt(q.z**2 + q.w**2)
        frame.pose.pose.orientation.z = q.z / qm
        frame.pose.pose.orientation.w = q.w / qm
        frame.pose.covariance = gps_covariance
        gps_pub.publish(frame)

        rate.sleep()
