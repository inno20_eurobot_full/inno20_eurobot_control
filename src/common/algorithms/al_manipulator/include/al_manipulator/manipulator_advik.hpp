#pragma once

#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <al_manipulator/ManipulatorEndPos.h>
#include <al_manipulator/ManipulatorTF.h>
#include <al_manipulator/ManipulatorIK.h>
#include <al_manipulator/ManipulatorAdvancedIK.h>

#include <vector>
#include <string>
#include <algorithm>

#include <eurobot/core/path_utils.hpp>
#include <eurobot/math/linalg.hpp>
#include <al_manipulator/manipulator_h.hpp>
#include <al_manipulator/manipulator_ik.hpp>

struct Constrain{
    double min, max;
};

class ManipulatorAdvIK : public ManipulatorIK
{
private:
    PathResolver resolve_path;

    std::vector<Constrain> constrains;// min, max, mid
    double base_height;

    bool process(const al_manipulator::ManipulatorEndPos& pos, std::vector<sensor_msgs::JointState>& state);

    bool loadConstrains(const ros::NodeHandle& nh);

protected:
    bool service_callback(al_manipulator::ManipulatorAdvancedIK::Request& req, al_manipulator::ManipulatorAdvancedIK::Response& res);

    bool start_service(ros::NodeHandle& nh) override;

public:
    bool start_node(ros::NodeHandle& nh) override;
};