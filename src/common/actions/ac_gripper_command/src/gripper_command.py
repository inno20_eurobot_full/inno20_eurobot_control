#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Изменить состояние гриппера (закрыть гриппер или открыть его)
"""

import rospy
from eurobot.core import PathResolver

import actionlib
import ac_gripper_command.msg
from ct_manipulator_gripper.msg import GripperControlAction, GripperControlGoal


class GripperCommand:
    """Wrapper for action."""

    Action = ac_gripper_command.msg.GripperCommandAction
    Feedback = ac_gripper_command.msg.GripperCommandFeedback
    Goal = ac_gripper_command.msg.GripperCommandGoal
    Result = ac_gripper_command.msg.GripperCommandResult


class GripperCommandNode:
    """Main class."""

    _result = GripperCommand.Result()

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self.gipper_action_name = self.resolve_path(rospy.get_param("gripper_action"))

        self._action_server = actionlib.SimpleActionServer(
            "command",
            GripperCommand.Action,
            execute_cb=self.action_callback,
            auto_start=False,
        )
        self._action_client = actionlib.SimpleActionClient(
            self.gipper_action_name,
            GripperControlAction,
        )

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.loginfo("Main for action 'gripper_command' is loaded")
        self._action_client.wait_for_server()
        self._action_server.start()

    def action_callback(self, goal: GripperCommand.Goal):
        """Callback for action."""
        if goal.goal == GripperCommand.Goal.GOAL_OPEN:
            forward_goal = GripperControlGoal.GOAL_OPEN
        elif goal.goal == GripperCommand.Goal.GOAL_CLOSE:
            forward_goal = GripperControlGoal.GOAL_CLOSE
        else:
            rospy.logwarn("GripperCommand received unexpected goal")
            self._result = False
            self._action_server.set_succeeded(self._result)
            return

        self._action_client.send_goal(GripperControlGoal(forward_goal))
        self._action_client.wait_for_result()

        self._result.res = True
        self._action_server.set_succeeded(self._result)


def main():
    """Main entrypoint."""
    rospy.init_node("gripper_command", anonymous=True)

    main = GripperCommandNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
