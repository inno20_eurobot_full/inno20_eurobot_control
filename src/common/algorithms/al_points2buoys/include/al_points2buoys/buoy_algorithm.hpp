#pragma once

#include <ros/ros.h>
#include <ros/package.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/TransformStamped.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/obj_io.h>
#include <pcl/common/transforms.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/features/normal_3d.h>

#include <al_points2buoys/Points2buoys.h>

#include <vector>
#include <string>

#include <eurobot/core/path_utils.hpp>


using PointT = pcl::PointXYZ;
using PCLPoints = pcl::PointCloud<PointT>;
using vectorPCLpointsPtr = std::vector<PCLPoints::Ptr>;
using SurfaceNormals = pcl::PointCloud<pcl::Normal>;
using LocalFeatures = pcl::PointCloud<pcl::FPFHSignature33>;
using SearchMethod = pcl::search::KdTree<PointT>;

class BuoyAlgorithm
{
public:
    BuoyAlgorithm();

    virtual void start_node(ros::NodeHandle& nh);

    virtual void spin();

private:
    void positionCallback(const geometry_msgs::TransformStamped::ConstPtr&);
    
    void bouyCallback(const sensor_msgs::PointCloud2::ConstPtr&);
    bool bouyService(al_points2buoys::Points2buoys::Request &request, al_points2buoys::Points2buoys::Response &response);
    std::string bouy_file_name;
    std::string points_topic_name;
    std::string pos_topic_name;

    geometry_msgs::PoseArray::Ptr findBouy(const sensor_msgs::PointCloud2&);

    ros::Subscriber points_sub;
    ros::Subscriber position_sub;
    ros::ServiceServer buoy_pos_service;

protected:
    virtual geometry_msgs::PoseArray::Ptr findBouy(const PCLPoints::Ptr) = 0;

    static PCLPoints::Ptr cutPointCloud(const PCLPoints::Ptr, double xMin, double xMax, double yMin, double yMax, double zMin, double zMax);
    static PCLPoints::Ptr removeFarPoints(const PCLPoints::Ptr);
    static PCLPoints::Ptr downSampling(const PCLPoints::Ptr, double size = 0.005);

    void publish(const ros::Publisher& pub, const PCLPoints& point) const;
    void publish(const ros::Publisher& pub, const sensor_msgs::PointCloud2&) const;
    void getBuoyInPose(PCLPoints& points, const geometry_msgs::Pose&) const;
    
    PathResolver resolve_path;

    std::string frame_name;

    ros::Publisher buoy_pos_pub;
    ros::Publisher buoy_cloud_pub;

    PCLPoints::Ptr buoy_cloud;

    PointT cam_pos;

    // testing
    // ros::Publisher all_points_pub;
};