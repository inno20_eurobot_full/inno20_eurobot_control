#include <behavior_tree/wrap_nodes/actions/refine_buoy_pos.h>
#include <geometry_msgs/PoseStamped.h>

BT::PortsList RefineBuoyPos::providedPorts()
{
    return {
        BT::BidirectionalPort<geometry_msgs::PoseStamped>("pos")
    };
}

void RefineBuoyPos::sendRequest(RequestType& request)
{
    geometry_msgs::PoseStamped value;
    if( !getInput("pos", value ) ){
        ROS_WARN("RefineBuoyPos, no value in port");
    }
    request.pos = value;
}

BT::NodeStatus RefineBuoyPos::onResponse(const ResponseType& rep)
{
    setOutput("pos", rep.pos);
    if (rep.done == false)
        return BT::NodeStatus::FAILURE;
    return BT::NodeStatus::SUCCESS;
}

BT::NodeStatus RefineBuoyPos::onFailedRequest(RosServiceNode::FailureCause failure)
{
    ROS_ERROR("RefineBuoyPos request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}