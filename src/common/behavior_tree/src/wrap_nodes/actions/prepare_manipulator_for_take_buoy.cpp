#include <behavior_tree/wrap_nodes/actions/prepare_manipulator_for_take_buoy.h>

BT::PortsList PrepareManipulatorForTakeBuoy::providedPorts() {
    return {
        BT::InputPort<geometry_msgs::PoseStamped>("pos")
    };
}

bool PrepareManipulatorForTakeBuoy::sendGoal(GoalType& goal){
    geometry_msgs::PoseStamped value;
    if( !getInput("pos", value ) ){
        ROS_WARN("PrepareManipulatorForTakeBuoy has incorrect port value");
        return false;
    }
    goal.buoy_pos = value;
    return true;
}
    

BT::NodeStatus PrepareManipulatorForTakeBuoy::onResult( const ResultType& res)
{
    if (res.res == true)
        return BT::NodeStatus::SUCCESS;
    return BT::NodeStatus::FAILURE; 
}


BT::NodeStatus PrepareManipulatorForTakeBuoy::onFailedRequest(FailureCause failure)
{
    ROS_ERROR("PrepareManipulatorForTakeBuoy request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}

void PrepareManipulatorForTakeBuoy::halt()
{
    if( status() == BT::NodeStatus::RUNNING ) {
        ROS_WARN("PrepareManipulatorForTakeBuoy aborted(halted)");
        BaseClass::halt();
    }
}