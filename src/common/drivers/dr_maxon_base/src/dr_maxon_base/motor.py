"""Module for control real motor using 'canopen' library."""

import rospy
import canopen

_reduction = 299.0 / 14.0
_pos_scale = 2000.0 * _reduction
_vel_scale = 60.0 * _reduction


class Motor:
    """Class for control one real motor."""

    def __init__(self, network, node_id, scale, object_dictionary, transmit_rate):
        """Connect to canopen node."""
        self.node_id = node_id
        self.scale = scale
        self.network = network

        self.node = canopen.RemoteNode(node_id, object_dictionary)
        network.add_node(self.node)

        rospy.loginfo(f'Voltage on maxon node {node_id} is {self.get_voltage()}')

        self.node.pdo.read()

        self.node.nmt.state = "OPERATIONAL"

        self.node.sdo["Modes of operation"].phys = 3
        self.node.sdo["Controlword"].phys = 0x0006

        self.set_speed(0)
        self.node.rpdo[4]["Controlword"].phys = 0x0006
        self.node.rpdo[4].start(1 / transmit_rate)

        self.node.tpdo[3]["Position actual value"].phys = self.node.sdo[
            "Position actual value"
        ].phys

    def disconnect(self):
        """Disconnect from node."""
        self.node.rpdo[4].stop()
        self.node.sdo["Controlword"].phys = 0x0006
        self.node.nmt.state = "PRE-OPERATIONAL"
        del self.network[self.node_id]

    def get_voltage(self):
        """Get voltage on driver in volts."""
        return 0.1 * self.node.sdo['Power supply']['Power supply voltage'].phys

    def get_position(self):
        """Get position of motor in turns."""
        return self.node.tpdo[3]["Position actual value"].phys / self.scale / _pos_scale

    def set_speed(self, speed=0):
        """Set speed of motor in turns/sec."""
        self.node.rpdo[4]["Target velocity"].phys = speed * _vel_scale * self.scale

    def enable(self):
        """Enables motors torque"""
        self.set_speed(0)
        self.node.rpdo[4]["Controlword"].phys = 0x000F

    def disable(self):
        """Disables motors torque"""
        self.node.rpdo[4]["Controlword"].phys = 0x0006


def init_motors(object_dictionary, config, transmit_rate):
    """Create two instances of class Motor for control two real motors."""
    network = canopen.Network()
    network.connect(bustype=config['bustype'], channel=config['channel'], bitrate=config['bitrate'])

    motor_left = Motor(network, 1, 1.0, object_dictionary, transmit_rate)
    motor_right = Motor(network, 2, -1.0, object_dictionary, transmit_rate)

    return network, motor_left, motor_right
