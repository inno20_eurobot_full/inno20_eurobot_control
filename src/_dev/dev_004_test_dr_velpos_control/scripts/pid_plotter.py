"""Модуль для визуализации работы PID регулятора."""


import matplotlib.pyplot as plt


FPS = 60
SIMULATE_TIME = 1.3  # sec


class PID:
    """Класс PID регулятора."""

    def __init__(self, p, i, d):
        """Создание PID регулятора с заданными параметрами."""
        self._p, self._i, self._d = p, i, d
        self._last_err = 0
        self._intergal = 0

    def update(self, err, dt):
        """Итерация регулятора."""
        p, i, d = self._p, self._i, self._d

        self._intergal += err * dt

        res = p * err + i * self._intergal + d * (err - self._last_err) / dt
        self._last_err = res
        return res


pid = PID(0.5 / 5, 0.2 / 5, 0)

pos = 0
target = 1

time = [t / FPS for t in range(0, round(SIMULATE_TIME * FPS) + 1, 1)]
res = [pos]

for t in time[1:]:
    pos += pid.update(target - pos, 1 / FPS)
    res.append(pos)

plt.plot(time, res)
plt.xlabel('time')
plt.ylabel('x(t)')

plt.show()
