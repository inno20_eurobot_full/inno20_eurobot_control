#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Вытащена ли веревка из робота?
"""

import rospy
from eurobot.core import PathResolver

from con_is_robot_activated.srv import IsRobotActivated, IsRobotActivatedRequest, IsRobotActivatedResponse


class IsRobotActivatedNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self._server = rospy.Service("command", IsRobotActivated, self.server_callback)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.logwarn("Mock for condition 'is_robot_activated' is loaded")

    def server_callback(self, req: IsRobotActivatedRequest) -> IsRobotActivatedResponse:
        """Callback for server."""
        return IsRobotActivatedResponse(True)


def main():
    """Main entrypoint."""
    rospy.init_node("is_robot_activated", anonymous=True)

    main = IsRobotActivatedNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
