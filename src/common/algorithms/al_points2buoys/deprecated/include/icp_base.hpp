#pragma once

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/PoseArray.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/fpfh.h>
#include <pcl/registration/icp.h>
#include <pcl/common/transforms.h>
// #include <pcl/visualization/cloud_viewer.h>

#include <al_points2buoys/buoy_algorithm.hpp>

class ICPBase : public BuoyAlgorithm
{
    // void start_node(ros::NodeHandle& nh) override;
private:
    geometry_msgs::PoseArray::Ptr findBouy(const PCLPoints::Ptr) override;

    vectorPCLpointsPtr splitPointCloud(const PCLPoints::Ptr) const;
    geometry_msgs::PoseArray::Ptr calculatePoses(const vectorPCLpointsPtr&) const;

};