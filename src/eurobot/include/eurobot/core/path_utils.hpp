#pragma once

#include <ros/ros.h>
#include <string>


std::string join_paths(const std::string& a, const std::string& b);
std::string join_paths(const std::string& a, const std::string& b, const std::string& c);


class PathResolver
{
private:
    std::string home_path;

public:
    explicit PathResolver(const std::string& default_path = "/") : home_path(default_path) {}
    
    // Read parameter 'home_path'
    void init(ros::NodeHandle& nh);

    // Resolve path
    // If path starts from '~/' then replace in to home_path.
    std::string operator ()(const std::string& path);

    // Return home path
    std::string get_home_path() { return home_path; }
};