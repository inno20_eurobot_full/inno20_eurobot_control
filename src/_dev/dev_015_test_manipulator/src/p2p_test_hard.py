#!/usr/bin/env python3
import rospy
import sys

# Brings in the SimpleActionClient
import actionlib

# goal message and the result message.
from ct_manipulator.msg import ManipulatorP2PAction, ManipulatorP2PGoal
from sensor_msgs.msg import JointState

MOTOR_NAMES = ["motor0", "motor1", "motor2", "motor3", "motor4"]
MOTOR_BASE = ["motor0"]


def feedback_processing(fb):
    """Функция для обработки сигнала Обратной связи (Action Feadback)"""
    print("Server feedback:", fb)


def example_client(name, index):
    """example_client"""
    client = actionlib.SimpleActionClient(
        "/robot1/control/manipulator_p2p/command",
        ManipulatorP2PAction,
    )

    print(name, ": wait for server")
    client.wait_for_server()  # wait until the action server has been started and is listening for goals
    print(name, ": connected")

    # creates a goal to send to the action server
    q = [
        [0, 0, 0, 0, 0],        # 0
        [0, 0, 0, 0, -3.14],    # 1
        [0, 0, 0, -0.5, 0],     # 2
        [0, 0, 0, 1, 0],        # 3
        [-0.8, -0.092, -1.336, 1.425, -0.072],    # 4
        [0.306, 0.821, 1.0787, -1.422, -2.582],     # 5
        [0.306, 0.821, 1.0787, -1.422, -0.072],     # 6
        [0, 1.5, 0.1, 0.1, 1.56],      # 7 finish
        [0, 0, 1.66, -1.67, -1.62],     # 8 start
        [-1.1, -2, 2, 0, 0],        # 9
        [-1, 0, 0.1, 0.1, 1.57],     # 10
        [1, 0, 0.1, 0.1, 1.57],     # 11
        [-1, 0, 0.1, 0.1, 1.57],    # 12
    ]

    goalZero = ManipulatorP2PGoal(JointState(name=MOTOR_NAMES, position=q[0]))

    goal = ManipulatorP2PGoal(
        JointState(name=MOTOR_NAMES if index != 12 else MOTOR_BASE, position=q[index])
    )
    for i in range(10):
        client.send_goal(goal, feedback_cb=feedback_processing)
        client.wait_for_result()  # wait for server to finish the action
        res = client.get_result().done
        rospy.logerr(f": result = {res}")  # print out the result
        rospy.sleep(3)

        client.send_goal(goalZero, feedback_cb=feedback_processing)
        client.wait_for_result()  # wait for server to finish the action
        res = client.get_result().done
        rospy.logerr(f": result = {res}")  # print out the result
        rospy.sleep(3)

    return client.get_result()  # print out the result


if __name__ == "__main__":
    name = "action_client"
    rospy.init_node(name)
    index = int(sys.argv[1]) if len(sys.argv) > 1 else 0
    result = example_client(name, index)
    print(name, ": result = ", result)
