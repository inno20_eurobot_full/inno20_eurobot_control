#include <vector>
#include <string>
#include <al_points2buoys/buoy_algorithm.hpp>
#include <al_points2buoys/projective_icp.hpp>

// Other algorithms are in the deprecated folder
enum AlgorithmsNaming{
    // ransac,
    // icp_point_to_plane,
    // ia_ransac,
    projective_icp,
    // icp_base,
    incorrect_name
};

std::string algorithmNameToStr(AlgorithmsNaming name){
    switch (name){
        // case ransac: return "ransac";
        // case icp_point_to_plane: return "icp_point_to_plane";
        // case ia_ransac: return "ia_ransac";
        case projective_icp: return "projective_icp";
        // case icp_base: return "icp_base";
        default: return "incorrect_name";
    }
}

std::vector<AlgorithmsNaming> allAlgorithmNames(){
    std::vector<AlgorithmsNaming> names;
    // names.push_back(ransac);
    // names.push_back(icp_point_to_plane);
    // names.push_back(ia_ransac);
    names.push_back(projective_icp);
    // names.push_back(icp_base);
    return names;
}

AlgorithmsNaming getAlgorithm(const std::string& nameString){
    std::vector<AlgorithmsNaming> names = allAlgorithmNames();
    for(const auto& name : names)
        if(algorithmNameToStr(name) == nameString)
            return name;
    return incorrect_name;
}

void incorrectName(std::string input_string){
    std::string all_algorithms;
    for(const auto& alg :allAlgorithmNames())
        all_algorithms += " '" +algorithmNameToStr(alg)+"',";

    ROS_FATAL("Unknown algorithm '%s'. Algorithm List:%s", input_string.c_str(), all_algorithms.c_str());
}

int main(int argc, char** argv)
{
    // сheck the number of arguments.
    ROS_ASSERT_MSG(argc > 0, "No algorithm specified");

    // initialize ros
    ros::init(argc, argv, "points2buoy");
    ros::NodeHandle nh;

    std::string nameString(argv[1]);

    ROS_INFO("Start points2buoy with algorithm '%s'", nameString.c_str());

    AlgorithmsNaming algorithm_name = getAlgorithm(nameString);

    BuoyAlgorithm* main;
    switch (algorithm_name){
        // case ransac: 
        //     main = (new RANSACFindCylinder);
        //     break;    
        // case icp_point_to_plane: main = (new ICPPointToPlane); break;
        // case ia_ransac: 
        //     main = (new IA_RANSAC);
        //     break;    
        case projective_icp: 
            main = (new ProjectiveICP);
            break;    
        // case icp_base: 
        //     ROS_WARN("This method doesn't work"); 
        //     main = (new ICPBase); 
        //     break;    
        case incorrect_name: 
            incorrectName(nameString);
            return 0;
    }

    main->start_node(nh);
    main->spin();

    delete main;
    return 0;
}
