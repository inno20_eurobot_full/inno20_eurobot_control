# !/usr/bin/env python3
"""Module for control real robot base via ROS."""

import rospy
import math
import time
from geometry_msgs.msg import Twist, Quaternion
from nav_msgs.msg import Odometry
from dr_maxon_base.motor import init_motors
from dr_maxon_base.odometry import OdometryAlgebra
from dr_maxon_base.utils import quaternion_from_yaw
from dr_maxon_base.srv import SetPower, SetPowerResponse


class _MotorMock:
    def __init__(self):
        self._pos, self._vel, self._ts = 0, 0, 0
        self.enabled = False

    def _sync(self):
        now = time.monotonic()
        if self.enabled:
            self._pos += self._vel * (now - self._ts)
        self._ts = now

    def set_speed(self, vel):
        self._sync()
        self._vel = vel

    def get_position(self):
        self._sync()
        return self._pos

    def disable(self):
        self.enabled = False

    def enable(self):
        self.enabled = True
        self.vel = 0

    def disconnect(self):
        pass


class Control:
    """Class for control real robot base via ROS."""

    def __init__(self):
        """Create node, read parameters, subscribe for topics."""
        rospy.init_node('control')

        self._last_received = rospy.get_time()

        self._timeout = rospy.get_param('cmd_vel_timeout')
        self._rate = rospy.get_param('odometry_rate')
        self._wheel_radius = rospy.get_param('wheel_radius')
        self._wheels_separation = rospy.get_param('wheels_separation')

        self._wheel_circle = 2 * math.pi * self._wheel_radius
        self._wheel_offset = self._wheels_separation * 0.5

        if bool(rospy.get_param('mocked')):
            self._network = None
            self._left = _MotorMock()
            self._right = _MotorMock()
        else:
            self._network, self._left, self._right = init_motors(
                rospy.get_param('~object_dictionary', 'epos4.eds'),
                rospy.get_param('can', {}),
                rospy.get_param('transmit_rate')
            )

        self._odom = OdometryAlgebra(
            self._wheel_circle,
            self._wheel_offset,
            self._left.get_position(),
            self._right.get_position(),
        )

        self._odom_ts = time.monotonic()

        self.sub = rospy.Subscriber('cmd_vel', Twist, self._cmd_vel_callback)
        self.pub = rospy.Publisher('odom', Odometry, queue_size=1)
        self.srv = rospy.Service('set_power', SetPower, self._set_power)

    def _set_power(self, req):
        if req.power:
            self._left.enable()
            self._right.enable()
        else:
            self._left.disable()
            self._right.disable()
        return SetPowerResponse()

    def _cmd_vel_callback(self, cmd_vel):
        self._last_received = rospy.get_time()

        lin, ang = cmd_vel.linear.x, -cmd_vel.angular.z

        left = lin + ang * self._wheel_offset
        right = lin - ang * self._wheel_offset

        self._left.set_speed(left / self._wheel_circle)
        self._right.set_speed(right / self._wheel_circle)

    def _update_odometry(self):
        odom_ts = time.monotonic()
        self._odom.update(
            self._left.get_position(),
            self._right.get_position(),
            odom_ts - self._odom_ts,
        )
        self._odom_ts = odom_ts

        odom = Odometry()
        odom.header.stamp = rospy.Time.now()
        odom.twist.twist.linear.x = self._odom.lin
        odom.twist.twist.angular.z = self._odom.ang
        odom.pose.pose.position.x = self._odom.x
        odom.pose.pose.position.y = self._odom.y
        odom.pose.pose.orientation = Quaternion(
            *quaternion_from_yaw(self._odom.a)
        )

        self.pub.publish(odom)

    def run(self):
        """Main loop."""
        rate = rospy.Rate(self._rate)

        while not rospy.is_shutdown():
            if rospy.get_time() - self._last_received >= self._timeout:
                self._left.set_speed(0)
                self._right.set_speed(0)

            self._update_odometry()

            rate.sleep()

        self._left.disconnect()
        self._right.disconnect()
        if self._network:
            self._network.disconnect()


if __name__ == '__main__':
    control = Control()
    control.run()
