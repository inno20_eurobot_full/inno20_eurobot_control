#pragma once

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/PoseArray.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/features/normal_3d.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/common/transforms.h>
#include <pcl_conversions/pcl_conversions.h>
 
#include <al_points2buoys/buoy_algorithm.hpp>

class RANSACFindCylinder : public BuoyAlgorithm
{
private:
    geometry_msgs::PoseArray::Ptr findBouy(const PCLPoints::Ptr) override;
protected:
    geometry_msgs::Pose doRANSAC(const PCLPoints::Ptr) const;
};