#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Подготовить манипулятор к езде (установить его в такое положение, в котором он должен
находится когда робот едет)
"""

import time
import rospy
from eurobot.core import PathResolver

import actionlib
import ac_prepare_manipulator_for_drive.msg


class PrepareManipulatorForDrive:
    """Wrapper for action."""
    Action = ac_prepare_manipulator_for_drive.msg.PrepareManipulatorForDriveAction
    Feedback = ac_prepare_manipulator_for_drive.msg.PrepareManipulatorForDriveFeedback
    Goal = ac_prepare_manipulator_for_drive.msg.PrepareManipulatorForDriveGoal
    Result = ac_prepare_manipulator_for_drive.msg.PrepareManipulatorForDriveResult


class PrepareManipulatorForDriveNode:
    """Main class."""

    _result = PrepareManipulatorForDrive.Result()

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self._action_server = actionlib.SimpleActionServer(
            "command",
            PrepareManipulatorForDrive.Action,
            execute_cb=self.action_callback,
            auto_start=False)
        self._action_server.start()

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.logwarn("Mock for action 'prepare_manipulator_for_drive' is loaded")

    def action_callback(self, goal: PrepareManipulatorForDrive.Goal):
        """Callback for action."""
        time.sleep(4)
        self._result.res = True
        self._action_server.set_succeeded(self._result)


def main():
    """Main entrypoint."""
    rospy.init_node("prepare_manipulator_for_drive", anonymous=True)

    main = PrepareManipulatorForDriveNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
