#include <behavior_tree/wrap_nodes/actions/gripper_command.h>

BT::PortsList GripperCommand::providedPorts() {
    return {
        BT::InputPort<std::string>("goal")
    };
}

bool GripperCommand::sendGoal(GoalType& goal){
    auto value = getInput<std::string>("goal").value();

    if( value == "CLOSE" ){
        goal.goal = goal.GOAL_CLOSE;
        return true;
    }

    if( value == "OPEN" ){
        goal.goal = goal.GOAL_OPEN;
        return true;
    }

    ROS_ERROR("GripperCommand has incorrect port value '%s'", value.c_str());
    return false;
}

BT::NodeStatus GripperCommand::onResult( const ResultType& res)
{
    if (res.res == true)
        return BT::NodeStatus::SUCCESS;
    return BT::NodeStatus::FAILURE;
}

BT::NodeStatus GripperCommand::onFailedRequest(FailureCause failure)
{
    ROS_ERROR("GripperCommand request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}

void GripperCommand::halt()
{
    if( status() == BT::NodeStatus::RUNNING ) {
        ROS_WARN("GripperCommand aborted(halted)");
        BaseClass::halt();
    }
}