#!/usr/bin/env python3
import rospy

import actionlib

import ac_activate_windsock.msg


def main():
    """Main entrypoint."""
    client = actionlib.SimpleActionClient(
        "/robot1/actions/activate_windsock/command",
        ac_activate_windsock.msg.ActivateWindsockAction,
    )

    print("wait_for_server")
    client.wait_for_server()

    print("send_goal")
    client.send_goal(ac_activate_windsock.msg.ActivateWindsockGoal())

    print("wait_for_result")
    client.wait_for_result()

    print(f"Result: {client.get_result()}")


if __name__ == "__main__":
    rospy.init_node("take_activate_windsock")
    main()
