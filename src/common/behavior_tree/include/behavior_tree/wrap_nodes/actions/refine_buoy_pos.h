#pragma once

#include <ros/ros.h>
#include <behavior_tree/node_template/bt_condition_node.h>
#include <ac_refine_buoy_pos/RefineBuoyPos.h>

#include <string>

class RefineBuoyPos: public BT::RosServiceNode<ac_refine_buoy_pos::RefineBuoyPos>
{
public:
    RefineBuoyPos( ros::NodeHandle& handle, const std::string& node_name, const std::string& server_name, const BT::NodeConfiguration & conf)
        :RosServiceNode<ac_refine_buoy_pos::RefineBuoyPos>(handle, node_name, server_name, conf) {}

    static BT::PortsList providedPorts();

    void sendRequest(RequestType& request) override;

    BT::NodeStatus onResponse(const ResponseType& rep) override;

    BT::NodeStatus onFailedRequest(RosServiceNode::FailureCause failure) override;
};