# ds_models

В пакете храняться модели для проекта Eurobot2021

## Usefull links

[Compute inertia via Meshlab](http://gazebosim.org/tutorials?tut=inertia&cat=build_robot)

## Model zoo

### eurobot_2020_field

Модель статической части поля. Имеется текстура двух размеров 3000x2000 и 6000x4000.

![eurobot_2020_field](models/eurobot_2020_field/model.png)

### eurobot_2020_buoy

Модель буйка. Не имеет цвета

![eurobot_2020_buoy](models/eurobot_2020_buoy/model.png)

### eurobot_2020_buoy_red

Модель буйка красного цвета

![eurobot_2020_buoy_red](models/eurobot_2020_buoy_red/model.png)

### eurobot_2020_buoy_green

Модель буйка зеленого цвета

![eurobot_2020_buoy_green](models/eurobot_2020_buoy_green/model.png)