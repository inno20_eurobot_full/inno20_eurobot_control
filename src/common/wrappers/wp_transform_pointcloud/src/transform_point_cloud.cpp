#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/TransformStamped.h>
#include <pcl_conversions/pcl_conversions.h>
#include <tf2_sensor_msgs/tf2_sensor_msgs.h>

#include <eurobot/core/path_utils.hpp>

class PCFlipper
{
  const double WARNING_TIME = 5;   

public:
  explicit PCFlipper(ros::NodeHandle&  nh);
  void checkStatus();
  
private:
  void pcCallback(const sensor_msgs::PointCloud2ConstPtr&);
  void positionCallback(const geometry_msgs::TransformStamped::ConstPtr&);

  bool pos_state = true;
  double pos_time;
  bool point_state = true;
  double point_time;

  ros::NodeHandle node_handle;
  ros::Publisher flipped_point_pub;
  ros::Subscriber point_sub;
  ros::Subscriber position_sub;

  bool getting_first_pos = false;
  geometry_msgs::TransformStamped cam_transform;
  std::string frame_name;
};

PCFlipper::PCFlipper(ros::NodeHandle& nh)
  :node_handle(nh),pos_time(ros::Time::now().toSec()),point_time(ros::Time::now().toSec())
{
  PathResolver resolve_path;
  resolve_path.init(nh);

  std::string points_topic_name, pos_topic_name;

  node_handle.getParam("input_point_topic", points_topic_name);
  node_handle.getParam("camera_pos_topic",  pos_topic_name);
  node_handle.getParam("frame_name",  frame_name);
  
  points_topic_name = resolve_path(points_topic_name);
  pos_topic_name = resolve_path(pos_topic_name);
  frame_name = resolve_path(frame_name).substr(1);

  ROS_INFO("Point cloud flipper start"); 

  flipped_point_pub = node_handle.advertise<sensor_msgs::PointCloud2>("points_flip", 1);
  // ROS_INFO("set Flipped PointCloud publisher: %s ",flipped_point_pub.getTopic().c_str() );

  point_sub = node_handle.subscribe<sensor_msgs::PointCloud2>(points_topic_name, 1, &PCFlipper::pcCallback, this);
  // ROS_INFO("set PointCloud listener: %s ",point_sub.getTopic().c_str() );

  position_sub = node_handle.subscribe<geometry_msgs::TransformStamped>(pos_topic_name, 1, &PCFlipper::positionCallback, this);
  // ROS_INFO("set Realsense position: %s ",position_sub.getTopic().c_str() );
}


void PCFlipper::checkStatus(){   // check topics
  double cur_time = ros::Time::now().toSec();

  if(pos_state && (cur_time - pos_time > WARNING_TIME)){
    ROS_WARN("Waiting for position publisher(%s)",position_sub.getTopic().c_str());
    pos_state = false;
  }

  if(point_state && (cur_time - point_time > WARNING_TIME)){
    ROS_WARN("Waiting for pointCloud publisher(%s)",point_sub.getTopic().c_str());
    point_state = false;
  }
}


void PCFlipper::positionCallback(const geometry_msgs::TransformStamped::ConstPtr& T){ // update position
  getting_first_pos = true;

  if(!pos_state)
    ROS_INFO("Connected to position publisher");
  pos_state = true;
  pos_time  = ros::Time::now().toSec();

  cam_transform = *T;
  cam_transform.header.frame_id = frame_name;
}

void PCFlipper::pcCallback(const sensor_msgs::PointCloud2ConstPtr& IN_PC2){ // transform pointcloud
  if(!point_state)
    ROS_INFO("Connected to pointCloud publisher");
  point_state = true;
  point_time  = ros::Time::now().toSec();
  if(!getting_first_pos || !pos_state)return;

  sensor_msgs::PointCloud2 OUT_PC2;
  cam_transform.child_frame_id = IN_PC2->header.frame_id;
  tf2::doTransform(*IN_PC2, OUT_PC2, cam_transform);
  flipped_point_pub.publish(OUT_PC2);
}


int main(int argc, char** argv){
	ros::init(argc, argv, "pc_flipper");
  ros::NodeHandle n;
	PCFlipper pc_flipper(n);
  
  while (ros::ok()){
    pc_flipper.checkStatus();  
    ros::getGlobalCallbackQueue()->callAvailable(ros::WallDuration(5));
	}

  return 0;
}
