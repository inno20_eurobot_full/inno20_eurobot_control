#!/usr/bin/env python3

import sys
import rospy
from al_manipulator.srv import ManipulatorFK, ManipulatorIK, ManipulatorH
from al_manipulator.msg import ManipulatorTF, ManipulatorEndPos
from sensor_msgs.msg import JointState
from ct_manipulator.msg import ManipulatorP2PAction, ManipulatorP2PGoal
import numpy as np

# Brings in the SimpleActionClient
import actionlib

MOTOR_NAMES = ["motor0", "motor1", "motor2", "motor3", "motor4"]


def feedback_processing(fb):
    """Callback for JointState"""
    print("p2p feedback:", fb)


def main():
    """Main entrypoint."""
    rospy.init_node("test_H_in_ik", anonymous=True)

    rospy.wait_for_service("/robot1/algorithms/manipulator_fk/calc_fk")
    rospy.wait_for_service("/robot1/algorithms/manipulator_ik/calc_ik")
    rospy.wait_for_service("/robot1/algorithms/manipulator_h/calc_h")

    calc_fk = rospy.ServiceProxy(
        "/robot1/algorithms/manipulator_fk/calc_fk", ManipulatorFK
    )
    calc_ik = rospy.ServiceProxy(
        "/robot1/algorithms/manipulator_ik/calc_ik", ManipulatorIK
    )
    calc_h = rospy.ServiceProxy(
        "/robot1/algorithms/manipulator_h/calc_h", ManipulatorH
    )
    p2p = actionlib.SimpleActionClient(
        "/robot1/control/manipulator_p2p/command",
        ManipulatorP2PAction,
    )
    p2p.wait_for_server()

    x, y, z, r, p = 0.38, 0, 0.12, 0, 0
    if len(sys.argv) > 3:
        x = float(sys.argv[1])
        y = float(sys.argv[2])
        z = float(sys.argv[3])
    if len(sys.argv) > 4:
        r = float(sys.argv[4])
    if len(sys.argv) > 5:
        p = float(sys.argv[5])

    # motor_poses = [0.0, 0.7126371281533739,-2.157387288001584, 1.44475015984821 , 0.0]
    # motor_poses = [0.0, 0.7126371281533739, -0.712637128153373, 1.44475015984821 , 1.57]
    # H_init = calc_fk(JointState(name=MOTOR_NAMES, position=motor_poses)).res.data
    # print()
    # for i in [0, 1, 2, 3]:
    #     print(
    #         "[%.5f, %.5f, %.5f, %.5f]"
    #         % (H_init[i * 4], H_init[i * 4 + 1], H_init[i * 4 + 2], H_init[i * 4 + 3])
    #     )
    # print()
    pos = ManipulatorEndPos(x=x, y=y, z=z, roll=r, pitch=p)
    H = calc_h(pos).res.data
    print("for pos = ", pos)
    print("H:")
    for i in [0, 1, 2, 3]:
        print(
            "[%.5f, %.5f, %.5f, %.5f]"
            % (H[i * 4], H[i * 4 + 1], H[i * 4 + 2], H[i * 4 + 3])
        )
    print()
    # H = H_init

    ik = calc_ik(ManipulatorTF(H)).res
    for sol in ik:
        print(sol.name)
        print(sol.position)
        print()

    qs = [i.position for i in ik]
    assert len(qs) in [2, 4], "Количество решений не 2 и не 4"

    for qsi in qs:
        cH = calc_fk(JointState(name=MOTOR_NAMES, position=qsi)).res
        assert (np.array(cH.data) - np.array(H) < 1e-10).all()

    p2p.send_goal(ManipulatorP2PGoal(ik[1]), feedback_cb=feedback_processing)
    p2p.wait_for_result()  # wait for server to finish the action
    print("test_H_in_ik", ": result = ", p2p.get_result().done)


if __name__ == "__main__":
    main()
