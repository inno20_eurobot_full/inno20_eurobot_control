#include <behavior_tree/wrap_nodes/actions/get_closest_bouy.h>
#include <geometry_msgs/PoseStamped.h>

BT::PortsList GetClosestBuoy::providedPorts()
{
    return {
        BT::OutputPort<geometry_msgs::PoseStamped>("pos")
    };
}

void GetClosestBuoy::sendRequest(RequestType& request)
{
}

BT::NodeStatus GetClosestBuoy::onResponse(const ResponseType& rep)
{
    setOutput("pos", rep.pos);
    if (rep.done == false)
        return BT::NodeStatus::FAILURE;
    return BT::NodeStatus::SUCCESS;
}

BT::NodeStatus GetClosestBuoy::onFailedRequest(RosServiceNode::FailureCause failure)
{
    ROS_ERROR("GetClosestBuoy request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}