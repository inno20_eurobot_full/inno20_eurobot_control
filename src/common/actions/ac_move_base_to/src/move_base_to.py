#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Доехать до определенной позиции
"""

import rospy
from eurobot.core import PathResolver

import actionlib
from ac_move_base_to.msg import (
    MoveBaseToAction,
    MoveBaseToFeedback,
    MoveBaseToGoal,
    MoveBaseToResult,
)
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal

import tf2_geometry_msgs as tf2_g
import tf2_ros


class MoveBaseTo:
    """Wrapper for action."""

    Action = MoveBaseToAction
    Feedback = MoveBaseToFeedback
    Goal = MoveBaseToGoal
    Result = MoveBaseToResult


class MoveBaseToNode:
    """Main class."""

    _result = MoveBaseTo.Result()

    def __init__(self):
        """Create main class."""
        resolve_path = PathResolver()
        move_to_name = resolve_path(rospy.get_param("move_base"))

        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)

        self.move_to = actionlib.SimpleActionClient(move_to_name, MoveBaseAction)

        self._action_server = actionlib.SimpleActionServer(
            "command",
            MoveBaseTo.Action,
            execute_cb=self.action_callback,
            auto_start=False,
        )

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.loginfo("Main for action 'move_base_to' is loaded")
        self.move_to.wait_for_server()
        self._action_server.start()

    def action_callback(self, goal: MoveBaseTo.Goal):
        """Callback for action."""
        try:
            trans = self.tfBuffer.lookup_transform(
                "map", goal.pos.header.frame_id, rospy.Time(0)
            )
            goal.pos = tf2_g.do_transform_pose(goal.pos, trans)
            goal.pos.header.frame_id = "map"
        except (
            tf2_ros.LookupException,
            tf2_ros.ConnectivityException,
            tf2_ros.ExtrapolationException,
        ):
            rospy.logwarn(
                f"move_base_to: no transform from '{self.robot_frame}' to '{self.in_buoy_frame}'"
            )

            self._result.res = False
            self._action_server.set_succeeded(self._result)
            return

        move_goal = MoveBaseGoal()
        move_goal.target_pose.header.frame_id = "map"
        move_goal.target_pose.header.stamp = rospy.Time.now()
        move_goal.target_pose.pose.position = goal.pos.pose.position
        move_goal.target_pose.pose.orientation = goal.pos.pose.orientation
        rospy.logwarn(
            f"pos:{move_goal.target_pose.pose.position}, quat: {move_goal.target_pose.pose.orientation}"
        )
        self.move_to.send_goal(move_goal)

        check_rate = 50
        while (
            not self._action_server.is_preempt_requested() and not rospy.is_shutdown()
        ):
            if self.move_to.wait_for_result(rospy.Duration(1 / check_rate)):
                status_text = self.move_to.get_goal_status_text()
                status = self.move_to.get_state()
                rospy.loginfo(
                    f"MoveBaseTo: goal execution done, status={status}, status_text={status_text}"
                )
                #  (status == 3)
                self._result.res = True
                self._action_server.set_succeeded(self._result)
                return

        if not rospy.is_shutdown():
            rospy.logwarn("MoveBaseTo: is preempt")
            self.move_to.cancel_all_goals()
            self._action_server.set_preempted()

        self._result.res = False
        self._action_server.set_succeeded(self._result)


def main():
    """Main entrypoint."""
    rospy.init_node("move_base_to", anonymous=True)

    main = MoveBaseToNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
