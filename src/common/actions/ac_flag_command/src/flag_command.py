#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Изменить состояние флага (поднять его или опустить)
"""

import rospy
from eurobot.core import PathResolver

import actionlib
import ac_flag_command.msg

from std_msgs.msg import Float32


class FlagCommand:
    """Wrapper for action."""
    Action = ac_flag_command.msg.FlagCommandAction
    Feedback = ac_flag_command.msg.FlagCommandFeedback
    Goal = ac_flag_command.msg.FlagCommandGoal
    Result = ac_flag_command.msg.FlagCommandResult


class FlagCommandNode:
    """Main class."""

    _result = FlagCommand.Result()

    def __init__(self):
        """Create main class."""
        resolve_path = PathResolver()

        topic = resolve_path(rospy.get_param("topic"))
        self.Lower = rospy.get_param("lower")
        self.Raise = rospy.get_param("raise")

        self.pub = rospy.Publisher(topic, Float32, queue_size=1)
        self._action_server = actionlib.SimpleActionServer(
            "command",
            FlagCommand.Action,
            execute_cb=self.action_callback,
            auto_start=False)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.loginfo("Main for action 'flag_command' is loaded")
        self._action_server.start()

    def action_callback(self, goal: FlagCommand.Goal):
        """Callback for action."""
        if goal.goal == goal.GOAL_RAISE:
            self.pub.publish(self.Raise)
            self._result.res = True
        elif goal.goal == goal.GOAL_LOWER:
            self.pub.publish(self.Lower)
            self._result.res = True
        else:
            rospy.logwarn(f"unexpected cmd: {goal.goal}, list:{goal.GOAL_RAISE, goal.GOAL_LOWER}")
            self._result.res = False
        self._action_server.set_succeeded(self._result)


def main():
    """Main entrypoint."""
    rospy.init_node("flag_command", anonymous=True)

    main = FlagCommandNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
