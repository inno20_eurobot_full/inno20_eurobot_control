#pragma once

#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <al_manipulator/ManipulatorTF.h>
#include <al_manipulator/ManipulatorIK.h>

#include <vector>
#include <string>

#include <al_manipulator/dh_params.hpp>
#include <eurobot/core/path_utils.hpp>


class ManipulatorIK
{
private:
    PathResolver resolve_path;

    std::vector<std::string> motor_names;

    bool service_callback(al_manipulator::ManipulatorIK::Request& req, al_manipulator::ManipulatorIK::Response& res);

    bool load_dh_params(const ros::NodeHandle& nh, DHParams* dh_params);
    bool build_manipulator(const DHParams& dh);
    void norm_and_push_back(std::vector<sensor_msgs::JointState>& res, const std::vector<double>& pos);

protected:
    DHParams dh_params;
    ros::ServiceServer service;

    virtual bool start_service(ros::NodeHandle& nh);
    
    bool process(const al_manipulator::ManipulatorTF& H, std::vector<sensor_msgs::JointState>& res);

public:
    virtual bool start_node(ros::NodeHandle& nh);

    void spin();
};