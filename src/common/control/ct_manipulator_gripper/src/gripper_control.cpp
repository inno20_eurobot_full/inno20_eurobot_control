#include <ct_manipulator_gripper/gripper_control.hpp>

#include <eurobot/core/path_utils.hpp>
#include <eurobot/utils/ros/get_param_list_with_log_error.hpp>

GripperControl::GripperControl(ros::NodeHandle& nh)
	:server(nh, "gripper_control", boost::bind(&GripperControl::execute, this, _1), false)
{
	service = nh.advertiseService("get_state", &GripperControl::service_callback, this);
	resolve_path.init(nh);
	takeParameters(nh);
}
bool GripperControl::getSrvState(ServiceResponse& res)
{	
	auto& res_state = res.state;
	switch (gripper_state)
	{
	case GripperState::STATE_UNDEFINED:
		res_state = ServiceResponse::STATE_UNDEFINED;
		return true;
	case GripperState::STATE_CLOSE:
		res_state = ServiceResponse::STATE_CLOSE;
		return true;
	case GripperState::STATE_OPEN:
		res_state = ServiceResponse::STATE_OPEN;
		return true;
	case GripperState::STATE_WITH_BUOY:
		res_state = ServiceResponse::STATE_WITH_BUOY;
		return true;
	default: break;
	}
	return false;
}
bool GripperControl::service_callback(ServiceRequest& req, ServiceResponse& res)
{
	return getSrvState(res);
}

/*
 *	Забирает параметры из конфигов
 */
void GripperControl::takeParameters(ros::NodeHandle& nh){

	const char* param_name;

	// Required parameters:

	//position_topic
	param_name = "position_topic/path";
	ROS_ASSERT_MSG(nh.getParam(param_name, pos_topic.PATH), "Parameter '%s' not found", param_name);

	param_name = "position_topic/motor_name";
	ROS_ASSERT_MSG(nh.getParam(param_name, pos_topic.MOTOR_NAME), "Parameter '%s' not found", param_name);

	//effort_topic
	param_name = "effort_topic/path";
	ROS_ASSERT_MSG(nh.getParam(param_name, eff_topic.PATH), "Parameter '%s' not found", param_name);



	// Optional parameters:
	// set default value (third argument) if the parameter could not be retrieved
	
	//power
	param_name = "power/max";
	if (!nh.param <double> (param_name, power.MAX, power.MAX))
		ROS_WARN ("Parameter '%s' not found. Will be substituted with the default value", param_name);

	param_name = "power/min";
	if (!nh.param <double> (param_name, power.MIN, power.MIN))
		ROS_WARN ("Parameter '%s' not found. Will be substituted with the default value", param_name);

	//pos
	param_name = "pos/open";
	if (!nh.param <double> (param_name, pos.OPEN, pos.OPEN))
		ROS_WARN ("Parameter '%s' not found. Will be substituted with the default value", param_name);
	
	param_name = "pos/middle";
	if (!nh.param <double> (param_name, pos.MIDDLE, pos.MIDDLE))
		ROS_WARN ("Parameter '%s' not found. Will be substituted with the default value", param_name);

	param_name = "pos/close_with_buoy_max";
	if (!nh.param <double> (param_name, pos.CLOSE_WITH_BUOY_MAX, pos.CLOSE_WITH_BUOY_MAX))
		ROS_WARN ("Parameter '%s' not found. Will be substituted with the default value", param_name);
	
	param_name = "pos/close_with_buoy_min";
	if (!nh.param <double> (param_name, pos.CLOSE_WITH_BUOY_MIN, pos.CLOSE_WITH_BUOY_MIN))
		ROS_WARN ("Parameter '%s' not found. Will be substituted with the default value", param_name);
	
	param_name = "pos/close";
	if (!nh.param <double>  (param_name, pos.CLOSE, pos.CLOSE))
		ROS_WARN ("Parameter '%s' not found. Will be substituted with the default value", param_name);

	//config
	param_name = "execute_param/epsilon";
	if (!nh.param <double>  (param_name, config.EPSILON, config.EPSILON))
		ROS_WARN ("Parameter '%s' not found. Will be substituted with the default value", param_name);

	param_name = "execute_param/magnitude";
	if (!nh.param <double>  (param_name, config.MAGNITUDE, config.MAGNITUDE))
		ROS_WARN ("Parameter '%s' not found. Will be substituted with the default value", param_name);

	param_name = "execute_param/time_limit";
	if (!nh.param <double>  (param_name, config.TIME_LIMIT, config.TIME_LIMIT))
		ROS_WARN ("Parameter '%s' not found. Will be substituted with the default value", param_name);

	param_name = "execute_param/buffer_size";
	if (!nh.param <int>  (param_name, config.BUFFER_SIZE, config.BUFFER_SIZE))
		ROS_WARN ("Parameter '%s' not found. Will be substituted with the default value", param_name);

	pos_topic.PATH = resolve_path(pos_topic.PATH);
	eff_topic.PATH = resolve_path(eff_topic.PATH);

}

void GripperControl::start_node(ros::NodeHandle& nh)
{
	ROS_INFO("Starting ct_manipulator_gripper_control");
	sub_position = nh.subscribe(pos_topic.PATH, 1, &GripperControl::updatePosition, this);
	pub_effort = nh.advertise <std_msgs::Float32> (eff_topic.PATH, 1);
	server.start();
}

void GripperControl::updatePosition(const sensor_msgs::JointStateConstPtr& data){
	for (int i = 0; i < data->name.size(); i++) {
		if (data->name[i] != pos_topic.MOTOR_NAME) continue;
		pos.curr_pos = data->position[i];
		return;
	}
	ROS_WARN("target motor not found");
}

void GripperControl::execute(const ct_manipulator_gripper::GripperControlGoalConstPtr& goal)
{
	ct_manipulator_gripper::GripperControlResult result;
	bool success = false;
	result.res = true;

	ROS_DEBUG("Executing %s gripper", (goal->goal == goal->GOAL_CLOSE)?"closing":"opening");
	
	double target_pos = (goal->goal == goal->GOAL_CLOSE)? pos.CLOSE : pos.OPEN;
	std::vector<double> cycle_buffer(config.BUFFER_SIZE,-1);
	std_msgs::Float32 effort;
	ros::Rate r(50);

	const auto start_time = ros::Time::now();
	for (size_t i = 0; !server.isPreemptRequested() && ros::ok(); i = (i+1) % cycle_buffer.size()) {

		auto last_pos = cycle_buffer.at(i);
		cycle_buffer.at(i) = pos.curr_pos; 

		auto cur_time = ros::Time::now();
        const double dt = (cur_time - start_time).toSec();

		double delta = target_pos - pos.curr_pos;
		if (goal->goal == goal->GOAL_CLOSE)
			delta = -delta;
	
		if( delta < config.EPSILON      				    ||
			abs(pos.curr_pos - last_pos) < config.MAGNITUDE ||
			dt > config.TIME_LIMIT
		){  
			success = true;
			if (goal->goal == goal->GOAL_OPEN)
				gripper_state = GripperState::STATE_OPEN;
			if (goal->goal == goal->GOAL_CLOSE)
				gripper_state = GripperState::STATE_CLOSE;
			if ((pos.CLOSE_WITH_BUOY_MAX > pos.curr_pos) && (pos.curr_pos > pos.CLOSE_WITH_BUOY_MIN) && (goal->goal == goal->GOAL_CLOSE) )
				gripper_state = GripperState::STATE_WITH_BUOY;
			break;
		}

		effort.data = (getPower(goal->goal == goal->GOAL_CLOSE));
		pub_effort.publish(effort);
		//###########################
		r.sleep();
	}

	ROS_DEBUG("Fineshed %s gripper", (goal->goal == goal->GOAL_CLOSE)?"closing":"opening");

	effort.data = power.NO_POWER;
	if (gripper_state == GripperState::STATE_WITH_BUOY){
		ROS_DEBUG("Buoy is grabbed successfully");
		effort.data = -power.MIN;
	}
	pub_effort.publish(effort);
	
	if (!success){
		ROS_WARN("Preempted");
		gripper_state = GripperState::STATE_UNDEFINED; 
		result.res = false;
		server.setPreempted();
		return;
	}
	server.setSucceeded(result);
}

double GripperControl::getPower(bool closing) const
{
	if (closing)
	{
		// closing
		if (pos.curr_pos > pos.MIDDLE) return -power.MAX;
		if (pos.curr_pos > pos.CLOSE_WITH_BUOY_MAX) {
			// normalized coefficient 
			double res = (pos.curr_pos - pos.CLOSE_WITH_BUOY_MAX) / (pos.MIDDLE - pos.CLOSE_WITH_BUOY_MAX); 
			res = power.MIN + res*res*(power.MAX -power.MIN); 
			return -res;
		}
		if (pos.curr_pos > pos.CLOSE) return -power.MIN;
		return -power.NO_POWER;
		
	} else {
		// opening
		if (pos.curr_pos < pos.MIDDLE) return power.MAX;
		if (pos.curr_pos < pos.OPEN) {
			// normalized coefficient 
			double res = (pos.OPEN - pos.curr_pos) / (pos.OPEN - pos.MIDDLE); 
			return  power.MIN + res*res*(power.MAX - power.MIN); 
		}
		return power.NO_POWER;
	}
};

void GripperControl::spin()
{
	ros::spin();
}

