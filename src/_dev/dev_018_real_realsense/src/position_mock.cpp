#include <ros/ros.h>
#include <geometry_msgs/TransformStamped.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "position_mock");
    ros::NodeHandle nh;

    double x,y,z, base;
    nh.getParam("/robot1/description/general/tof_camera_pos/x", x);
    nh.getParam("/robot1/description/general/tof_camera_pos/y", y);
    nh.getParam("/robot1/description/general/tof_camera_pos/z", z);
    nh.getParam("/robot1/description/general/manipulator_base_height", base);

    ros::Publisher mock_pos_pub = nh.advertise<geometry_msgs::TransformStamped>("/robot1/description/realsense/pos", 1);

    ros::Rate loop_rate(30);
    while (ros::ok()){
        geometry_msgs::TransformStamped pos;
        auto& trans = pos.transform.translation;
        auto& rotate = pos.transform.rotation;
        trans.x = x;//0.123;
        trans.y = y;//0.112;
        trans.z = z + base;//0.027 + 0.079;
        rotate.w = 0.5;
        rotate.x = -0.5;
        rotate.y = 0.5;
        rotate.z = -0.5;

        mock_pos_pub.publish(pos);

        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}