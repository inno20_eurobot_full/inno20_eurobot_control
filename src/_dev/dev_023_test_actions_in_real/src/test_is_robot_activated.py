#!/usr/bin/env python3
import rospy

from con_is_robot_activated.srv import IsRobotActivated


def main():
    """Main entrypoint."""
    client = rospy.ServiceProxy(
        "/robot1/conditions/is_robot_activated/command",
        IsRobotActivated
    )

    print("send_goal")
    result = client()

    print(f"Result: {result}")


if __name__ == "__main__":
    rospy.init_node("is_buoy_reachable")
    main()
