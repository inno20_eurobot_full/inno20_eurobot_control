#pragma once

#include <ros/ros.h>
#include <ros/topic.h>
#include <behaviortree_cpp_v3/condition_node.h>

#include <vector>
#include <string>

class IsListOfNodesExist : public BT::ConditionNode
{
    const ros::V_string&  action_list_;
    const ros::V_string& service_list_;
    ros::NodeHandle& node_;
    IsListOfNodesExist(ros::NodeHandle& nh, const std::string& name,const BT::NodeConfiguration & conf,
                        const ros::V_string& action_list, const ros::V_string& service_list);
public:
    IsListOfNodesExist()  = delete;
    ~IsListOfNodesExist() = default;
    BT::NodeStatus tick() override; 
};

IsListOfNodesExist::IsListOfNodesExist(ros::NodeHandle& nh, const std::string& name,const BT::NodeConfiguration & conf,
                        const ros::V_string& action_list, const ros::V_string& service_list)
    : BT::ConditionNode::ConditionNode(name, conf), action_list_(action_list), service_list_(service_list), node_(nh)
{ }

BT::NodeStatus IsListOfNodesExist::tick()
{
    for (const auto& path : action_list_)
    return BT::NodeStatus::SUCCESS;
}