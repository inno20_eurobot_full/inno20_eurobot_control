<?xml version="1.0"?>
<robot name="robot1" xmlns:xacro="http://www.ros.org/wiki/xacro">
    <xacro:macro name="robot1" params="team aruco_no model plugins">
        <!-- Define parameters -->
        <xacro:if value="${team == 'blue'}">
            <xacro:property name="team_color" value="Eurobot/Blue"/>
        </xacro:if>
        <xacro:if value="${team == 'yellow'}">
            <xacro:property name="team_color" value="Eurobot/Yellow"/>
        </xacro:if>

        <!-- Define motors -->
        <xacro:property name="base_motors" value="${dict(motorR='base_motorR', motorL='base_motorL')}"/>
        <xacro:property name="flag_motors" value="${dict(flag_motor='flag_motor')}"/>
        <xacro:property name="manipulator_motors" value="${dict(motor0='manipulator_motor0', motor1='manipulator_motor1', motor2='manipulator_motor2', motor3='manipulator_motor3', motor4='manipulator_motor4', gripper0='manipulator_gripper0', gripper1='manipulator_gripper1')}"/>

        <!-- Configs -->
        <xacro:property name="general" value="${load_yaml('$(find robot1_description)/config/general.yaml')}"/>
        <xacro:property name="cfg_base" value="${load_yaml('$(find robot1_description)/config/base.yaml')}"/>
        <xacro:property name="cfg_middle" value="${load_yaml('$(find robot1_description)/config/middle.yaml')}"/>
        


        <!-- Building: base -->
        <xacro:property name="spawn_base" value="1"/>
        <xacro:if value="${spawn_base}">
            <xacro:include filename="$(find robot1_description)/urdf/base.xacro"/>
            <xacro:base base_link="base" cfg="${cfg_base}" motors_name="${base_motors}"/>
        </xacro:if>

        <!-- Building: middle -->
        <xacro:property name="spawn_middle" value="${spawn_base and model['middle']}"/>
        <xacro:if value="${spawn_middle}">
            <xacro:include filename="$(find robot1_description)/urdf/middle.xacro"/>
            <xacro:middle base_link="middle" cfg="${cfg_middle}"/>
            <joint name="base-middle" type="fixed">
                <parent link="base"/>
                <child link="middle"/>
                <origin xyz="0 0 ${cfg_base['height']/2 + cfg_middle['height']/2}" rpy="0 0 0"/>
            </joint>
        </xacro:if>

        <!-- Building: manipulator -->
        <xacro:property name="spawn_manipulator" value="${spawn_base and model['manipulator']}"/>
        <xacro:if value="${spawn_manipulator}">
            <xacro:include filename="$(find robot1_description)/urdf/manipulator.xacro"/>
            <xacro:manipulator base_link="manipulator" motors_name="${manipulator_motors}"/>
            <joint name="base-manipulator" type="fixed">
                <parent link="base"/>
                <child link="manipulator"/>
                <origin xyz="0 0 ${general['manipulator_base_height'] - cfg_base['wheel_big']['radius']}" rpy="0 0 0"/>
            </joint>
        </xacro:if>

        <!-- Building: realsense -->
        <xacro:property name="spawn_realsense" value="${spawn_manipulator and model['realsense']}"/>
        <xacro:if value="${spawn_realsense}">
            <xacro:include filename="$(find robot1_description)/urdf/realsense.xacro"/>
            <xacro:realsense base_link="realsense"/>
            <joint name="mainpulator-realsense" type="fixed">
                <parent link="${manipulator_motors['motor0']}Wrap"/>
                <child link="realsense"/>
                <origin xyz="${general['tof_camera_pos']['x']} ${general['tof_camera_pos']['y']} ${general['tof_camera_pos']['z']}"
                        rpy="${general['tof_camera_pos']['rotx']} ${general['tof_camera_pos']['roty']} ${general['tof_camera_pos']['rotz']}"/>
            </joint>
        </xacro:if>

        <!-- Building: flag -->
        <xacro:property name="spawn_flag" value="${spawn_base and model['flag']}"/>
        <xacro:if value="${spawn_flag}">
            <xacro:include filename="$(find robot1_description)/urdf/flag.xacro"/>
            <xacro:flag base_link="flag" motors_name="${flag_motors}"/>
            <joint name="base-flag" type="fixed">
                <parent link="base"/>
                <child link="flag"/>
                <origin xyz="${-cfg_base['radius'] - 0.01} 0 0" rpy="0 0 0"/>
            </joint>
        </xacro:if>

        <!-- Building: aruco_top -->
        <xacro:property name="spawn_aruco_top" value="${spawn_base and model['aruco_top']}"/>
        <xacro:if value="${spawn_aruco_top}">
            <xacro:include filename="$(find robot1_description)/urdf/aruco_top.xacro"/>
            <xacro:aruco_top base_link="aruco" color="${team_color}" aruco_material="Eurobot/Aruco_${aruco_no}"/>
            <joint name="base-aruco" type="fixed">
                <parent link="base"/>
                <child link="aruco"/>
                <origin xyz="0 0 ${cfg_base['height'] / 2 + cfg_middle['height']}" rpy="0 0 0"/>
            </joint>
        </xacro:if>

        <!-- Building: imu -->
        <xacro:property name="spawn_imu" value="${spawn_base and model['imu']}"/>
        <xacro:if value="${spawn_imu}">
            <xacro:include filename="$(find robot1_description)/urdf/imu.xacro"/>
            <xacro:imu base_link="imu"/>
            <joint name="base-imu" type="fixed">
                <parent link="base"/>
                <child link="imu"/>
                <origin xyz="0 0 0" rpy="0 0 0"/>
            </joint>
        </xacro:if>




        <!-- Load drivers: ros_control_1 -->
        <xacro:if value="${plugins['ros_control_1']}">
            <xacro:include filename="$(find robot1_drivers)/urdf/ros_control_1.xacro"/>
            <xacro:dr_ros_control_1/>
        </xacro:if>

        <!-- Load drivers: gazebo_imu_1 -->
        <xacro:if value="${spawn_imu and plugins['gazebo_imu_1']}">
            <xacro:include filename="$(find robot1_drivers)/urdf/gazebo_imu_1.xacro"/>
            <xacro:dr_gazebo_imu_1 link_name="imu"/>
        </xacro:if>

        <!-- Load drivers: gazebo_realsense_1 -->
        <xacro:if value="${spawn_realsense and plugins['gazebo_realsense_1']}">
            <xacro:include filename="$(find robot1_drivers)/urdf/gazebo_realsense_1.xacro"/>
            <xacro:dr_gazebo_realsense_1 link_name="realsense"/>
        </xacro:if>
    </xacro:macro>
</robot>
