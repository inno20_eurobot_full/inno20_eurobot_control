#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Есть ли сейчас в гриппере буек?
"""

import rospy
from eurobot.core import PathResolver

from con_is_buoy_in_gripper.srv import IsBuoyInGripper, IsBuoyInGripperRequest, IsBuoyInGripperResponse


class IsBuoyInGripperNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self._server = rospy.Service("command", IsBuoyInGripper, self.server_callback)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.logwarn("Mock for condition 'is_buoy_in_gripper' is loaded")

    def server_callback(self, req: IsBuoyInGripperRequest) -> IsBuoyInGripperResponse:
        """Callback for server."""
        return IsBuoyInGripperResponse(True)


def main():
    """Main entrypoint."""
    rospy.init_node("is_buoy_in_gripper", anonymous=True)

    main = IsBuoyInGripperNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
