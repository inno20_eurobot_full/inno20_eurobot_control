#pragma once

#include <ros/ros.h>
#include <behavior_tree/node_template/bt_condition_node.h>
#include <ac_set_power_state/SetPowerState.h>

#include <string>

class SetPowerState: public BT::RosServiceNode<ac_set_power_state::SetPowerState>
{
public:
    SetPowerState( ros::NodeHandle& handle, const std::string& node_name, const std::string& server_name, const BT::NodeConfiguration & conf)
        :RosServiceNode<ac_set_power_state::SetPowerState>(handle, node_name, server_name, conf) {}

    static BT::PortsList providedPorts();

    void sendRequest(RequestType& request) override;

    BT::NodeStatus onResponse(const ResponseType& rep) override;

    BT::NodeStatus onFailedRequest(RosServiceNode::FailureCause failure) override;
};
