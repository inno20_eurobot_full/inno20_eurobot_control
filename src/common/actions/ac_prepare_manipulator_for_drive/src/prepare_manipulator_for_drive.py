#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Подготовить манипулятор к езде (установить его в такое положение, в котором он должен
находится когда робот едет)
"""

import rospy
from eurobot.core import PathResolver

import actionlib
import ac_prepare_manipulator_for_drive.msg
from sensor_msgs.msg import JointState

# goal message and the result message.
from ct_manipulator.msg import ManipulatorP2PGoal, ManipulatorP2PAction


class PrepareManipulatorForDrive:
    """Wrapper for action."""
    Action = ac_prepare_manipulator_for_drive.msg.PrepareManipulatorForDriveAction
    Feedback = ac_prepare_manipulator_for_drive.msg.PrepareManipulatorForDriveFeedback
    Goal = ac_prepare_manipulator_for_drive.msg.PrepareManipulatorForDriveGoal
    Result = ac_prepare_manipulator_for_drive.msg.PrepareManipulatorForDriveResult


class PrepareManipulatorForDriveNode:
    """Main class."""
    _result = PrepareManipulatorForDrive.Result()

    def __init__(self):
        """Create main class."""
        resolve_path = PathResolver()

        p2p_name = resolve_path(rospy.get_param("p2p_action"))
        motor_names = rospy.get_param("motor_names")
        with_buoy = rospy.get_param("state_with_buoy")
        without_buoy = rospy.get_param("state_without_buoy")

        self.state_with_buoy = JointState(name=motor_names, position=with_buoy)
        self.state_without_buoy = JointState(name=motor_names, position=without_buoy)

        self.call_p2p = actionlib.SimpleActionClient(p2p_name, ManipulatorP2PAction)

        self._action_server = actionlib.SimpleActionServer(
            "command",
            PrepareManipulatorForDrive.Action,
            execute_cb=self.action_callback,
            auto_start=False
        )

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.loginfo("Main for action 'prepare_manipulator_for_drive' is loaded")
        self.call_p2p.wait_for_server()
        self._action_server.start()

    def feedback_processing(self, fb):
        """Processing the feedback from actions"""
        # print("Server feedback:", fb)
        pass

    def action_callback(self, goal: PrepareManipulatorForDrive.Goal):
        """Callback for action."""
        if goal.position == goal.POSITION_WITH_BUOY:
            state = self.state_with_buoy
        elif goal.position == goal.POSITION_WITHOUT_BUOY:
            state = self.state_without_buoy
        else:
            rospy.logwarn("unexpected goal")
            self._result.res = False
            self._action_server.set_succeeded(self._result)
            return

        self.call_p2p.send_goal(
            ManipulatorP2PGoal(state), feedback_cb=self.feedback_processing
        )
        self.call_p2p.wait_for_result()
        self._result.res = self.call_p2p.get_result().done
        self._action_server.set_succeeded(self._result)


def main():
    """Main entrypoint."""
    rospy.init_node("prepare_manipulator_for_drive", anonymous=True)

    main = PrepareManipulatorForDriveNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
