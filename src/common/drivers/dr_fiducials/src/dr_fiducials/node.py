#!/usr/bin/env python3
"""Node for receiving fiducials via udp and send into tf"""

import socket
import struct
import math
import numpy as np
import rospy
import tf2_ros
from geometry_msgs.msg import TransformStamped, Vector3, Quaternion
from tf import transformations
from tf import transformations as t

config = None
broadcasters = {}


class _QuatExpAverage:
    def __init__(self, alpha: float):
        self._alpha = alpha
        self._accum = None

    def update(self, val: np.ndarray):
        if self._alpha is None or self._accum is None:
            self._accum = val
        else:
            self._accum = transformations.quaternion_slerp(self._accum, val, self._alpha)
        return self._accum


class _Broadcaster:
    def __init__(self, fid: int):
        self._fid = fid
        self._name = f'fiducial_{fid}'

        if fid in config['static_fids']:
            self._broadcaster = tf2_ros.StaticTransformBroadcaster()
            self._agv = _QuatExpAverage(config['static_alpha'])
        else:
            self._broadcaster = tf2_ros.TransformBroadcaster()
            self._agv = _QuatExpAverage(None)

        self._frame = TransformStamped()
        if fid in config['static_fids']:
            self._frame.header.frame_id = self._name
            self._frame.child_frame_id = config['frame_id']
        else:
            self._frame.header.frame_id = config['frame_id']
            self._frame.child_frame_id = self._name

    def update(self, pose: np.ndarray, time: rospy.Time = None):
        trans, rot = pose[:3], self._agv.update(pose[3:])

        if self._fid in config['static_fids']:
            transform = t.concatenate_matrices(t.translation_matrix(trans), t.quaternion_matrix(rot))
            inversed_transform = t.inverse_matrix(transform)
            trans = t.translation_from_matrix(inversed_transform)
            rot = t.quaternion_from_matrix(inversed_transform)

        self._frame.header.stamp = time if time else rospy.Time.now()
        self._frame.transform.translation = Vector3(*trans)
        self._frame.transform.rotation = Quaternion(*rot)

        self._broadcaster.sendTransform(self._frame)


def _rotvec_to_quat(r):
    theta = math.sqrt(r[0] ** 2 + r[1] ** 2 + r[2] ** 2)
    cos, sin = math.cos(theta * 0.5), math.sin(theta * 0.5)
    if cos < 0:
        cos, sin = -cos, -sin
    r = [i * sin / theta for i in r]
    return [*r, cos]


def check_whitelist(aruco_no) -> bool:
    """Если есть нетт белого список или арука в белом списке, верни тру."""
    if config["whitelist"] is None:
        return True

    if aruco_no in config["whitelist"]:
        return True

    return False


def check_blacklist(aruco_no) -> bool:
    """Если арука в чёрном списке, верни фолс."""
    if config["blacklist"] is None:
        return True

    if aruco_no in config["blacklist"]:
        return False

    return True


def _process_aruco(aruco, time):
    global config

    [fid, tx, ty, tz, rx, ry, rz] = aruco

    if not check_whitelist(fid):
        return  # игнорируем

    if not check_blacklist(fid):
        return  # игнорируем

    name = f"fiducial_{fid}"

    s = config["scale"][fid]
    tran = [tx * s, ty * s, tz * s]
    quat = _rotvec_to_quat([rx, ry, rz])

    pose = np.array(tran + quat, np.double)

    if name not in broadcasters:
        broadcasters[name] = _Broadcaster(fid)
    broadcasters[name].update(pose, time)


def _process_msg(msg):
    global time_last, time_seq

    if len(msg) < 12:
        return

    [tseq, count, time_offset] = struct.unpack(">QHH", msg[:12])

    if tseq < time_seq:
        rospy.logerr_throttle(
            5, f"Time travel detected: {1e-3 * (tseq - time_seq)} seconds"
        )
        return
    time_seq = tseq

    if len(msg) != count * 26 + 12:
        return

    time = rospy.Time.now() - rospy.Duration(0, time_offset * 1e3)

    if time_last >= time:
        return
    time_last = time

    for aruco in struct.iter_unpack(">Hffffff", msg[12:]):
        _process_aruco(aruco, time)


def _parse_scale(sizes):
    detected = float(sizes["detected"])
    default = float(sizes["default"])

    scale = [default / detected] * 1000

    for i in sizes:
        if i in ["default", "detected"]:
            continue
        if "-" in i:
            a, b = map(int, i.split("-"))
            s = float(sizes[i]) / detected
            scale[a: b + 1] = [s] * (b - a + 1)
        else:
            a = int(i)
            s = float(sizes[i]) / detected
            scale[a] = s

    return scale


if __name__ == "__main__":
    rospy.init_node("fiducials_node")

    config = {
        "listen": rospy.get_param("listen"),
        "frame_id": rospy.get_param("frame_id"),
        "scale": _parse_scale(rospy.get_param("sizes")),
        "static_fids": rospy.get_param("static_fids"),
        "static_alpha": rospy.get_param("static_alpha"),
        "whitelist": rospy.get_param("whitelist", None),
        "blacklist": rospy.get_param("blacklist", None),
    }
    config["listen"] = {
        "addr": config["listen"]["addr"],
        "port": int(config["listen"]["port"]),
    }

    time_last = rospy.Time(0, 0)
    time_seq = 0

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(0.1)
    sock.bind((config["listen"]["addr"], config["listen"]["port"]))

    while not rospy.is_shutdown():
        try:
            msg, _ = sock.recvfrom(1024)
            _process_msg(msg)
        except socket.timeout:
            pass

    sock.close()
