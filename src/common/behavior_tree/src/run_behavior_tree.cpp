#include <ros/ros.h>
#include <ros/package.h>

#include <behaviortree_cpp_v3/bt_factory.h>
#include <behaviortree_cpp_v3/loggers/bt_minitrace_logger.h>
#include <behaviortree_cpp_v3/loggers/bt_zmq_publisher.h>
#include <behaviortree_cpp_v3/loggers/bt_file_logger.h>
#include <behavior_tree/logger/rosout_logger.h>
#include <behavior_tree/wrap_nodes/wrap_node.h>

#include <vector>
#include <string>
#include <utility>
#include <ctime>

#include <eurobot/utils/ros/get_param_list_with_log_error.hpp>
#include <eurobot/utils/current_date_time.hpp>


int main(int argc, char **argv)
{
    ros::init(argc, argv, "behavior_tree");

    ROS_FATAL_COND(argc != 2, "No BT '.xml' path argument in command-line options");
    const std::string& path_to_tree = argv[1];

    ros::NodeHandle nh;
    BT::BehaviorTreeFactory factory;
    std::pair<ros::V_string, ros::V_string> node;    // node_name + topic_path

    const ros::V_string params_act_list_name = {
        "activate_windsock",
        "flag_command",
        "get_closest_buoy",
        "get_home_direction",
        "gripper_command",
        "lighthouse_command",
        "manipulator_lighthouse_activation",
        "move_base_to",
        "prepare_manipulator_for_drive",
        "prepare_manipulator_for_take_buoy",
        "refine_buoy_pos",
        "set_power_state"
    };
    const ros::V_string params_con_list_name = {
        "is_buoy_in_gripper",
        "is_buoy_reachable",
        "is_robot_activated",
        "is_someone_around"
    };
    ros::V_string action_params = get_param_list_with_log_error(nh, params_act_list_name,    "actions/");
    ros::V_string condit_params = get_param_list_with_log_error(nh, params_con_list_name, "conditions/");

    BT::RegisterRosAction <ActivateWindsock> (factory, nh, "activateWindsock", action_params[0]);
    BT::RegisterRosAction <FlagCommand>      (factory, nh, "flagCommand",      action_params[1]);
    BT::RegisterRosService<GetClosestBuoy>   (factory, nh, "getClosestBuoy",   action_params[2]);
    BT::RegisterRosService<GetHomeDirection> (factory, nh, "getHomeDirection", action_params[3]);
    BT::RegisterRosAction <GripperCommand>   (factory, nh, "gripperCommand",   action_params[4]);
    BT::RegisterRosService<LighthouseCommand>(factory, nh, "lighthouseCommand",action_params[5]);
    BT::RegisterRosAction <LighthouseActivation>          (factory, nh, "manipulatorLighthouseActivation",action_params[6]);
    BT::RegisterRosAction <MoveBaseTo>       (factory, nh, "moveBaseTo",       action_params[7]);
    BT::RegisterRosAction <PrepareManipulatorForDrive>    (factory, nh, "prepareManipulatorForDrive",     action_params[8]);
    BT::RegisterRosAction <PrepareManipulatorForTakeBuoy> (factory, nh, "prepareManipulatorForTakeBuoy",  action_params[9]);
    BT::RegisterRosService<RefineBuoyPos>    (factory, nh, "refineBuoyPos",    action_params[10]);
    BT::RegisterRosService<SetPowerState>    (factory, nh, "setPowerState",    action_params[11]);

    BT::RegisterRosService<IsBuoyInGripper>  (factory, nh, "isBuoyInGripper", condit_params[0]);
    BT::RegisterRosService<IsBuoyReachable>  (factory, nh, "isBuoyReachable", condit_params[1]);
    BT::RegisterRosService<IsRobotActivated> (factory, nh, "isRobotActivated",condit_params[2]);
    BT::RegisterRosService<IsSomeoneAround>  (factory, nh, "isSomeoneAround", condit_params[3]);

    auto tree = factory.createTreeFromFile(path_to_tree);
    
    ROS_INFO("#####BEHAVIOR_TREE_START#####");
    /// LOGGERS
    printTreeRecursively(tree.rootNode());
    BT::RosoutLogger logger(tree.rootNode());  
    logger.enableTransitionToIdle(false);

    // This logger saves state changes on file
    BT::FileLogger logger_file(
                                tree,
                                std::string(currentDateTime() + "_BT_trace.fbl").c_str(),
                                20
                            );

    // server name: robot1-eurobot
    BT::PublisherZMQ publisher_zmq ( 
                                tree, // const BT::Tree& tree
                                50,   // unsigned max_msg_per_second
                                1666, // unsigned publisher_port 
                                1667  // unsigned server_port
                            );
    publisher_zmq.enableTransitionToIdle(true);

    BT::NodeStatus status = BT::NodeStatus::RUNNING;
    while(ros::ok() && (status == BT::NodeStatus::IDLE || status == BT::NodeStatus::RUNNING)){
        ros::spinOnce();
        status = tree.tickRoot();
        ros::Duration sleep_time(0.01);
        sleep_time.sleep();
    }
    ROS_INFO("#####BEHAVIOR_TREE_FINISH#####");

    return 0;
}