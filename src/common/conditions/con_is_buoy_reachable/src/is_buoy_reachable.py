#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Сможет ли манипулятор взять стаканчик не двигая базой (дотянется ли?)
"""

import rospy
from eurobot.core import PathResolver

from con_is_buoy_reachable.srv import IsBuoyReachable, IsBuoyReachableRequest, IsBuoyReachableResponse

# goal message and the result message.
from al_manipulator.srv import ManipulatorAdvancedIK
from al_manipulator.msg import ManipulatorEndPos


class IsBuoyReachableNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        resolve_path = PathResolver()

        advik_name = resolve_path(rospy.get_param("advik_service"))
        level_over_buoy_name = resolve_path(rospy.get_param("path_to_high_take_height"))
        buoy_level_name = resolve_path(rospy.get_param("path_to_low_take_height"))
        pitch_angle_name = resolve_path(rospy.get_param("path_to_pitch_angle"))

        self.height_high = float(rospy.get_param(level_over_buoy_name))
        self.height_low = float(rospy.get_param(buoy_level_name))
        self.pitch = float(rospy.get_param(pitch_angle_name))

        self.calc_advik = rospy.ServiceProxy(advik_name, ManipulatorAdvancedIK)
        self.calc_advik.wait_for_service()

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.loginfo("Main for condition 'is_buoy_reachable' is loaded")
        self._server = rospy.Service("command", IsBuoyReachable, self.server_callback)

    def server_callback(self, req: IsBuoyReachableRequest) -> IsBuoyReachableResponse:
        """Callback for server."""
        pos = req.pos.pose.position
        states1 = self.calc_advik(ManipulatorEndPos(x=pos.x, y=pos.y, z=self.height_high, roll=0, pitch=self.pitch)).res
        states2 = self.calc_advik(ManipulatorEndPos(x=pos.x, y=pos.y, z=self.height_low, roll=0, pitch=self.pitch)).res
        if not states1 or not states2:
            return False
        return True


def main():
    """Main entrypoint."""
    rospy.init_node("is_buoy_reachable", anonymous=True)

    main = IsBuoyReachableNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
