#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Доехать до определенной позиции
"""

import time
import rospy
from eurobot.core import PathResolver
from tf.transformations import euler_from_quaternion

import actionlib
import ac_move_base_to.msg


class MoveBaseTo:
    """Wrapper for action."""

    Action = ac_move_base_to.msg.MoveBaseToAction
    Feedback = ac_move_base_to.msg.MoveBaseToFeedback
    Goal = ac_move_base_to.msg.MoveBaseToGoal
    Result = ac_move_base_to.msg.MoveBaseToResult


class MoveBaseToNode:
    """Main class."""

    _result = MoveBaseTo.Result()

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self._action_server = actionlib.SimpleActionServer(
            "command",
            MoveBaseTo.Action,
            execute_cb=self.action_callback,
            auto_start=False,
        )
        self._action_server.start()

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.logwarn("Mock for action 'move_base_to' is loaded")

    def action_callback(self, goal: MoveBaseTo.Goal):
        """Callback for action."""
        cord = goal.pos.pose.position
        quat = goal.pos.pose.orientation
        angles = euler_from_quaternion([quat.x, quat.y, quat.z, quat.w])
        rospy.logwarn(
            f"header: frame '{goal.pos.header.frame_id}' time '{goal.pos.header.stamp}'"
        )
        rospy.logwarn(f"cords:  x '{cord.x}' y '{cord.y}' z '{cord.z}'")
        rospy.logwarn(f"angles: x '{angles[0]}' y '{angles[1]}' z '{angles[2]}'")
        time.sleep(4)
        self._result.res = True
        self._action_server.set_succeeded(self._result)


def main():
    """Main entrypoint."""
    rospy.init_node("move_base_to", anonymous=True)

    main = MoveBaseToNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
