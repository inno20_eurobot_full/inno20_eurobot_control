#include <al_points2buoys/projective_icp.hpp>

#define PI 3.14159265

void ProjectiveICP::start_node(ros::NodeHandle& nh){
    BuoyAlgorithm::start_node(nh);

    //testing 
    // Init = nh.advertise<sensor_msgs::PointCloud2>("INIT", 1);
    // ReducedPoints = nh.advertise<sensor_msgs::PointCloud2>("ReducedPoints", 1);
    // border_pub = nh.advertise<sensor_msgs::PointCloud2>("borders", 1);
}

void ProjectiveICP::toPlane(PCLPoints::Ptr& cloud) const {
    for(PointT& point:*cloud)
        point.z=0;
    cloud = downSampling(cloud,0.001);
}

double ProjectiveICP::getMinHeight(const PCLPoints::Ptr cloud) {
    double minheight = 10; // max
    for(PointT& point:*cloud)
        if(minheight > point.z)
            minheight = point.z;
    return minheight;
}

geometry_msgs::PoseArray::Ptr ProjectiveICP::findBouy(const PCLPoints::Ptr pointCloud){
    PCLPoints::Ptr points = removeFarPoints(pointCloud);
    
    // publish(ReducedPoints, *points);

    vectorPCLpointsPtr points_arr = splitPointCloud(points);

    return calculatePoses(points_arr);
}

vectorPCLpointsPtr ProjectiveICP::splitPointCloud(const PCLPoints::Ptr points) const{
    vectorPCLpointsPtr points_arr;

    PCLPoints::Ptr plane_points(new PCLPoints(*points));
    toPlane(plane_points);

    Image image = Image::toImage(plane_points);
    std::vector<Image> image_arr = Image::splitImage(image);

    // PCLPoints border;
    for(const auto& im:image_arr){
        auto edge = im.getEdges();
        if (edge.xsize() > 0.15 || edge.ysize() > 0.15) continue; // обрезание больших облаков
        PCLPoints::Ptr buoy = cutPointCloud(points, edge.xmin, edge.xmax, edge.ymin, edge.ymax, 0, 0.25); 
        if (buoy->size() < 200) continue; // убрать малые облака
        if (getMinHeight(buoy) > 0.055) continue; // Убрать облака точек, которые расположены на высоте
        
        points_arr.push_back(buoy);
        
        // border += im.getPointEdges();
    }
    
    // publish(border_pub, border);

    return points_arr;
}

inline bool doubleEq(double d1, double d2, double eps = 0.0001){
    return abs(d1-d2) < eps;
}

Eigen::Vector3d ProjectiveICP::getAverage(const PCLPoints& points){
    Eigen::Vector3d p(0,0,0); // center;
    for(const PointT& point: points){
        p.x() += point.x;
        p.y() += point.y;
        p.z() += point.z;
    }
    p.x() /= points.size();
    p.y() /= points.size();
    p.z() /= points.size();
    return p;
}

std::pair<PCLPoints::Ptr, PCLPoints::Ptr> ProjectiveICP::getPair(const PCLPoints& points, const Eigen::Vector3d& cam_shift) const{
    PCLPoints::Ptr newPoints( new PCLPoints() );
    PCLPoints::Ptr pair( new PCLPoints() );

    // Облако точек уже сдвинуто и повернуто
    // Сдвигаем позицию камеры
    Eigen::Vector3d cam_point (cam_pos.x, cam_pos.y, cam_pos.z);
    cam_point -= cam_shift;

    for(const PointT& point: points){
        // cam_point - точка камеры
        // point - точка в облаке точек

        // расчет вектора к точке 
        Eigen::Vector3d cam_ray(point.x, point.y, point.z);
        cam_ray -= cam_point;
        cam_ray.normalize();

        // расстояние до правильной точки по этому лучу
        double distance = find_nearest_buoy_point(cam_point.x(), cam_point.y(), cam_point.z(), cam_ray.x(), cam_ray.y(), cam_ray.z());

        // удаление плохой точки
        if( distance < 0 )
            continue;

        // расчет правильной точки
        cam_ray *= distance;
        cam_ray += cam_point;
        
        newPoints->push_back(point);
        pair->push_back(PointT(cam_ray.x(), cam_ray.y(), cam_ray.z()));
    }
    return std::make_pair(newPoints,pair);
}

double ProjectiveICP::calculateError(const PCLPoints& source, const PCLPoints& target){ // точеи должны быть соответствующие друг другу, то есть после поиска пар
    double error = 0;
    for(int i=0; i<source.size();i++)
        error+= pow(source[i].x - target[i].x,2) 
             +  pow(source[i].y - target[i].y,2)
             +  pow(source[i].z - target[i].z,2);
    return error;
}

std::pair<Eigen::Matrix3d, Eigen::Vector3d> ProjectiveICP::makePCreg(const PCLPoints& X, const PCLPoints& Y){ // точки должны быть соответствующие друг другу, то есть после поиска пар
    Eigen::Vector3d x0 = getAverage(X);   // center исходных точек
    Eigen::Vector3d y0 = getAverage(Y);   // center целевых точек

    // calculating matrix H
    Eigen::Matrix3d H = Eigen::Matrix3d::Zero(); // sum (yn - y0)*(xn - x0)'
    for(int i=0 ;i<X.size();i++){
        Eigen::Vector3d targ ( Y[i].x, Y[i].y, Y[i].z);    
        Eigen::Vector3d point( X[i].x, X[i].y, X[i].z);            
        H += (point - x0)*(targ - y0).transpose();
    }
    
    // calculate Rotaion matrix and translation matrix
    Eigen::JacobiSVD<Eigen::Matrix3d> svd(H, Eigen::ComputeFullU | Eigen::ComputeFullV);
    Eigen::MatrixXd Vt = svd.matrixV().transpose();
    Eigen::MatrixXd U  = svd.matrixU();
    Eigen::Matrix3d R = Vt.transpose() * U.transpose();

    if (R.determinant() < 0 ){
        Vt.block<1,3>(2,0) *= -1;
        R = Vt.transpose()*U.transpose();
    }

    Eigen::Vector3d t = y0 - R * x0;

    return std::make_pair(R,t);
}

void ProjectiveICP::translatePoints(const PCLPoints& in, PCLPoints& out, const Eigen::Vector3d& v){
    out.resize(in.size());
    for(int i=0;i<in.size();i++){
        out[i].x = in[i].x + v.x();
        out[i].y = in[i].y + v.y();
        out[i].z = in[i].z + v.z();
    }
}
void ProjectiveICP::rotatePoints(const PCLPoints& in, PCLPoints& out, const Eigen::Matrix3d& m){
    Eigen::Affine3d transform(Eigen::Affine3d::Identity());
    transform.matrix().block(0,0,3,3) = m;
    pcl::transformPointCloud(in, out, transform);     
}

geometry_msgs::PoseArray::Ptr ProjectiveICP::calculatePoses(const vectorPCLpointsPtr& points_arr) const {
    static const int NUMBER_OF_ITERATION = 40;

    geometry_msgs::PoseArray::Ptr poses(new geometry_msgs::PoseArray);
    
    PCLPoints buoy;
    for(const PCLPoints::Ptr points: points_arr){
        Eigen::Affine3d FLIP180(Eigen::Affine3d::Identity());
            FLIP180.translation() << 0,0, BUOY_H;
            FLIP180(2,2) = -1;

        PCLPoints FlippedCloud; // перевернул облако и работаю только с ним
        pcl::transformPointCloud(*points, FlippedCloud, FLIP180);

        Eigen::Vector3d Translate = getAverage(FlippedCloud); // my translate
            Translate.z() = 0; // потому что стаканчик на земле
            
        PCLPoints myPoints; // Xn
        translatePoints(FlippedCloud, myPoints, -Translate);

        double error = 1e5; // max error
        double usedPoints  = 1; // процент совпадающих точек
        bool Correct = true;
        // START ICP
        for(int iter = 0; iter<NUMBER_OF_ITERATION; iter++){

            // Расчет точек соответствий облаков
            auto pair = getPair(myPoints, Translate);
            PCLPoints& X = *pair.first;
            PCLPoints& Y = *pair.second;

            // publish(InitICP, X);
            // publish(StepICP, Y);

            if(X.size() == 0){
                // ROS_WARN("empty correspondences");
                Correct = false;
                break;
            }

            // make Iteration
            auto [cur_R, cur_T] = makePCreg(X,Y);
            translatePoints(myPoints, myPoints, cur_T);
            rotatePoints   (myPoints, myPoints, cur_R);
            Translate -=cur_T;

            // calculate error
            double newError = calculateError(X,Y);
            // error stabilization
            if(doubleEq(error,newError))
                break;
            error = newError;
            usedPoints = static_cast<double>(X.size())/FlippedCloud.size();

            // if(iter < 6)
            //     ros::Duration(1,500000000).sleep();
        }
        
        // END ICP
        if(!Correct || (usedPoints < 0.80 && error > 0.25 ))continue;

        // Обратно перевернуть стаканчик
        PCLPoints final_points;
        pcl::transformPointCloud(myPoints, final_points, FLIP180.inverse());

        // calculate final transpose
        auto [AllR, AllT] = makePCreg(final_points, *points);

        // transform to pose
        Eigen::Quaterniond quat(AllR);
        geometry_msgs::Pose pose;
        pose.position.x = AllT.x();
        pose.position.y = AllT.y();
        pose.position.z = AllT.z();
        pose.orientation.x = quat.x();
        pose.orientation.y = quat.y();
        pose.orientation.z = quat.z();
        pose.orientation.w = quat.w();

        poses->poses.push_back(pose);

        // rotate buoy
        PCLPoints bouy_trans;
        getBuoyInPose(bouy_trans,pose);

        buoy+=bouy_trans;
    }

    publish(buoy_cloud_pub, buoy);

    return poses;
}