#pragma once

#include <behaviortree_cpp_v3/behavior_tree.h>
#include <geometry_msgs/Point.h>

#include <string>

namespace BT
{
/**
 * @brief Parse XML string to geometry_msgs::msg::Point
 * @param key XML string
 * @return geometry_msgs::msg::Point
 */
template<> inline
geometry_msgs::Point convertFromString(const StringView key)
{
    // three real numbers separated by semicolons
    auto parts = BT::splitString(key, ';');
    if (parts.size() != 3) {
        throw std::runtime_error("invalid number of fields for point attribute)");
    } else {
        geometry_msgs::Point position;
        position.x = BT::convertFromString<double>(parts[0]);
        position.y = BT::convertFromString<double>(parts[1]);
        position.z = BT::convertFromString<double>(parts[2]);
        return position;
    }
}
} // end namespace BT