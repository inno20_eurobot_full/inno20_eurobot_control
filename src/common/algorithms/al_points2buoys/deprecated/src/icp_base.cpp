#include <al_points2buoys/icp_base.hpp>

vectorPCLpointsPtr ICPBase::splitPointCloud(const PCLPoints::Ptr points) const{
    vectorPCLpointsPtr pointArr;
    /* TODO 
        Нужно разделить облако точек на предворительные части, в которых есть стаканчики.
    */
    if(points->size() > 10)
        pointArr.push_back(points);
    return pointArr;
}

geometry_msgs::PoseArray::Ptr ICPBase::calculatePoses(const vectorPCLpointsPtr& pointsArr) const{
    geometry_msgs::PoseArray::Ptr poses(new geometry_msgs::PoseArray);
    for(const auto& points_ :pointsArr){
        PCLPoints::Ptr alignment;

        pcl::IterativeClosestPoint<PointT, PointT> icp;
        // Set the input source and target
        icp.setInputSource (buoy_cloud);
        icp.setInputTarget (points_);

        // Set the max correspondence distance to 5cm (e.g., correspondences with higher
        // distances will be ignored)
        icp.setMaxCorrespondenceDistance (0.02);
        // Set the maximum number of iterations (criterion 1)
        icp.setMaximumIterations (50000);
        // Set the transformation epsilon (criterion 2)
        // icp.setTransformationEpsilon (1e-8);
        // Set the euclidean distance difference epsilon (criterion 3)
        icp.setEuclideanFitnessEpsilon (1);

        // Perform the alignment
        icp.align (*alignment);

        // Obtain the transformation that aligned cloud_source to cloud_source_registered
        Eigen::Matrix4f transformation = icp.getFinalTransformation ();

         // Print the alignment fitness score (values less than 0.00002 are good)
        ROS_INFO("Best fitness score: %f (%d)\n", icp.getFitnessScore(), alignment->size());

        // Print the rotation matrix and translation vector
        Eigen::Matrix3f rotation = transformation.block<3,3>(0, 0);
        Eigen::Vector3f translation = transformation.block<3,1>(0, 3);
        ROS_INFO("    | %6.3f %6.3f %6.3f |", rotation (0,0), rotation (0,1), rotation (0,2));
        ROS_INFO("R = | %6.3f %6.3f %6.3f |", rotation (1,0), rotation (1,1), rotation (1,2));
        ROS_INFO("    | %6.3f %6.3f %6.3f |", rotation (2,0), rotation (2,1), rotation (2,2));
        ROS_INFO("t = < %0.3f, %0.3f, %0.3f >", translation (0), translation (1), translation (2));

        // PCLPoints bouy_trans;
        // pcl::transformPointCloud (*buoy_cloud, bouy_trans, transformation);

        // publish rotated bouy
        sensor_msgs::PointCloud2 OUT_PC2;
        pcl::toROSMsg(*alignment,OUT_PC2);
        ROS_INFO("size (%d)\n",OUT_PC2.width*OUT_PC2.height);

        buoy_cloud_pub.publish(OUT_PC2);

        // pcl::visualization::CloudViewer viewer("alignment");
        // viewer.showCloud(alignment);

        break;
    }
    return poses;
}

geometry_msgs::PoseArray::Ptr ICPBase::findBouy(const PCLPoints::Ptr pointCloud){   
    PCLPoints::Ptr points = removeFarPoints(pointCloud);
    PCLPoints::Ptr sparse_points = downSampling(points);
    vectorPCLpointsPtr pointsArr = splitPointCloud(sparse_points);
    return calculatePoses(pointsArr);
}
