#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Куда ехать домой (на север или на юг)
(по факту, сервис цепляется к топику и ждёт, когда телефон определит и пришлет направление.
Если его вызвали до получения сообщения - тогда HOME_UNKNOWN)
"""

import time
import random

import rospy
from eurobot.core import PathResolver

from ac_get_home_direction.srv import GetHomeDirection, GetHomeDirectionRequest, GetHomeDirectionResponse


class GetHomeDirectionNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self._server = rospy.Service("command", GetHomeDirection, self.server_callback)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.logwarn("Mock for action 'get_home_direction' is loaded")
        self._start_time = time.time_ns()

    def server_callback(self, req: GetHomeDirectionRequest) -> GetHomeDirectionResponse:
        """Callback for server."""
        if time.time_ns() - self._start_time < 25e9:
            return GetHomeDirectionResponse(GetHomeDirectionResponse.HOME_UNKNOWN)

        if random.random() < 0.5:
            return GetHomeDirectionResponse(GetHomeDirectionResponse.HOME_NORTH)
        return GetHomeDirectionResponse(GetHomeDirectionResponse.HOME_SOUTH)


def main():
    """Main entrypoint."""
    rospy.init_node("get_home_direction", anonymous=True)

    main = GetHomeDirectionNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
