#!/usr/bin/env python3
"""RosAction client to test the local planner"""

import rospy
import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import Vector3, Quaternion
import sys


def _quat_from_yaw(yaw):
    from math import sin, cos
    return (0.0, 0.0, sin(yaw * 0.5), cos(yaw * 0.5))


def _movebase_to(x, y, theta):
    client = actionlib.SimpleActionClient("/robot1/control/base/move_base", MoveBaseAction)
    rospy.loginfo("Waiting for server")
    client.wait_for_server()

    # rospy.loginfo("Cancelling all goals")
    # client.cancel_all_goals()

    goal = MoveBaseGoal()
    goal.target_pose.header.frame_id = "map"
    goal.target_pose.header.stamp = rospy.Time.now()
    goal.target_pose.pose.position = Vector3(x, y, 0)
    goal.target_pose.pose.orientation = Quaternion(*_quat_from_yaw(theta))

    client.send_goal(goal)
    rospy.loginfo("Goal sent")

    while not client.wait_for_result(rospy.Duration(0, 5e8)) and not rospy.is_shutdown():
        rospy.loginfo(f"Status {client.get_state()} {client.get_goal_status_text()}")

    if not rospy.is_shutdown():
        rospy.loginfo(f"Result received {client.get_state()} {client.get_goal_status_text()}")


if __name__ == "__main__":
    if len(sys.argv) < 4:
        rospy.loginfo(f"Usage: {sys.argv[0]} X(m) Y(m) Theta(rad)")
    else:
        x, y, z = map(float, sys.argv[1:])

    try:
        rospy.init_node("movebase_client_py")
        _movebase_to(x, y, z)
    except rospy.ROSInterruptException:
        rospy.loginfo("Navigation test finished.")
