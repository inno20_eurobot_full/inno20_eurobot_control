<img src=".gitlab/logo.svg" width="300" >

Проект создан командой студентов из университета Иннополис для участия в соревнованиях [Eurobot2021](https://www.eurobot.org/eurobot/eurobot-2021).

![workspace](.gitlab/img1.png)

# Getting started

В этой секции описаны краткие инструкции по запуку проекта и работы с ним.

### Setup environment

Перед началом работы необходимо [установить докер](https://docs.docker.com/engine/install/ubuntu/), так как разработка проекта осуществляется в докере. Если у вас есть видеокарта NVIDIA, но необходимо установить драйвера для нее ([for Ubuntu](https://www.cyberciti.biz/faq/ubuntu-linux-install-nvidia-driver-latest-proprietary-driver/)), установить [nvidia-container-runtime](https://github.com/NVIDIA/nvidia-container-runtime) и [nvidia-docker2](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html)

### Run docker container

Для запуска докера, можно использовать следующие скрипты:
- **workspace**: run `./workspace.sh nvidia` if you have nvidia. Or run `./workspace.sh intel` if you have intel (see --help)
- **pipeline**: run `./pipeline.sh`. Передайте параметр `full` чтобы продолжать работу после получения ошибки.

![workspace_welcome](.gitlab/workspace_docker_welcome.png)

### Deal with dependencies

Зависимости для каждого пакета должны быть добавлены в один из следующих файлов:
- `[package]/requirements.txt` for python3 requirements
- `[package]/package.xml` for [ros dependencies](http://wiki.ros.org/rosdep/Tutorials/How%20to%20add%20a%20system%20dependency)

После чего в докере необходимо запустить скрипт dep_update

### Drivers

Для работы некоторых hardware-модулей, необходимо на основной компьютер установить драйвера. Подробнее смотри [drivers/README.md](drivers/README.md)

# Program Architecture

Описание основных идей архитекутры проекта. Описывается какие модули есть и как они взаимодействуют между собой

See [src/ARCHITECTURE.md](src/ARCHITECTURE.md)

# Code organization

Описание где какой код находится в репозитори, а так же правил, по которым осуществляется работа с ним

See [src/CODE_ORGANIZATION.md](src/CODE_ORGANIZATION.md)