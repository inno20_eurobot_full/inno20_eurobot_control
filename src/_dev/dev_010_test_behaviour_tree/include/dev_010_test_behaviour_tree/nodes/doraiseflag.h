#pragma once

#include <actionlib/client/simple_action_client.h>
#include <ros/ros.h>

#include <string>

#include "dev_010_test_behaviour_tree/bt_action_node.h"
#include "robot1_actions/RaiseFlagAction.h"

class DoRaiseFlag : public BT::RosActionNode<robot1_actions::RaiseFlagAction>
{

public:
    DoRaiseFlag( ros::NodeHandle& handle, const std::string& name, const BT::NodeConfiguration & conf)
        :RosActionNode<robot1_actions::RaiseFlagAction>(handle, name, conf) {}
    
    static BT::PortsList providedPorts();

    bool sendGoal(GoalType& goal) override;

    BT::NodeStatus onResult( const ResultType& res) override;
    
    BT::NodeStatus onFailedRequest(FailureCause failure) override;
    
    void halt() override;
};
