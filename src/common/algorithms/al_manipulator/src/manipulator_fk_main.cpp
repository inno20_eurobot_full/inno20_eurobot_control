#include <al_manipulator/manipulator_fk.hpp>


int main(int argc, char** argv)
{
	// initialize ros
	ros::init(argc, argv, "manipulator_fk");

	ros::NodeHandle nh;

	// start program
	ManipulatorFK main;
	if (!main.start_node(nh)) return -1;
	main.spin();

	return 0;
}