#include <al_manipulator/dh_params.hpp>

#include <eurobot/core/path_utils.hpp>


bool DHParams::load_from_ROS_params(const ros::NodeHandle& nh, std::string ns, DHParams* res)
{
	std::vector<double> distance_a, angle_alpha, distance_d, angle_theta_shift;
	if (nh.hasParam(join_paths(ns, "distance_a")))
		nh.getParam(join_paths(ns, "distance_a"), distance_a);
	if (nh.hasParam(join_paths(ns, "angle_alpha")))
		nh.getParam(join_paths(ns, "angle_alpha"), angle_alpha);
	if (nh.hasParam(join_paths(ns, "distance_d")))
		nh.getParam(join_paths(ns, "distance_d"), distance_d);
	if (nh.hasParam(join_paths(ns, "angle_theta_shift")))
		nh.getParam(join_paths(ns, "angle_theta_shift"), angle_theta_shift);

	if (distance_a.size() != angle_alpha.size() || angle_alpha.size() != distance_d.size()) return false;

	res->clear();
	res->reserve(distance_a.size());
	for (int i = 0; i < distance_a.size(); i++)
		res->push_back({distance_a[i], angle_alpha[i], distance_d[i], angle_theta_shift[i]});

	return true;
}