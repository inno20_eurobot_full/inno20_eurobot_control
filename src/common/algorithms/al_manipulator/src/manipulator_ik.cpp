#include <al_manipulator/manipulator_ik.hpp>

#include <cmath>

#include <eurobot/math/linalg.hpp>

/*
	Manual test:
	input: [-0.6146060937305478, 0.5011101391073779, 0.6092191543550332, 0.3186805902219001, -0.5506747169066272, -0.8255339978095457, 0.12349483641187217, 0.05254146636462477, 0.5648156386902573, -0.2595809063555589, 0.7833269096274833, 0.22278747820242098, 0.0, 0.0, 0.0, 1.0]
	output: [0.2, 0.7, -0.8, 1.0, -1.14], [0.2, -0.1, 0.8, 0.2, -1.14]

	rosservice call /robot1/algorithms/manipulator_fk/end_effector_state "{state: {name: ['motor0', 'motor1', 'motor2', 'motor3', 'motor4'], position: [0.2, -0.1, 0.8, 0.2, -1.14]}}"
	rosservice call /robot1/algorithms/manipulator_ik/calc_ik "{H: {data: []}}"
*/


bool ManipulatorIK::service_callback(al_manipulator::ManipulatorIK::Request& req, al_manipulator::ManipulatorIK::Response& res)
{
	return process(req.H, res.res);
}

bool ManipulatorIK::load_dh_params(const ros::NodeHandle& nh, DHParams* dh_params)
{
	if (!nh.hasParam("path_to_dh_params"))
	{
		ROS_FATAL("Parameter 'path_to_dh_params' not found.");
		return false;
	}
	std::string dh_params_ns;
	nh.getParam("path_to_dh_params", dh_params_ns);
	dh_params_ns = resolve_path(dh_params_ns);

	if (!DHParams::load_from_ROS_params(nh, dh_params_ns, dh_params))
	{
		ROS_FATAL("Error while reading DH params. Probably incorrect format. Exit");
		return false;
	}
	if (dh_params->size() == 0)
	{
		ROS_FATAL("DH params has zero length. Probably it is not what you want. Exit");
		return false;
	}

	return true;
}

bool ManipulatorIK::build_manipulator(const DHParams& dh)
{
	if (motor_names.size() != dh.size())
	{
		ROS_FATAL("Size of parameter 'motor_names' must be equal to size of DH-params.");
		return false;
	}

	bool check_dh = true;
	check_dh &= dh.size() == 5;
	check_dh &= dh[1].distance_a > 0 && dh[2].distance_a > 0 && dh[3].distance_a == 0;
	check_dh &= abs(dh[0].angle_alpha - M_PI_2) < 1e-9 && dh[1].angle_alpha == 0 && dh[2].angle_alpha == 0;
	check_dh &= abs(dh[3].angle_alpha - M_PI_2) < 1e-9 && dh[4].angle_alpha == 0;
	check_dh &= dh[1].distance_d == 0 && dh[2].distance_d == 0 && dh[3].distance_d == 0;

	if (!check_dh)
	{
		ROS_FATAL("Incorrect pattern of DH-params");
		return false;
	}

	dh_params = dh;

	return true;
}

bool ManipulatorIK::start_service(ros::NodeHandle& nh)
{
	service = nh.advertiseService("calc_ik", &ManipulatorIK::service_callback, this);
	return true;
}

void ManipulatorIK::norm_and_push_back(std::vector<sensor_msgs::JointState>& res, const std::vector<double>& pos)
{
	sensor_msgs::JointState state;
	state.name = motor_names;
	state.position = pos;

	// normalization
	for (double& p : state.position)
	{
		if (p > M_PI) p -= M_PI * 2;
		if (p < -M_PI) p += M_PI * 2;
	}

	res.push_back(state);
}

bool ManipulatorIK::process(const al_manipulator::ManipulatorTF& Hraw, std::vector<sensor_msgs::JointState>& res)
{
	res.clear();

	Matrix H(4, 4);
	for (int y = 0; y < 4; y++)
		for (int x = 0; x < 4; x++)
			H.at(y, x) = Hraw.data[4 * y + x];

	Matrix R = H.minor(3, 3);
	Vector EEx = R.get_column(0).normalize(), EEy = R.get_column(1).normalize(), EEz = R.get_column(2).normalize();
	Point end_effector = H.minor(3, 0).get_column(2).to_point();

	Point motor4_pos = end_effector - EEz * dh_params[4].distance_d - EEx * dh_params[4].distance_a;
	Vector motor1_direction = Vector({motor4_pos.at(0), motor4_pos.at(1), 0}).normalize();

	// Проверяем что решение вообще может существовать (что вектор EEz || motor1_direction)
	if (abs(EEz.at(0) * motor1_direction.at(1) - EEz.at(1) * motor1_direction.at(0)) > 1e-12)
	{
		ROS_WARN("Wrong request: vector EE_z_diraction is not collinear to motor1_diraction. Impossible to calculate IK");
		return false;
	}

	// Вычисляем угол q5
	Vector EEx_target({motor1_direction.at(1), -motor1_direction.at(0), 0});
	double q5 = atan2(-EEy * EEx_target, EEx * EEx_target);

	// Переход в 2d плоскость (остальное рассчитаем в 2d)
	double q1 = atan2(motor1_direction.at(1), motor1_direction.at(0));
	double x = motor4_pos.to_vector().sub_vec(0, 2).length();
	double y = motor4_pos.at(2) - dh_params[0].distance_d;
	double EE_angle = atan2(EEz.at(2), EEz * motor1_direction);

	double l1 = dh_params[1].distance_a, l2 = dh_params[2].distance_a;


	// Case 1, 2
	double x_ = x - dh_params[0].distance_a;
	double q2d_ = atan2(y, x_);
	double c2_ =  x_ * x_ + y * y;

	// Теорема косинусов для угла q3
	double cos_q3_ = (l1 * l1 + l2 * l2 - c2_) / (2 * l1 * l2);

	if (abs(cos_q3_) > 1 + 1e-8) return true;  // no solutions
	if (cos_q3_ > 1) cos_q3_ = 1;
	if (cos_q3_ < -1) cos_q3_ = -1;

	double q3_ = acos(-cos_q3_);
	// Теорема синусов для углов q3 и q2
	double q2_ = asin(sqrt((1 - cos_q3_ * cos_q3_) / c2_) * l2);

	norm_and_push_back(res, {q1, q2d_ - q2_, q3_, EE_angle - q3_ - (q2d_ - q2_), q5});
	norm_and_push_back(res, {q1, q2d_ + q2_, -q3_, EE_angle + q3_ - (q2_ + q2d_), q5});


	// Case 3, 4
	q1 += M_PI;
	EE_angle = -EE_angle - M_PI;
	x_ = x + dh_params[0].distance_a;
	q2d_ = atan2(y, x_);
	c2_ =  x_ * x_ + y * y;

	// Теорема косинусов для угла q3
	cos_q3_ = (l1 * l1 + l2 * l2 - c2_) / (2 * l1 * l2);

	if (abs(cos_q3_) > 1 + 1e-8) return true;  // no solutions
	if (cos_q3_ > 1) cos_q3_ = 1;
	if (cos_q3_ < -1) cos_q3_ = -1;

	q3_ = acos(-cos_q3_);
	// Теорема синусов для углов q3 и q2
	q2_ = asin(sqrt((1 - cos_q3_ * cos_q3_) / c2_) * l2);

	norm_and_push_back(res, {q1, M_PI - q2d_ + q2_, -q3_, EE_angle + q3_ - (M_PI - q2d_ + q2_), q5 + M_PI});
	norm_and_push_back(res, {q1, M_PI - q2d_ - q2_, q3_, EE_angle - q3_ - (M_PI - q2d_ - q2_), q5 + M_PI});
	
	return true;
}

bool ManipulatorIK::start_node(ros::NodeHandle& nh)
{
	// get home_path param
	resolve_path.init(nh);

	if (!nh.hasParam("motor_names"))
	{
		ROS_FATAL("Parameter 'motor_names' not found.");
		return false;
	}
	nh.getParam("motor_names", motor_names);


	DHParams dh_params;
	if (!load_dh_params(nh, &dh_params)) return false;
	if (!build_manipulator(dh_params)) return false;

	start_service(nh);

	return true;
}

void ManipulatorIK::spin()
{
	ros::spin();
}