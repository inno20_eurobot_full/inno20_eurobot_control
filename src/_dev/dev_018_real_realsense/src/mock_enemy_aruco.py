#!/usr/bin/env python3
import rospy

from geometry_msgs.msg import TransformStamped
import tf2_ros
import tf


def main():
    """Main entrypoint."""
    broadcaster = tf2_ros.StaticTransformBroadcaster()
    enemy_frame = TransformStamped()

    enemy_frame.header.stamp = rospy.Time.now()
    enemy_frame.header.frame_id = "map"
    enemy_frame.child_frame_id = "fiducials_6"

    enemy_frame.transform.translation.x = 1.1
    enemy_frame.transform.translation.y = 1.1
    enemy_frame.transform.translation.z = 0.43

    quat = tf.transformations.quaternion_from_euler(0, 0, 0)
    enemy_frame.transform.rotation.x = quat[0]
    enemy_frame.transform.rotation.y = quat[1]
    enemy_frame.transform.rotation.z = quat[2]
    enemy_frame.transform.rotation.w = quat[3]
    broadcaster.sendTransform(enemy_frame)
    rospy.spin()


if __name__ == "__main__":
    rospy.init_node("mock_enemy_transform")
    main()
