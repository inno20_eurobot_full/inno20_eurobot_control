#!/usr/bin/env python3

import rospy
from al_manipulator.msg import ManipulatorTF
import numpy as np


def callback(msg: ManipulatorTF):
    """Callback for end_effector_state"""
    a = np.array(msg.data).reshape(4, 4).round(3)
    print(a, end="\n\n")


def main():
    """Main entrypoint."""
    rospy.init_node("show_fk", anonymous=True)
    rospy.Subscriber(
        "/robot1/algorithms/manipulator_fk/end_effector_state", ManipulatorTF, callback
    )
    rospy.spin()


if __name__ == "__main__":
    main()
