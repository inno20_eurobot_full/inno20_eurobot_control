#pragma once

#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <al_manipulator/ManipulatorTF.h>
#include <al_manipulator/ManipulatorFK.h>

#include <vector>
#include <string>

#include <eurobot/math/linalg.hpp>
#include <eurobot/core/path_utils.hpp>
#include <al_manipulator/dh_params.hpp>


class ManipulatorFKStep
{
public:
    Matrix transform;
    double angle_theta_shift;

public:
    explicit ManipulatorFKStep(DHParam p);

    Matrix get(double angle_theta);
};


class ManipulatorFK
{
private:
    PathResolver resolve_path;

    std::vector<std::string> motor_names;
    std::vector<ManipulatorFKStep> steps;

    ros::Subscriber input_topic;
    ros::Publisher output_topic;
    ros::ServiceServer service;

    void topic_callback(const sensor_msgs::JointState::ConstPtr& msg);
    bool service_callback(al_manipulator::ManipulatorFK::Request& req, al_manipulator::ManipulatorFK::Response& res);

    bool load_dh_params(const ros::NodeHandle& nh, DHParams* dh_params);
    bool build_manipulator(const DHParams& dh);
    bool start_topics(ros::NodeHandle& nh);
    bool start_service(ros::NodeHandle& nh);

    bool process(const sensor_msgs::JointState& joint_state, al_manipulator::ManipulatorTF& res);

public:
    bool start_node(ros::NodeHandle& nh);

    void spin();
};