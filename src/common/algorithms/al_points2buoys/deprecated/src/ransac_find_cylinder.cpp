#include <al_points2buoys/ransac_find_cylinder.hpp>

geometry_msgs::Pose RANSACFindCylinder::doRANSAC(const PCLPoints::Ptr cloud_filtered) const{
    // Datasets
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
    pcl::PointIndices::Ptr inliers_cylinder (new pcl::PointIndices);
    pcl::ModelCoefficients::Ptr coefficients_cylinder (new pcl::ModelCoefficients);

    // Estimate point normals
    pcl::NormalEstimation<PointT, pcl::Normal> ne;
    SearchMethod::Ptr tree (new pcl::search::KdTree<PointT> ());
    ne.setSearchMethod (tree);
    ne.setInputCloud (cloud_filtered);
    ne.setRadiusSearch (0.02f);
    ne.compute (*cloud_normals);

    // Create the segmentation object for cylinder segmentation and set all the parameters
    pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg; 
    seg.setOptimizeCoefficients (true);
    seg.setModelType (pcl::SACMODEL_CYLINDER);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setNormalDistanceWeight (0.7);
    seg.setMaxIterations (1000);
    seg.setDistanceThreshold (0.03);
    seg.setRadiusLimits (0.02, 0.06);
    seg.setInputCloud (cloud_filtered);
    seg.setInputNormals (cloud_normals);

    // Obtain the cylinder inliers and coefficients
    seg.segment (*inliers_cylinder, *coefficients_cylinder);
    
    geometry_msgs::PoseArray::Ptr poseArr(new geometry_msgs::PoseArray);

    bool find = (inliers_cylinder->indices.size() > 5);


    geometry_msgs::Pose pose;
    if(find){
        // from angle to quaternion
        Eigen::Matrix3f m;
            m =   Eigen::AngleAxisf(coefficients_cylinder->values[3], Eigen::Vector3f::UnitX())
                * Eigen::AngleAxisf(coefficients_cylinder->values[4], Eigen::Vector3f::UnitY())
                * Eigen::AngleAxisf(coefficients_cylinder->values[5], Eigen::Vector3f::UnitZ());
        Eigen::Quaternionf quat(m);

        pose.position.x = coefficients_cylinder->values[0];
        pose.position.y = coefficients_cylinder->values[1];
        pose.position.z = 0;//coefficients_cylinder->values[2]; //height is often incorrect
        pose.orientation.x = quat.x();
        pose.orientation.y = quat.y();
        pose.orientation.z = quat.z();
        pose.orientation.w = quat.w();

        // make rotation matrix to display buoy
        Eigen::Affine3f transform(Eigen::Affine3f::Identity());
        transform.translation() << pose.position.x, pose.position.y, pose.position.z;
        transform.matrix().block(0,0,3,3) = quat.toRotationMatrix();

        // rotate buoy
        PCLPoints bouy_trans;
        pcl::transformPointCloud(*buoy_cloud, bouy_trans, transform);
        
        // publish rotated bouy
        sensor_msgs::PointCloud2 OUT_PC2;
        pcl::toROSMsg(bouy_trans,OUT_PC2);
        buoy_cloud_pub.publish(OUT_PC2);
    } else {
        ROS_INFO("Number of buoys: 0");
    }
    return pose;
}

geometry_msgs::PoseArray::Ptr RANSACFindCylinder::findBouy(const PCLPoints::Ptr pointCloud){   
    PCLPoints::Ptr points = removeFarPoints(pointCloud);
    // ROS_INFO("points size: %d", points->size());
    PCLPoints::Ptr sparse_points = downSampling(points);

    geometry_msgs::PoseArray::Ptr poseArr(new geometry_msgs::PoseArray);
    poseArr->poses.push_back(doRANSAC(sparse_points));
    return poseArr;
}