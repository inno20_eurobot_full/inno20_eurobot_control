"""Utility library"""

from math import sin, cos


def quaternion_from_euler(roll, pitch, yaw):
    """Create quaternion from euler angles"""
    cy = cos(yaw * 0.5)
    sy = sin(yaw * 0.5)
    cr = cos(roll * 0.5)
    sr = sin(roll * 0.5)
    cp = cos(pitch * 0.5)
    sp = sin(pitch * 0.5)
    w = cy * cr * cp + sy * sr * sp
    x = cy * sr * cp - sy * cr * sp
    y = cy * cr * sp + sy * sr * cp
    z = sy * cr * cp - cy * sr * sp
    return (x, y, z, w)


def quaternion_from_yaw(yaw):
    """Create quaternion from euler yaw"""
    return (0.0, 0.0, sin(yaw * 0.5), cos(yaw * 0.5))
