#!/usr/bin/env python3
import rospy
import sys


import actionlib

from ac_prepare_manipulator_for_drive.msg import PrepareManipulatorForDriveAction
from ac_prepare_manipulator_for_drive.msg import PrepareManipulatorForDriveGoal


def main():
    """Main entrypoint."""
    client = actionlib.SimpleActionClient(
        "/robot1/actions/prepare_manipulator_for_drive/command",
        PrepareManipulatorForDriveAction,
    )

    print("wait_for_server")
    client.wait_for_server()

    print("send_goal")
    goal = PrepareManipulatorForDriveGoal()
    goal.position = int(sys.argv[1]) if len(sys.argv) > 1 else 0
    client.send_goal(goal)

    print("wait_for_result")
    client.wait_for_result()

    print(f"Result: {client.get_result()}")


if __name__ == "__main__":
    rospy.init_node("prepare_manipulator_for_drive")
    main()
