#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Вытащена ли веревка из робота?
"""

import rospy
from eurobot.core import PathResolver

from con_is_robot_activated.srv import IsRobotActivated, IsRobotActivatedRequest, IsRobotActivatedResponse

from dr_jetson_gpio.srv import IsButtonActivated


class IsRobotActivatedNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        resolve_path = PathResolver()

        service_name = resolve_path(rospy.get_param("driver_service"))

        self.get_state = rospy.ServiceProxy(service_name, IsButtonActivated)
        self.get_state.wait_for_service()

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.loginfo("main for condition 'is_robot_activated' is loaded")
        self._server = rospy.Service("command", IsRobotActivated, self.server_callback)

    def server_callback(self, req: IsRobotActivatedRequest) -> IsRobotActivatedResponse:
        """Callback for server."""
        return IsRobotActivatedResponse(self.get_state().res)


def main():
    """Main entrypoint."""
    rospy.init_node("is_robot_activated", anonymous=True)

    main = IsRobotActivatedNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
