#include <al_manipulator/manipulator_h.hpp>

#include <cmath>

/*
	Manual test:
	input: [0.28, 0, 0.05, 1.570796, 0]
	output: [ 0 0 1 0.28
              0 1 0 0
             -1 0 0 -0.07
              0 0 0 1    ]
			  
	rosservice call /robot1/algorithms/manipulator_h/calc_h "{pos: {x,y,z,roll,pitch}}"
*/

bool ManipulatorH::service_callback(al_manipulator::ManipulatorH::Request& req, al_manipulator::ManipulatorH::Response& res)
{
	return process(req.pos, res.res);
}

void ManipulatorH::load_dh_params(const ros::NodeHandle& nh, DHParams* dh_params)
{
    ROS_ASSERT_CMD(nh.hasParam("path_to_dh_params"), throw "Parameter 'path_to_dh_params' not found.");

	std::string dh_params_ns;
	nh.getParam("path_to_dh_params", dh_params_ns);
	dh_params_ns = resolve_path(dh_params_ns);

    bool loadParam = DHParams::load_from_ROS_params(nh, dh_params_ns, dh_params);
    ROS_ASSERT_CMD(loadParam, throw "Error while reading DH params. Probably incorrect format. Exit");

    ROS_ASSERT_CMD(dh_params->size() != 0, throw "DH params has zero length. Probably it is not what you want. Exit");
}

void ManipulatorH::build_manipulator(const DHParams& dh)
{
    ROS_ASSERT_CMD(motor_names.size() == dh.size(), throw "Size of parameter 'motor_names' must be equal to size of DH-params.");

	bool check_dh = true;
	check_dh &= dh.size() == 5;
	check_dh &= dh[1].distance_a > 0 && dh[2].distance_a > 0 && dh[3].distance_a == 0;
	check_dh &= abs(dh[0].angle_alpha - M_PI_2) < 1e-9 && dh[1].angle_alpha == 0 && dh[2].angle_alpha == 0;
	check_dh &= abs(dh[3].angle_alpha - M_PI_2) < 1e-9 && dh[4].angle_alpha == 0;
	check_dh &= dh[1].distance_d == 0 && dh[2].distance_d == 0 && dh[3].distance_d == 0;

    ROS_ASSERT_CMD(check_dh, throw "Incorrect pattern of DH-params");
	dh_params = dh;
}

Matrix ManipulatorH::getMatrixH(const al_manipulator::ManipulatorEndPos& posMSG, double base_height, const DHParams& dh_params){
	auto pos = posMSG;

	// сдвиг по вертикальной оси, потому что у манипулятора ноль на уровне базы
	pos.z -= base_height;

	// расчет вращения манипулятора с учетом сдвига точки эндэффектора 
	double r = sqrt(pos.x*pos.x + pos.y*pos.y);
	double dy = dh_params[4].distance_a * cos(pos.roll);
	double yaw_sh = asin(dy/r);
	double yaw = atan2(pos.y,pos.x) + yaw_sh;

	// расчет матриц трансформации
	Matrix translate    = AffineMatrix4x4::TranslateXYZ(pos.x, pos.y, pos.z),
		   rotate_yaw   = AffineMatrix4x4::RotateZ(yaw),
		   rotate_pitch = AffineMatrix4x4::RotateY(pos.pitch),
		   rotate_roll  = AffineMatrix4x4::RotateX(pos.roll);
	Matrix change_axis(4,4);
	change_axis.get_data() = { 0, 0, 1, 0,
							  -1, 0, 0, 0,
							   0,-1, 0, 0,
							   0, 0, 0, 1};

	// расчет матрицы трансформации
	return translate * (rotate_yaw * rotate_pitch * rotate_roll) * change_axis;
}

bool ManipulatorH::process(const al_manipulator::ManipulatorEndPos& pos, al_manipulator::ManipulatorTF& Hres)
{
	auto H = getMatrixH(pos, base_height, dh_params);

	for(int i=0; i<16; i++)
		Hres.data[i] = H.get_data()[i];

    return true;
}

void ManipulatorH::start_node(ros::NodeHandle& nh)
{
	// get home_path param
	resolve_path.init(nh);

    ROS_ASSERT_CMD(nh.hasParam("motor_names"), throw "Parameter 'motor_names' not found.");
	nh.getParam("motor_names", motor_names);

	std::string base_height_name;
	ROS_ASSERT_CMD(nh.hasParam("path_to_base_height"), throw "Parameter 'path_to_base_height' not found.");
	nh.getParam("path_to_base_height", base_height_name);

	base_height_name = resolve_path(base_height_name);
    ROS_ASSERT_CMD(nh.hasParam(base_height_name), throw "Parameter 'base_height_name' not found.");
	nh.getParam(base_height_name, base_height);

	DHParams dh_params;
	load_dh_params(nh, &dh_params);
	build_manipulator(dh_params);

	service = nh.advertiseService("calc_h", &ManipulatorH::service_callback, this);
}

void ManipulatorH::spin()
{
	ros::spin();
}