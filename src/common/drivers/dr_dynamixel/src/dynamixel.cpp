#include <dr_dynamixel/dynamixel.hpp>
#include <dr_dynamixel/utils.hpp>
#include <dr_dynamixel/dxl_api.hpp>

DynamixelDriver::DynamixelDriver(ros::NodeHandle& node, const std::vector<std::string>& motor_names) : running(false)
{
    const auto device = getParam<std::string>(node, "device_name");
    const auto baudrate = getParam<int>(node, "baud_rate");

    for (auto i : motor_names) {
        ros::NodeHandle _node(node, i);
        
        motors[i].id = getParam<int>(_node, "dynamixel_id");
        motors[i].mode = getParam<DynamixelMode>(_node, "mode");
        motors[i].pid = getParamArray<double, 3>(_node, "position_pid");
        motors[i].constrains = getParamArray<double, 2>(_node, "constrains");
        motors[i].integralLimits = getParamArray<double, 2>(_node, "integralLimits", {0., 0.});
        motors[i].config = getParam<std::map<uint16_t, std::vector<uint8_t>>>(_node, "config");
    }

    portHandler = dynamixel::PortHandler::getPortHandler(device.c_str());
    packetHandler = dynamixel::PacketHandler::getPacketHandler(2.0);

    if (!portHandler->openPort()) {
        ROS_FATAL("Failed to open port %s",device.c_str());
        throw std::runtime_error("Failed to open port");
    }

    if (!portHandler->setBaudRate(baudrate)) {
        throw std::runtime_error("Failed to set baudrate");
    }

    pub = node.advertise<sensor_msgs::JointState>("joint_states", 1);
    sub = node.subscribe("command", 1, &DynamixelDriver::goalCallback, this);
    srv = node.advertiseService("set_power", &DynamixelDriver::setPower, this);
}

DynamixelDriver::~DynamixelDriver()
{
    pub.shutdown();
    sub.shutdown();
    portHandler->closePort();
    delete packetHandler;
    delete portHandler;
}

void DynamixelDriver::goalCallback(const sensor_msgs::JointStateConstPtr &goal)
{
    for (size_t i = 0; i < goal->name.size(); i++) {
        const auto name = goal->name[i];
        if (motors.find(name) != motors.end()) {
            motors[name].goalPos = goal->position[i];
            motors[name].goalVel = goal->velocity[i];
            motors[name].goalPwm = goal->effort[i];
        }
    }
}

void DynamixelDriver::syncReadPosition(dynamixel::GroupSyncRead &posRead)
{
    for (auto &i : motors) {
        i.second.prevPos = i.second.pos;
        i.second.pos.reset();
    }

    int dxl_comm_result = posRead.txRxPacket();
    if (dxl_comm_result != COMM_SUCCESS) {
        ROS_ERROR("Error while bulk reading position: %s", packetHandler->getTxRxResult(dxl_comm_result));
    }
    for (auto &i : motors) {
        uint8_t dxl_error;
        if (posRead.getError(i.second.id, &dxl_error)) {
            ROS_ERROR("[%s] Error while bulk reading position: %s", i.first.c_str(), packetHandler->getRxPacketError(dxl_error));
        }
    }
    for (auto &i : motors) {
        if (!posRead.isAvailable(i.second.id, MX28_PRESENT_POSITION, 4)) {
            ROS_ERROR("[%s] Position data is not available", i.first.c_str());
        } else {
            i.second.pos = ((int32_t)posRead.getData(i.second.id, MX28_PRESENT_POSITION, 4)) * MX28_PTS_TO_POS;
        }
    }
}

void DynamixelDriver::syncReadVelocity(dynamixel::GroupSyncRead &velRead)
{
    for (auto &i : motors) {
        i.second.vel.reset();
    }

    int dxl_comm_result = velRead.txRxPacket();
    if (dxl_comm_result != COMM_SUCCESS) {
        ROS_ERROR("Error while bulk reading velocity: %s", packetHandler->getTxRxResult(dxl_comm_result));
    }
    for (auto &i : motors) {
        uint8_t dxl_error;
        if (velRead.getError(i.second.id, &dxl_error)) {
            ROS_ERROR("[%s] Error while bulk reading velocity: %s", i.first.c_str(), packetHandler->getRxPacketError(dxl_error));
        }
    }
    for (auto &i : motors) {
        if (!velRead.isAvailable(i.second.id, MX28_PRESENT_VELOCITY, 4)) {
            ROS_ERROR("[%s] Velocity data is not available", i.first.c_str());
        } else {
            i.second.vel = ((int32_t)velRead.getData(i.second.id, MX28_PRESENT_VELOCITY, 4)) * MX28_PTS_TO_VEL;
        }
    }
}

void DynamixelDriver::approximateVelocity(double dt)
{
    for (auto &i : motors) {
        if (i.second.pos.has_value() && i.second.prevPos.has_value()) {
            i.second.vel = (*i.second.pos - *i.second.prevPos) / dt;
        } else {
            i.second.vel.reset();
        }
    }
}

void DynamixelDriver::ensureInitialGoal()
{
    for (auto &i : motors) {
        if (!i.second.hasInput && i.second.pos.has_value()) {
            i.second.goalPos = *i.second.pos;
            i.second.goalVel = 0;
            i.second.goalPwm = 0;
            i.second.hasInput = true;
        }
    }
}

void DynamixelDriver::calculateControl(double dt)
{
    for (auto &i : motors) {
        auto &motor = i.second;
        motor.rawGoal.reset();
            
        double goalPos = motor.goalPos, goalVel = motor.goalVel, goalPwm = motor.goalPwm;
        if (motor.pos < motor.constrains[0]) {
            if (goalVel < 0 || goalPos < motor.constrains[0]) {
                goalPos = motor.constrains[0];
                goalVel = 0;
            }
            if (goalPwm < 0) {
                goalPwm = 0;
            }
        }
        if (motor.pos > motor.constrains[1]) {
            if (goalVel > 0 || goalPos > motor.constrains[1]) {
                goalPos = motor.constrains[1];
                goalVel = 0;
            }
            if (goalPwm > 0) {
                goalPwm = 0;
            }
        }

        if (motor.mode == MODE_VELPOS) {
            if (motor.pos.has_value() && motor.vel.has_value()) {
                const auto &pid = motor.pid;
                const auto posError = goalPos - *motor.pos;
                const auto velError = goalVel - *motor.vel;
                motor.integral += posError * dt;
                if (motor.integralLimits[0] + 1e-9 < motor.integralLimits[1]) {
                    motor.integral = std::clamp(motor.integral, motor.integralLimits[0], motor.integralLimits[1]);
                }

                motor.rawGoal = pid[0] * posError + pid[1] * motor.integral + pid[2] * velError + goalVel;
                motor.rawGoal = std::clamp(motor.rawGoal.value(), -MX28_VEL_CLAMP * MX28_PTS_TO_VEL, MX28_VEL_CLAMP * MX28_PTS_TO_VEL);
            }
        }
        if (motor.mode == MODE_EFFORT) {
            motor.rawGoal = goalPwm;
            motor.rawGoal = std::clamp(motor.rawGoal.value(), -MX28_VEL_CLAMP * MX28_PTS_TO_PWM, MX28_VEL_CLAMP * MX28_PTS_TO_PWM);
        }
    }
}

void DynamixelDriver::syncWriteVelocity(dynamixel::GroupSyncWrite &velWrite)
{
    std::array<uint8_t, 4> dataArr[255];
    int count = 0;

    for (auto &i : motors) {
        if (!i.second.rawGoal.has_value() || i.second.mode != MODE_VELPOS)
            continue;
        
        const double mul = 1.0 / MX28_PTS_TO_VEL;
        const int32_t data = std::clamp<int32_t>(mul * *i.second.rawGoal, -MX28_VEL_CLAMP, MX28_VEL_CLAMP);
        const uint32_t udata = (uint32_t)data;
        dataArr[i.second.id] = {
            DXL_LOBYTE(DXL_LOWORD(udata)),
            DXL_HIBYTE(DXL_LOWORD(udata)),
            DXL_LOBYTE(DXL_HIWORD(udata)),
            DXL_HIBYTE(DXL_HIWORD(udata)),
        };

        if (!velWrite.addParam(i.second.id, dataArr[i.second.id].data())) {
            ROS_ERROR("[%s] Error adding velocity parameter for sync write", i.first.c_str());
        }

        count++;
    }

    if (count) {
        int dxl_comm_result = velWrite.txPacket();
        if (dxl_comm_result != COMM_SUCCESS) {
            ROS_ERROR("Error while sync writing velocity: %s", packetHandler->getTxRxResult(dxl_comm_result));
        }

        velWrite.clearParam();
    }
}

void DynamixelDriver::syncWriteEffort(dynamixel::GroupSyncWrite &pwmWrite)
{
    std::array<uint8_t, 2> dataArr[255];
    int count = 0;

    for (auto &i : motors) {
        if (!i.second.rawGoal.has_value() || i.second.mode != MODE_EFFORT)
            continue;
        
        const double mul = 1.0 / MX28_PTS_TO_PWM;
        const int16_t data = std::clamp<int16_t>(mul * *i.second.rawGoal, -MX28_PWM_CLAMP, MX28_PWM_CLAMP);
        const uint16_t udata = (uint16_t)data;
        dataArr[i.second.id] = {
            DXL_LOBYTE(udata),
            DXL_HIBYTE(udata),
        };

        if (!pwmWrite.addParam(i.second.id, dataArr[i.second.id].data())) {
            ROS_ERROR("[%s] Error adding effort parameter for sync write", i.first.c_str());
        }

        count++;
    }

    if (count) {
        int dxl_comm_result = pwmWrite.txPacket();
        if (dxl_comm_result != COMM_SUCCESS) {
            ROS_ERROR("Error while sync writing effort: %s", packetHandler->getTxRxResult(dxl_comm_result));
        }

        pwmWrite.clearParam();
    }
}

void DynamixelDriver::publishJointState()
{
    sensor_msgs::JointState state;
    for (auto &i : motors) {
        if (i.second.pos.has_value() && i.second.vel.has_value()) {
            state.name.emplace_back(i.first);
            state.position.emplace_back(*i.second.pos);
            state.velocity.emplace_back(*i.second.vel);
            state.effort.emplace_back(0);
        }
    }
    pub.publish(state);
}

void DynamixelDriver::worker()
{
    dynamixel::GroupSyncRead velRead(portHandler, packetHandler, MX28_PRESENT_VELOCITY, 4);
    dynamixel::GroupSyncRead posRead(portHandler, packetHandler, MX28_PRESENT_POSITION, 4);
    dynamixel::GroupSyncWrite velWrite(portHandler, packetHandler, MX28_GOAL_VELOCITY, 4);
    dynamixel::GroupSyncWrite pwmWrite(portHandler, packetHandler, MX28_GOAL_PWM, 2);
    {
        for (auto &i : motors) {
            if (!posRead.addParam(i.second.id)) {
                ROS_ERROR("[%s] Error adding position parameter for bulk read", i.first.c_str());
            }
            if (!velRead.addParam(i.second.id)) {
                ROS_ERROR("[%s] Error adding position parameter for bulk read", i.first.c_str());
            }
        }
    }

    auto lastTime = std::chrono::steady_clock::now();
    while (running) {
        std::lock_guard<std::mutex> guard(motors_mutex);

        const auto nowTime = std::chrono::steady_clock::now();
        const auto dt = std::chrono::duration<double>(nowTime - lastTime).count();
        lastTime = nowTime;

        syncReadPosition(std::ref(posRead));
        approximateVelocity(dt);

        if (enabled.load()) {
            ensureInitialGoal();
            calculateControl(dt);
            
            syncWriteVelocity(std::ref(velWrite));
            syncWriteEffort(std::ref(pwmWrite));
        }
        
        publishJointState();
    }
}

bool DynamixelDriver::configureMotors() {
    std::string err = "";
    bool success = true;

    for (auto &i : motors) {
        for (auto &j : i.second.config) {
            DXL_TXRX_ERR(err, packetHandler, writeTxRx, portHandler, i.second.id, j.first, j.second.size(), j.second.data());
            if (!err.empty()) {
                ROS_ERROR("[%s] Error configuring motor (address %d): %s", i.first.c_str(), j.first, err.c_str());
                success = false;
            }
        }
    }

    return success;
}

bool DynamixelDriver::verifyModes() {
    std::string err = "";
    bool success = true;

    for (auto &i : motors) {
        uint8_t mode;
        DXL_TXRX_ERR(err, packetHandler, read1ByteTxRx, portHandler, i.second.id, MX28_OPERATING_MODE, &mode);
        if (!err.empty()) {
            ROS_ERROR("[%s] Error reading operating mode of motor: %s", i.first.c_str(), err.c_str());
            success = false;
        } else {
            switch (mode) {
            case MX28_MODE_VEL_CONTROL: if (i.second.mode == MODE_VELPOS) { continue; }
            case MX28_MODE_PWM_CONTROL: if (i.second.mode == MODE_EFFORT) { continue; }
            default:
                ROS_ERROR("[%s] Wrong operating mode (%u) of motor", i.first.c_str(), mode);
                success = false;
            }
        }
    }

    return success;
}

bool DynamixelDriver::enableMotors(bool enable) {
    std::string err = "";
    bool success = true;

    for (auto &i : motors) {
        DXL_TXRX_ERR(err, packetHandler, write1ByteTxRx, portHandler, i.second.id, MX28_TORQUE_ENABLE, enable ? 1 : 0);
        if (!err.empty()) {
            ROS_ERROR("[%s] Error enabling motor: %s", i.first.c_str(), err.c_str());
            success = false;
        }
    }

    return success;
}

bool DynamixelDriver::setPower(dr_dynamixel::SetPower::Request &req, dr_dynamixel::SetPower::Response &res) {
    std::lock_guard<std::mutex> guard(motors_mutex);

    if (req.power) {
        for (auto &i : motors) {
            i.second.integral = 0.0;
            i.second.hasInput = false;
        }
        res.success = enableMotors(true);
        enabled = true && res.success;
    } else {
        enabled = false;
        res.success = enableMotors(false);
    }
    return true;
}

bool DynamixelDriver::start()
{
    bool success = true;

    if (success) {
        success = configureMotors();
    }

    if (success) {
        success = verifyModes();
    }

    if (success) {
        success = enableMotors(false);
    }

    if (success) {
        ROS_INFO("All dynamixels are succesfully configured and enabled");
        running = true;
        worker_thread = std::thread([this] {this->worker();});
    } else {
        stop();
    }

    return success;
}

bool DynamixelDriver::stop()
{
    if (running) {
        running = false;
        worker_thread.join();
    }

    bool success = true;

    if (success) {
        success = enableMotors(false);
    }

    return success;
}
