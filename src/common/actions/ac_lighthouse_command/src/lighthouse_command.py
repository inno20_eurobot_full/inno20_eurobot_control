#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Изменить состояние маяка (поднять его или опустить)
"""

import rospy
from eurobot.core import PathResolver

from ac_lighthouse_command.srv import LighthouseCommand, LighthouseCommandRequest, LighthouseCommandResponse

from dr_lighthouse.srv import Command, CommandRequest


class LighthouseCommandNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        resolve_path = PathResolver()
        service = resolve_path(rospy.get_param("lighthouse_service"))
        self.Raise_command = rospy.get_param("raise")
        self.Lower_command = rospy.get_param("lower")

        self.lighthouse_command = rospy.ServiceProxy(service, Command)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.loginfo("Main for action 'lighthouse_command' is loaded")
        self.lighthouse_command.wait_for_service()
        self._server = rospy.Service("command", LighthouseCommand, self.server_callback)

    def server_callback(self, req: LighthouseCommandRequest) -> LighthouseCommandResponse:
        """Callback for server."""
        if req.cmd == req.RAISE:
            res = self.lighthouse_command(CommandRequest(self.Raise_command, req.use_motor)).sended
            return LighthouseCommandResponse(res)
        elif req.cmd == req.LOWER:
            res = self.lighthouse_command(CommandRequest(self.Lower_command, req.use_motor)).sended
            return LighthouseCommandResponse(res)

        rospy.logwarn(f"unexpected cmd: {req.cmd}, list:{req.RAISE, req.LOWER}")
        return LighthouseCommandResponse(False)


def main():
    """Main entrypoint."""
    rospy.init_node("lighthouse_command", anonymous=True)

    main = LighthouseCommandNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
