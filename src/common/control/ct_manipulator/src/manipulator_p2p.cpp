// related
#include <ct_manipulator/manipulator_p2p.h>
// С
#include <sensor_msgs/JointState.h>
// С++
#include <chrono>
#include <sstream>
#include <cmath>


void ManipulatorP2P::start_node(ros::NodeHandle& nh, const std::vector<std::string>& motor_list)
{
	// get home_path param
	resolve_path.init(nh);

	for (const std::string& motor_name : motor_list)
		create_motor(nh, motor_name);

	const std::vector<std::string> params  = {"control_topic", "joint_states_topic", "rate", "epsilon", "time_limit"};
	for (const auto& param: params)
		ROS_ERROR_COND(!nh.hasParam(param), "Parameter '%s' for p2p control not found!", param.c_str());

	std::string control_topic_name;
	std::string js_topic_name;
	nh.getParam(params[0], control_topic_name);
	nh.getParam(params[1], js_topic_name);
	nh.getParam(params[2], RATE);
	nh.getParam(params[3], EPSILON);
	nh.getParam(params[4], TIME_LIMIT);
	control_topic = nh.advertise<sensor_msgs::JointState>(resolve_path(control_topic_name), 1);
	joint_states_topic = nh.subscribe(resolve_path(js_topic_name), 1, &ManipulatorP2P::joint_states_callback, this);

	action_server.reset(new ActionServerType(nh, "command", boost::bind(&ManipulatorP2P::action_callback, this, _1), false));
	action_server->start();
}

void ManipulatorP2P::create_motor(ros::NodeHandle& nh, const std::string& motor_name)
{
	ROS_DEBUG("Creating motor '%s'", motor_name.c_str());

	if (motors.count(motor_name)){
		ROS_ERROR("Motor with name '%s' already created", motor_name.c_str());
		return;
	}
	MotorControl motor;
	std::string description_name;
	
	bool success = true;
	success = success && nh.getParam(join_paths(motor_name, "joint_states_name"), motor.joint_states_name);
	success = success && nh.getParam(join_paths(motor_name, "control_name"), motor.control_name);
	success = success && nh.getParam(join_paths(motor_name, "description_name"), description_name);
	
	if (!success)
	{
		ROS_ERROR("Error while reading parameters for motor '%s'", motor_name.c_str());
		return;
	}

	description_name = join_paths(resolve_path("~/description/manipulator"), description_name);
	// success = success && nh.getParam(join_paths(description_name, "min_angle"), motor.min_angle);
	// success = success && nh.getParam(join_paths(description_name, "max_angle"), motor.max_angle);
	success = success && nh.getParam(join_paths(description_name, "max_vel"), motor.max_vel);
	success = success && nh.getParam(join_paths(description_name, "max_accel"), motor.max_accel);

	if (!success)
	{
		ROS_ERROR("Error while reading description parameters for motor '%s' (path: %s)", motor_name.c_str(), description_name.c_str());
		return;
	}

	motors[motor_name] = motor;
}

void ManipulatorP2P::action_callback(const ct_manipulator::ManipulatorP2PGoal::ConstPtr& goal)
{
	/*
		Принимает команду на управление моторами (goal->task) и производит управление.
		Задача - придти в указанную позицию из текущей

		Данные отправляет в control_topic.
		Обязательно проверить что первое сообщение с положенем моторов уже получено
	*/

	//configuration
	std::vector <Variable> motor_var;
	for (int i = 0; i < goal->task.name.size(); i++) 
		if(!set_variables(motor_var, goal->task.name[i], goal->task.position[i])) {
			action_result.done = false;
			action_server->setSucceeded( action_result );
			return;
		}
	double maxT = 0;
	for (const auto& var : motor_var){
		// updating max time
		maxT = (maxT < var.T)? var.T : maxT;
	}

	//maximum acceleration alignment
	for (auto & var : motor_var){
		var.T = maxT;
		var.maxA = pow(var.maxV, 2)/(var.T * var.maxV - var.L);
		var.Ta = var.T - var.L / var.maxV;

		// singularity test
		if (var.L < (pow(var.maxV, 2) / var.maxA))
		{ 	
			var.Ta = var.T - var.Ta;
			var.maxV = var.maxA * var.Ta;
		}
		int inv = var.endP > var.initP ? 1 : -1;
		var.maxA = var.maxA * inv;
		var.maxV = var.maxV * inv;
	}
	
	//movement
	ros::Rate r(RATE);
	bool success = false;
	const auto start_time = std::chrono::steady_clock::now();

	while(!action_server->isPreemptRequested() && ros::ok()) {

		auto cur_time = std::chrono::steady_clock::now();
        const auto dt = std::chrono::duration<double>(cur_time - start_time).count();

		sensor_msgs::JointState state;

		for (const auto & var: motor_var){
			double position;
			double velosity;
			// phase #1  rise
			if (dt < var.Ta )
			{
				velosity = dt * var.maxA;
				position = var.initP + 0.5 * var.maxA * pow(dt,2);
			}
			// phase #2  high tableland
			if ((var.Ta  <= dt) && (dt <= (var.T-var.Ta)))
			{
				velosity = var.Ta * var.maxA;
				position = var.initP + 0.5 * var.maxA * pow(var.Ta,2)   +  (var.maxV * (dt - var.Ta));
			}
			// phase #3  fall
			if (((var.T-var.Ta ) < dt) && (dt <= var.T))
			{
				velosity = (var.T - dt) * var.maxA;
				position = var.endP - 0.5 * var.maxA * pow(var.T-dt,2);
			}	
			// phase #4  low tableland
			if (var.T < dt)
			{
				velosity =  0;
				position =  var.endP;
			}
			std::string output_name = motors.at(var.motor_name).control_name;
			state.name.push_back(output_name);
            state.position.emplace_back(position);
            state.velocity.emplace_back(velosity);
			state.effort.push_back(0);
		}
		control_topic.publish(state);
		action_feedback.eta = maxT - dt;
		action_server->publishFeedback(action_feedback);

		double error = 0;
		for (const auto & var: motor_var)
			error += pow(motors.at(var.motor_name).current_pos - var.endP, 2);

		if ((maxT < dt) && (error < (EPSILON * motor_var.size())))
		{
			success = true;
			break;
		}
		if (TIME_LIMIT < dt-maxT)  break;		
		r.sleep();	
	}

	if (!success){
		ROS_WARN("p2p is preempted");
		sensor_msgs::JointState state;
		for (const auto & var: motor_var){
			std::string output_name = motors.at(var.motor_name).control_name;
			state.name.push_back(output_name);
            state.position.emplace_back(motors.at(var.motor_name).current_pos);
            state.velocity.emplace_back(0);
			state.effort.push_back(0);
		}
		control_topic.publish(state);
		action_server->setPreempted();
		return;
	}

	action_result.done = true;
	action_server->setSucceeded(action_result);
	return;
}

void ManipulatorP2P::joint_states_callback(const sensor_msgs::JointState::ConstPtr& msg)
{
	// Сохраняет текущее положение моторов
	// Условие сохранения: motors[name].joint_states_name == msg.name[i]

	for (auto & [motor_name, motor_control] : motors){
		bool flag = false;
		for (int i = 0; i < msg->name.size(); i++) {
			if (motor_control.joint_states_name != msg->name[i]) continue;
			motor_control.current_pos = msg->position[i];
			motor_control.is_received = true;
			flag = true; 
			break;
		}
		if(!flag)
			ROS_WARN("motor '%s' not recivet", motor_name.c_str());
	}
}


bool ManipulatorP2P::set_variables(std::vector<Variable> &motor_vars, const std::string &name, double goal_pos)
{
	// find motor
	bool flag = false;
	auto it = motors.begin();
	for (; it != motors.end(); ++it)
	{
		if (name != it->second.control_name) continue;
		flag = true; break;
	}
	auto &[motor_name, motor_control] = *it;
	// error handling 
	if (!flag)
	{
		ROS_WARN("unidentified motor received '%s' when set goal", name.c_str());
		return false;
	}
	// check the cur_position is exist
	if (!motor_control.is_received) {
		ROS_WARN("motor's '%s' cur_position is not received yet", motor_name.c_str());
		return false;
	}
	motor_vars.push_back({});
	auto& var = motor_vars.back();

	var.initP 	= motors[motor_name].current_pos;
	var.endP	= goal_pos;
	var.maxV 	= motor_control.max_vel;
	var.maxA 	= motor_control.max_accel;
	var.L 	= abs(var.endP - var.initP);
	var.Ta 	= var.maxV / var.maxA;
	var.T 	=  var.L/var.maxV + var.Ta;
	var.motor_name = motor_name;

	// singularity test
	if (var.L < (pow(var.maxV, 2) / var.maxA)){
		var.Ta = sqrt(var.L / var.maxA);
		var.T = 2 * var.Ta;
	}
	return true;
}

void ManipulatorP2P::spin()
{
	ros::spin();
}