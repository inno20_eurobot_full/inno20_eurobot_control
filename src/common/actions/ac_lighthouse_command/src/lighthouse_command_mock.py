#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Изменить состояние маяка (поднять его или опустить)
"""

import rospy
from eurobot.core import PathResolver

from ac_lighthouse_command.srv import LighthouseCommand, LighthouseCommandRequest, LighthouseCommandResponse


class LighthouseCommandNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self._server = rospy.Service("command", LighthouseCommand, self.server_callback)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.logwarn("Mock for action 'lighthouse_command' is loaded")

    def server_callback(self, req: LighthouseCommandRequest) -> LighthouseCommandResponse:
        """Callback for server."""
        return LighthouseCommandResponse(True)


def main():
    """Main entrypoint."""
    rospy.init_node("lighthouse_command", anonymous=True)

    main = LighthouseCommandNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
