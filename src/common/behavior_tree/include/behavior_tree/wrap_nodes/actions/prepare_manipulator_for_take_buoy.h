#pragma once

#include <ros/ros.h>

#include <behavior_tree/node_template/bt_action_node.h>
#include <ac_prepare_manipulator_for_take_buoy/PrepareManipulatorForTakeBuoyAction.h>
#include <geometry_msgs/PoseStamped.h>

#include <string>

class PrepareManipulatorForTakeBuoy : public BT::RosActionNode<ac_prepare_manipulator_for_take_buoy::PrepareManipulatorForTakeBuoyAction>
{
public:
    PrepareManipulatorForTakeBuoy( ros::NodeHandle& handle, const std::string& name, const std::string& server_name, const BT::NodeConfiguration & conf)
        :RosActionNode<ac_prepare_manipulator_for_take_buoy::PrepareManipulatorForTakeBuoyAction>(handle, name, server_name, conf) {}

    static BT::PortsList providedPorts();

    bool sendGoal(GoalType& goal) override;

    BT::NodeStatus onResult( const ResultType& res) override;
    
    BT::NodeStatus onFailedRequest(FailureCause failure) override;
    
    void halt() override;
};