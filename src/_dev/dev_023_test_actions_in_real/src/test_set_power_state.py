#!/usr/bin/env python3
import rospy
import sys

from ac_set_power_state.srv import SetPowerState, SetPowerStateRequest as SetReq


def main():
    """Main entrypoint."""
    client = rospy.ServiceProxy(
        "/robot1/actions/set_power_state/command",
        SetPowerState
    )

    print("send_goal")
    onb = len(sys.argv) > 1 and int(sys.argv[1]) == 1
    onm = len(sys.argv) > 2 and int(sys.argv[2]) == 1

    goal = SetReq
    goal.base_state = SetReq.STATE_POWER_ON if onb else SetReq.STATE_POWER_OFF
    goal.manip_state = SetReq.STATE_POWER_ON if onm else SetReq.STATE_POWER_OFF

    result = client(goal)

    print(f"Result: {result}")


if __name__ == "__main__":
    rospy.init_node("set_power_state")
    main()
