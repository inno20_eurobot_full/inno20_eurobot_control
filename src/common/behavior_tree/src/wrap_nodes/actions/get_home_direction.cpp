#include <behavior_tree/wrap_nodes/actions/get_home_direction.h>

BT::PortsList GetHomeDirection::providedPorts()
{
    return  {  
        BT::OutputPort<std::string>("home_direction")
    };
}

void GetHomeDirection::sendRequest(RequestType& request)
{

}

BT::NodeStatus GetHomeDirection::onResponse(const ResponseType& rep)
{
    setOutput("home_direction", "NORTH");
    if (rep.home == rep.HOME_UNKNOWN)
        return BT::NodeStatus::FAILURE;
    if (rep.home == rep.HOME_SOUTH)
        setOutput("home_direction", "SOUTH");
    return BT::NodeStatus::SUCCESS;
}

BT::NodeStatus GetHomeDirection::onFailedRequest(RosServiceNode::FailureCause failure)
{
    ROS_ERROR("GetHomeDirection request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}