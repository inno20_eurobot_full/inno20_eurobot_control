#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Коснуться маяка для его активации
"""

import rospy
from eurobot.core import PathResolver

import actionlib
import ac_manipulator_lighthouse_activation.msg
from sensor_msgs.msg import JointState

# goal message and the result message.
from ct_manipulator.msg import ManipulatorP2PGoal, ManipulatorP2PAction


class LighthouseActivation:
    """Wrapper for action."""
    Action = ac_manipulator_lighthouse_activation.msg.LighthouseActivationAction
    Feedback = ac_manipulator_lighthouse_activation.msg.LighthouseActivationFeedback
    Goal = ac_manipulator_lighthouse_activation.msg.LighthouseActivationGoal
    Result = ac_manipulator_lighthouse_activation.msg.LighthouseActivationResult


class LighthouseActivationNode:
    """Main class."""

    _result = LighthouseActivation.Result()

    def __init__(self):
        """Create main class."""
        resolve_path = PathResolver()

        p2p_name = resolve_path(rospy.get_param("p2p_action"))
        motor_names = rospy.get_param("motor_names")
        prepare_pos = rospy.get_param("prepare_pos")
        activate_pos = rospy.get_param("activate_pos")

        self.state_prepare = JointState(name=motor_names, position=prepare_pos)
        self.state_activate = JointState(name=motor_names, position=activate_pos)

        self.call_p2p = actionlib.SimpleActionClient(p2p_name, ManipulatorP2PAction)

        self._action_server = actionlib.SimpleActionServer(
            "command",
            LighthouseActivation.Action,
            execute_cb=self.action_callback,
            auto_start=False)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.loginfo("Main for action 'lighthouse_activation' is loaded")
        self.call_p2p.wait_for_server()
        self._action_server.start()

    def feedback_processing(self, fb):
        """Processing the feedback from actions"""
        pass

    def action_callback(self, goal: LighthouseActivation.Goal):
        """Callback for action."""
        self._result.res = True

        self.call_p2p.send_goal(
            ManipulatorP2PGoal(self.state_prepare), feedback_cb=self.feedback_processing
        )
        self.call_p2p.wait_for_result()
        self._result.res &= self.call_p2p.get_result().done

        self.call_p2p.send_goal(
            ManipulatorP2PGoal(self.state_activate), feedback_cb=self.feedback_processing
        )
        self.call_p2p.wait_for_result()
        # self._result.res &= self.call_p2p.get_result().done

        self.call_p2p.send_goal(
            ManipulatorP2PGoal(self.state_prepare), feedback_cb=self.feedback_processing
        )
        self.call_p2p.wait_for_result()
        self._result.res &= self.call_p2p.get_result().done

        self._action_server.set_succeeded(self._result)


def main():
    """Main entrypoint."""
    rospy.init_node("manipulator_lighthouse_activation", anonymous=True)

    main = LighthouseActivationNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
