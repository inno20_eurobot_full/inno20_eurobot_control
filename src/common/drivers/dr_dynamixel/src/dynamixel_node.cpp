#include <dr_dynamixel/dynamixel.hpp>


int main(int argc, char** argv)
{
    // initialize ros
    ros::init(argc, argv, "dynamixel");

    if (argc <= 1)
    {
        ROS_WARN("No controllers specified for dr_dynamixel");
        return 0;
    }

    std::stringstream ss;
    for (ssize_t i = 1; i < argc; i++)
        ss << (i > 1 ? " " : "") << argv[i];
    ROS_INFO("Start dr_dynamixel with controllers: '%s'", ss.str().c_str());

    std::vector<std::string> motor_names;
    motor_names.reserve(argc - 1);
    for (int i = 1; i < argc; i++)
        motor_names.emplace_back(argv[i]);

    ros::NodeHandle node;
    DynamixelDriver driver(node, motor_names);

    if (!driver.start()) {
        return 1;
    }
    
    while (!ros::isShuttingDown()) {
        ros::spin();
    }

    driver.stop();

    return 0;
}
