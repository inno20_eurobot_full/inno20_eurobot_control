#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Есть ли вражеский робот рядом?
"""

import rospy
import math
import time
import actionlib

import tf2_ros
import tf2_geometry_msgs
from move_base_msgs.msg import MoveBaseAction, MoveBaseResult, MoveBaseGoal
from geometry_msgs.msg import PoseStamped, Twist, Vector3


def quat_to_yaw(q):
    """Рассчет yaw из кватерниона."""
    siny_cosp = 2 * (q.w * q.z + q.x * q.y)
    cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z)
    return math.atan2(siny_cosp, cosy_cosp)


def sign_dist(x, y, q, x1, y1):
    """Вернуть знаковое расстояние до точки. Для направления использовать угол q."""
    A, B = math.cos(q), math.sin(q)
    C = -A * x1 - B * y1
    sign = 1 if A * x + B * y + C > 0 else -1
    d = ((x - x1) ** 2 + (y - y1) ** 2) ** 0.5
    return d * sign


class MarkovPlaner:
    """Main class."""

    _result = MoveBaseResult()

    def __init__(self):
        """Create main class."""
        self.robot_frame = rospy.get_param("robot_frame")
        self.cmd_vel_topic_name = rospy.get_param("cmd_vel")

        self.cfg = {
            "control_rate": rospy.get_param("control_rate"),
            "step1": rospy.get_param("step1"),
            "step2": rospy.get_param("step2"),
            "step3": rospy.get_param("step3"),
        }

        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)

    def start(self):
        """Subscribe to all neaded topics."""
        self.cmd_vel = rospy.Publisher(self.cmd_vel_topic_name, Twist, queue_size=1)
        self._as = actionlib.SimpleActionServer(
            "move_base", MoveBaseAction, execute_cb=self.execute_cb, auto_start=False
        )
        self._as.start()

    def move_pos_to_map(self, pos: PoseStamped):
        """Вернуть позицию во фрейме pos."""
        if pos.header.frame_id == "map":
            return (
                pos.pose.position.x,
                pos.pose.position.y,
                quat_to_yaw(pos.pose.orientation),
            )
        else:
            try:
                trans = self.tfBuffer.lookup_transform(
                    "map", pos.header.frame_id, rospy.Time(0)
                )
                pos = tf2_geometry_msgs.do_transform_pose(pos, trans)
                return (
                    pos.pose.position.x,
                    pos.pose.position.y,
                    quat_to_yaw(pos.pose.orientation),
                )
            except (
                tf2_ros.LookupException,
                tf2_ros.ConnectivityException,
                tf2_ros.ExtrapolationException,
            ):
                rospy.logwarn(
                    f"Markov planing: transformation to {pos.header.frame_id} not found"
                )
                return None

    def get_current_pos(self):
        """Получить текущую позицию робота."""
        try:
            trans = self.tfBuffer.lookup_transform(
                "map", self.robot_frame, rospy.Time(0)
            )
            return (
                trans.transform.translation.x,
                trans.transform.translation.y,
                quat_to_yaw(trans.transform.rotation),
            )
        except (
            tf2_ros.LookupException,
            tf2_ros.ConnectivityException,
            tf2_ros.ExtrapolationException,
        ):
            rospy.logwarn(
                f"Markov planing: transformation to {self.robot_frame} not found"
            )
            return None

    def execute_cb(self, goal: MoveBaseGoal):
        """Исполнительный блок action'а"""
        r = rospy.Rate(self.cfg["control_rate"])

        tx, ty, tq = self.move_pos_to_map(goal.target_pose)

        # rospy.logwarn(f"Markov planing: goal: {tx} {ty} {tq}")

        # rospy.logwarn(f"Markov planing: Step 01")

        # Поворот робота на месте по направлению к цели
        start_time = time.time_ns()
        while not rospy.is_shutdown():
            if self._as.is_preempt_requested():
                self.cmd_vel.publish(Twist())
                self._as.set_preempted()
                return
            rx, ry, rq = self.get_current_pos()
            pos_tq = math.atan2(ty - ry, tx - rx)

            dq = (pos_tq - rq + math.pi) % (math.pi * 2) - math.pi
            if abs(dq) < self.cfg["step1"]["delta_angle"]:
                break

            uq = max(
                -self.cfg["step1"]["max_rotate_speed"],
                min(dq, self.cfg["step1"]["max_rotate_speed"]),
            )
            k_warmup = min(
                (time.time_ns() - start_time) / 1e9 / self.cfg["step1"]["warmup_time"],
                1,
            )
            # rospy.logwarn(f"Markov planing: current pos: {rx} {ry} {rq} {uq}")
            self.cmd_vel.publish(Twist(Vector3(0, 0, 0), Vector3(0, 0, uq * k_warmup)))
            r.sleep()

        # rospy.logwarn(f"Markov planing: Step 02")

        # Езда до цели к поддержкой направления
        start_time = time.time_ns()
        while not rospy.is_shutdown():
            if self._as.is_preempt_requested():
                self.cmd_vel.publish(Twist())
                self._as.set_preempted()
                return
            rx, ry, rq = self.get_current_pos()
            pos_tq = math.atan2(ty - ry, tx - rx)
            dq = (pos_tq - rq + math.pi) % (math.pi * 2) - math.pi

            dst = sign_dist(rx, ry, math.pi + rq, tx, ty)
            if dst < self.cfg["step2"]["delta_dist"]:
                break

            uq = max(
                -self.cfg["step2"]["max_rotate_speed"],
                min(dq, self.cfg["step2"]["max_rotate_speed"]),
            )
            u = max(
                -self.cfg["step2"]["max_speed"],
                min(dst, self.cfg["step2"]["max_speed"]),
            )
            k_warmup = min(
                (time.time_ns() - start_time) / 1e9 / self.cfg["step2"]["warmup_time"],
                1,
            )
            # rospy.logwarn(f"Markov planing: current pos: {rx} {ry} {rq} {uq}")
            self.cmd_vel.publish(
                Twist(Vector3(u * k_warmup, 0, 0), Vector3(0, 0, uq * k_warmup))
            )
            r.sleep()

        # rospy.logwarn(f"Markov planing: Step 03")

        # Поворот на месте до указаного положения
        start_time = time.time_ns()
        while not rospy.is_shutdown():
            if self._as.is_preempt_requested():
                self.cmd_vel.publish(Twist())
                self._as.set_preempted()
                return
            rx, ry, rq = self.get_current_pos()

            dq = (tq - rq + math.pi) % (math.pi * 2) - math.pi
            if abs(dq) < self.cfg["step3"]["delta_angle"]:
                break

            uq = max(
                -self.cfg["step3"]["max_rotate_speed"],
                min(dq, self.cfg["step3"]["max_rotate_speed"]),
            )
            k_warmup = min(
                (time.time_ns() - start_time) / 1e9 / self.cfg["step3"]["warmup_time"],
                1,
            )
            # rospy.logwarn(f"Markov planing: current pos: {rx} {ry} {rq} {uq}")
            self.cmd_vel.publish(Twist(Vector3(0, 0, 0), Vector3(0, 0, uq * k_warmup)))
            r.sleep()

        self._as.set_succeeded(self._result)


def main():
    """Main entrypoint."""
    rospy.init_node("markov_planer", anonymous=True)

    main = MarkovPlaner()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
