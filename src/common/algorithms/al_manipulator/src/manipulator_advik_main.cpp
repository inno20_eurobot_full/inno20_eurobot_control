#include <al_manipulator/manipulator_advik.hpp>

int main(int argc, char** argv)
{
	// initialize ros
	ros::init(argc, argv, "manipulator_advik");

	ros::NodeHandle nh;

	// start program
	ManipulatorAdvIK main;
	if (!main.start_node(nh)) return -1;
	main.spin();

	return 0;
}