#include <wp_gripper_effort_control/simple_gripper_wrapper.hpp>

#include <string>


SimpleGripperWrapper::SimpleGripperWrapper(ros::NodeHandle& nh){
	resolve_path.init(nh);

	//checking that the 'topic_type' is 'joint_state' 
	SimpleGripperWrapper::check_param <std::string> (nh, "gripper/topic_type", "joint_state");
	
	takeParameters(nh);

	pub_topic = resolve_path(pub_topic);
}

SimpleGripperWrapper::~SimpleGripperWrapper(){
	pub.shutdown();
	sub.shutdown();
}


/*
 *	Забирает параметры из конфигов
 */
void SimpleGripperWrapper::takeParameters(ros::NodeHandle& nh){

	const char* param_name;

	// Required parameters:

	param_name = "gripper/topic";
	ROS_ASSERT_MSG(nh.getParam(param_name, pub_topic), "Parameter '%s' not found", param_name);

	param_name = "gripper/motor_name";
	ROS_ASSERT_MSG(nh.getParam(param_name, motor_name), "Parameter '%s' not found", param_name);
	
	// Optional parameters:
	// set default value (third argument) if the parameter could not be retrieved
	
	param_name = "gripper/gear_mult";
	if (!nh.param <double> (param_name, gear_mult, 1))
		ROS_WARN ("Parameter '%s' not found. Will be substituted with the default value", param_name);
}

/*
 *	Слушает топик 'command' (тип float32)
 */
void SimpleGripperWrapper::start_node(ros::NodeHandle& nh)
{
	
	ROS_INFO("Starting wp_gripper_effort_control");
	// ROS_INFO("Gripper_effort_wrapper: publish in '%s'", pub_topic.c_str());

	pub = nh.advertise <sensor_msgs::JointState> (pub_topic, 1);
    sub = nh.subscribe <std_msgs::Float32> ("command", 1, &SimpleGripperWrapper::goalCallback, this);

}

void SimpleGripperWrapper::goalCallback(std_msgs::Float32 effort){

	sensor_msgs::JointState goal;
	goal.name = {motor_name} ;
	goal.effort = {effort.data / gear_mult};
	goal.position =  {0};
	goal.velocity =  {0};

	pub.publish(goal);
}

void SimpleGripperWrapper::spin()
{
	ros::spin();
}    
