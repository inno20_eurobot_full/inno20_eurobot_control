// Copyright (c) 2019 Samsung Research America
// Copyright (c) 2020 Davide Faconti
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <behaviortree_cpp_v3/action_node.h>
#include <behaviortree_cpp_v3/bt_factory.h>
#include <actionlib/client/simple_action_client.h>
#include <ros/ros.h>
#include <behavior_tree/convertes/bt_conversions.h>

#include <string>
#include <memory> 

namespace BT
{

/** Helper Node to call an actionlib::SimpleActionClient<>
 * inside a BT::ActionNode.
 *
 * Note that the user must implement the methods:
 *
 *  - sendGoal
 *  - onResult
 *  - onFailedRequest
 *  - halt (optionally)
 *
 */
template<class ActionT>
class RosActionNode : public BT::ActionNodeBase
{
protected:
  RosActionNode(ros::NodeHandle& nh, const std::string& name, const std::string& server_name, const BT::NodeConfiguration & conf);
  
public:
  using BaseClass  = RosActionNode<ActionT>;
  using ActionClientType = actionlib::SimpleActionClient<ActionT>;
  using ActionType = ActionT;
  using GoalType   = typename ActionT::_action_goal_type::_goal_type;
  using ResultType = typename ActionT::_action_result_type::_result_type;

  enum FailureCause {
    MISSING_SERVER = 0,
    ABORTED_BY_SERVER = 1,
    REJECTED_BY_SERVER = 2
  };

  RosActionNode() = delete;
  virtual ~RosActionNode() = default;

  /// These ports will be added automatically if this Node is
  /// registered using RegisterRosAction<DeriveClass>()
  static PortsList providedPorts();

  /// Method called when the Action makes a transition from IDLE to RUNNING.
  /// If it return false, the entire action is immediately aborted, it returns
  /// FAILURE and no request is sent to the server.
  virtual bool sendGoal(GoalType& goal) = 0;

  /// Method (to be implemented by the user) to receive the reply.
  /// User can decide which NodeStatus it will return (SUCCESS or FAILURE).
  virtual NodeStatus onResult( const ResultType& res) = 0;

  /// Called when a service call failed. Can be overriden by the user.
  virtual NodeStatus onFailedRequest(FailureCause failure);

  /// If you override this method, you MUST call this implementation invoking:
  ///    BaseClass::halt()
  void halt() override;

protected:
  std::shared_ptr<ActionClientType> action_client_;
  ros::NodeHandle& node_;

  BT::NodeStatus tick() override;
};






/// Method to register the service into a factory.
/// It gives you the opportunity to set the ros::NodeHandle.
template <class DerivedT> static
void RegisterRosAction(BT::BehaviorTreeFactory& factory,
                        ros::NodeHandle& node_handle,
                        const std::string& registration_ID,
                        const std::string& server_name)
{
  NodeBuilder builder = [&node_handle, &server_name](const std::string& name, const NodeConfiguration& config) 
                        {
                          return std::make_unique<DerivedT>(node_handle, name, server_name, config );
                        };

  TreeNodeManifest manifest;
  manifest.type = getType<DerivedT>();
  manifest.ports = DerivedT::providedPorts();
  manifest.registration_ID = registration_ID;
  const auto& basic_ports = RosActionNode< typename DerivedT::ActionType>::providedPorts();
  manifest.ports.insert( basic_ports.begin(), basic_ports.end() );
  factory.registerBuilder( manifest, builder );
}




template<class ActionT>
RosActionNode<ActionT>::RosActionNode(ros::NodeHandle &nh, const std::string &name, const std::string& server_name, const BT::NodeConfiguration &conf)
  : BT::ActionNodeBase(name, conf), node_(nh)
{
  action_client_ = std::make_shared<ActionClientType>( node_, server_name, true );
  ROS_INFO("'%s': waitForserver", name.c_str());
  action_client_->waitForServer();
  ROS_INFO("'%s': isReady", name.c_str());
}

template<class ActionT>
PortsList RosActionNode<ActionT>::providedPorts()  
{
  return  {
    InputPort<unsigned>("timeout", 500, "timeout to connect (milliseconds)")
  };
}

template<class ActionT>
NodeStatus RosActionNode<ActionT>::onFailedRequest(RosActionNode<ActionT>::FailureCause failure) 
{
  return NodeStatus::FAILURE;
}

template<class ActionT>
void RosActionNode<ActionT>::halt() 
{
  if( status() == NodeStatus::RUNNING )
    action_client_->cancelGoal();
  setStatus(NodeStatus::IDLE);
}

template<class ActionT>
BT::NodeStatus RosActionNode<ActionT>::tick()
{
  unsigned msec = getInput<unsigned>("timeout").value();
  ros::Duration timeout(static_cast<double>(msec) * 1e-3); // from msec to sec
  
  bool connected = action_client_->waitForServer(timeout);
  if( !connected ){
    return onFailedRequest(MISSING_SERVER);
  }
    
  // first step to be done only at the beginning of the Action
  if (status() == BT::NodeStatus::IDLE) {
    // setting the status to RUNNING to notify the BT Loggers (if any)
    setStatus(BT::NodeStatus::RUNNING);
    
    GoalType goal;
    bool valid_goal = sendGoal(goal);
    if( !valid_goal )
    {
      return NodeStatus::FAILURE;
    }
    action_client_->sendGoal(goal);
  }
    
  // RUNNING
  auto action_state = action_client_->getState();
  
  // Please refer to these states
  using ClientGoalState  = actionlib::SimpleClientGoalState;

  if( action_state == ClientGoalState::PENDING ||
      action_state == ClientGoalState::ACTIVE )
    return NodeStatus::RUNNING;

  if( action_state == ClientGoalState::SUCCEEDED)
    return onResult( *action_client_->getResult());

  if( action_state == ClientGoalState::ABORTED)
    return onFailedRequest( ABORTED_BY_SERVER );

  if( action_state == ClientGoalState::REJECTED)
    return onFailedRequest( REJECTED_BY_SERVER );

  // FIXME: is there any other valid state we should consider?
  throw std::logic_error("Unexpected state in RosActionNode::tick()");
}

}  // namespace BT
