#pragma once

#include <ros/ros.h>

#include <std_msgs/Float32.h>
#include <sensor_msgs/JointState.h>
#include <actionlib/server/simple_action_server.h>

#include <ct_manipulator_gripper/GripperControlAction.h>
#include <ct_manipulator_gripper/GripperControlState.h>

#include <string>
#include <vector>

#include <eurobot/core/path_utils.hpp>


class GripperControl
{    

    ros::Subscriber sub_position;
    ros::Publisher pub_effort;

    using GripperServer = actionlib::SimpleActionServer<ct_manipulator_gripper::GripperControlAction>;
    GripperServer server;

    enum class GripperState : int32_t
    {
        STATE_UNDEFINED,
        STATE_OPEN,
        STATE_CLOSE,
        STATE_WITH_BUOY
    };
    GripperState gripper_state = GripperState::STATE_UNDEFINED;
    using ServiceRequest = ct_manipulator_gripper::GripperControlStateRequest;
    using ServiceResponse = ct_manipulator_gripper::GripperControlStateResponse;
    ros::ServiceServer service;

    PathResolver resolve_path;
    
    struct {
        std::string PATH;
        std::string MOTOR_NAME;
    } pos_topic;

    struct { 
        std::string PATH; 
    } eff_topic;

    struct {
        double MAX = 0.6;
        double MIN = 0.07;
        double NO_POWER = 0;
    } power;

    struct {
        double OPEN = 1.2;
        double MIDDLE = 0.35;
        double CLOSE_WITH_BUOY_MAX = 0.25;
        double CLOSE_WITH_BUOY_MIN = 0.08;
        double CLOSE = 0;

        double curr_pos;
    } pos;

    struct {
        double EPSILON = 0.005;
        double MAGNITUDE = 0.01; // в позиции манипулятора
        double TIME_LIMIT = 2;  // В секундах
        int BUFFER_SIZE = 5;
    } config;

public:
    explicit GripperControl(ros::NodeHandle& nh);

    void start_node(ros::NodeHandle& hn);

    bool service_callback(ServiceRequest& req, ServiceResponse& res);

    void spin();

private:
    void takeParameters(ros::NodeHandle& nh);

    void execute(const ct_manipulator_gripper::GripperControlGoalConstPtr& goal);

    bool getSrvState(ServiceResponse& res_state);

    void updatePosition(const sensor_msgs::JointStateConstPtr& data);

    double getPower(bool closing) const;
};




