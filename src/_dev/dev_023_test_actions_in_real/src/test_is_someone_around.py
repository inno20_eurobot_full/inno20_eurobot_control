#!/usr/bin/env python3
import rospy

from con_is_someone_around.srv import IsSomeoneAround, IsSomeoneAroundRequest


def main():
    """Main entrypoint."""
    client = rospy.ServiceProxy(
        "/robot1/conditions/is_someone_around/command",
        IsSomeoneAround
    )

    print("send_goal")
    goal = IsSomeoneAroundRequest()
    goal.dist = 0.5
    result = client(goal)

    print(f"Result: {result}")


if __name__ == "__main__":
    rospy.init_node("test_is_someone_around")
    main()
