## Code structure

```
src
├── _dev
├── _final
├── common
│   ├── algorithms
│   ├── description
│   ├── drivers
│   ├── eurobot
│   ├── wrappers
├── eurobot
├── robot1
├── supervisor
└── world
```

## Development and experimentation

Проекты, предназначенные для тестирования, отладки, экспериментов, разработки нового модуля, хранятся в директории `_dev`. Пакеты должны быть предназначены для тестирования одного конкретного модуля. Если необходимо приступить к тестированию следующего модуля, нужно создать новый пакет. Пакеты не предназначены для коллективной работы. Лучше скопировать пакет несколько раз, чем работать вдвоем над одним пакетом.

Имя пакетов должно соответствовать шаблону `dev_###_*`, где `###` - трехзначный порядковый номер проекта, а `*` - краткое описание пакета. Во время слияния веток порядковые номера пакетов могут совпадать, но лучше не допускать этого.

В старые пакеты, которые больше не нужны, необходимо поместить пустой файл с именем `CATKIN_IGNORE`, чтобы игнорировать этот пакет при сборке проекта.

В каждом пакете в файле package.xml обязательно должно быть как минимум одно поле maintainer, содержащее имя, фамилию и email разработчика, работавшего над этим пакетом.


## Final solutions

Проекты, содержащие готовое решение, хранятся в папке `_final`.

Имя пакетов должно соответствовать шаблону `final_*`, где `*` - краткое описание пакета.

## Common

Пакеты, содержащие общие для всего проекта модули хранятся в папке `common`. Эта директория будет содержать почти весь код. Все пакеты, находящиеся тут решают определенных кластер задач и не привязаны только к одной задаче. Другими словами, все параметры должны быть вынесены в сервер параметров и подгружаться при старте. Таким образом один пакет можно будет использовать несколько раз в разных местах. Например, тут может находится алгоритм, который будет поворачивать изображение с камеры. Но с какой конкретно камеры и на какой конкретно угол алгоритм должен брать из сервера параметров.

### common/description (LowLevel)

Пакеты, содержащие универсальные модели и материалы к ним (стаканик, поле, текстура ArUco-маркера), а так же макросы, использующиеся в большом количестве моделей.

### common/drivers (LowLevel)

Пакеты, содержащие универсальные драйвера, хранятся в директории `common/drivers`. Такие пакеты предоставляют интерфейс к "железу" и не привязаны к какой-то конкретной части робота. Все необходимые конфиги передаются через сервер параметров ROS. Сам пакет может содержать только конфиги, которые ни при каких условиях не могут быть изменены.

Имя пакетов должно соответствовать шаблону `dr_*`, где `*` - краткое описание пакета.

Пример хорошего драйвера - `ros_control`. Этому пакеты все равно сколько моторов нужно будет контролировать, какие joint'ы нужно будет контролировать, как их контролировать и так далее. Все эти параметры передаются через сервер параметров, за счет чего драйвер является универсальным.

Пример плохого драйвера - драйвер камеры, который при запуске заранее знает на каком порту сидит камера, какое у нее разрешение, ориентация и так далее. Изменить эти параметры, например, при отладке пакета не представляется возможным. Точно как и запуск нескольких драйверов одновременно для обработки нескольких камер.

### common/wrappers (LowLevel)

Пакеты, содержащие универсальные обертки, хранятся в директории `common/wrappers`. Такие пакеты предоставляют более удобный и желательно железо-независимый интерфейс к устройствам.

Имя пакетов должно соответствовать шаблону `wp_*`, где `*` - краткое описание пакета.

### common/algorithms (HighLevel)

Пакеты, содержащие различные алгоритмы (пока не доконца проработан). Тут будут такие алгоритмы как FK, IK для манипулятора, поиск стаканчика в облаке точек, а так же локализация.

Имя пакетов должно соответствовать шаблону `al_*`, где `*` - краткое описание пакета.

### common/control (HighLevel)

Пакеты, содержащие различные контроллеры. Тут будет planing, p2p для маниупялтора.

Имя пакетов должно соответствовать шаблону `ct_*`, где `*` - краткое описание пакета.

### common/actions (Behavior tree API)

Пакеты, содержащие различные контроллеры. Тут будет planing, p2p для маниупялтора.

Имя пакетов должно соответствовать шаблону `ac_*`, где `*` - краткое описание пакета.

### common/conditions (Behavior tree API)

Пакеты, содержащие различные контроллеры. Тут будет planing, p2p для маниупялтора.

Имя пакетов должно соответствовать шаблону `con_*`, где `*` - краткое описание пакета.

### common/behavior_tree (Behavior tree API)

Пакеты, содержащие различные контроллеры. Тут будет planing, p2p для маниупялтора.

Имя пакетов должно соответствовать шаблону `con_*`, где `*` - краткое описание пакета.

## Eurobot

Библиотеки, необходиме для работы всей системы. Тут хранятся c++ и python библиотеки eurobot, содержащие общие для многих модулей функции. А так же cmake файлы, которые используются в проекте.

## Robot1

Пакеты, относящиеся только к первому роботу, хранятся в папке `robot1`. В основном тут находятся конфиги и launch-файлы, которые запускают нужные алгоритмы из `common` и настраивают их. Тут может находится код, который очень сильно привязан к роботу и не может даже в теории нигде больше использоваться. Например, программа, определяющая положение камеры в пространстве может находится тут, так как едва ли у нас еще когда-нибудь будет робот, у которого положение камеры привязано к первому звену манипулятора.

Имя пакетов должно соответствовать шаблону `robot1_*`, где `*` - краткое описание пакета.

В этой папке будут такие пакеты как:
 - `robot1_drivers` - (LowLevel) пакет, содержащий конфиги и загрузчики для всех драйверов для первого робота
 - `robot1_wrappers` - (LowLevel) то же самое, но уже wrapper
 - `robot1_description` - (LowLevel) содержится описание робота
 - `robot1_algorithms` - (HighLevel) содержаться запуски различных алгоритмов и настройки их
 - `robot1_control` - (HighLevel) содержаться запуски различных контроллеров и настройки их
 - `robot1_action` - (BehaviorTree API) содержатся action'ы, которыми будет управлять Behaviour tree
 - `robot1_conditions` - (BehaviorTree API) содержатся condition'ы, которые будет использовать Behaviour tree.

Стоит отметить что в таких пакетах содержится **только** то что относится только к первому роботу. Все остальное содержится в `common`. Таким образом модель манипулятора первого робота хранится в папке `robot1`, но собирается из максоров, хранящихся в `common/cm_description`

## Supervisor

То же что и robot1, но для supervisor'а. Supervisor это штуковина, занимающаяся общей работой, необходимой для управления роботом. Например supervisor следит за обстоновкой на поле (где стоят стаканчики, куда повернут флюгер).

## World

Сущность, поддерживающая работу поля и всех его частей в симуляции. На данный момент содержит только модельки. В бадущем планировалось написать скрипты, обуспечивающие работу полю (вращать флюгер, поворачивать ветроуказатели)