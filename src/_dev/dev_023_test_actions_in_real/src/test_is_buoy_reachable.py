#!/usr/bin/env python3
import rospy

from con_is_buoy_reachable.srv import IsBuoyReachable
from geometry_msgs.msg import PoseStamped


def main():
    """Main entrypoint."""
    client = rospy.ServiceProxy(
        "/robot1/conditions/is_buoy_reachable/command",
        IsBuoyReachable
    )

    print("send_goal")
    goal = PoseStamped()
    goal.pose.position.x = 0.36
    goal.pose.position.y = 0.00
    goal.pose.position.z = 0.
    result = client(goal)

    print(f"Result: {result}")


if __name__ == "__main__":
    rospy.init_node("is_buoy_reachable")
    main()
