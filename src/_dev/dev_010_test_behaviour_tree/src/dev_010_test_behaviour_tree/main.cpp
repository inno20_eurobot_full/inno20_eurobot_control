#include <ros/ros.h>
#include <ros/package.h>

#include <behaviortree_cpp_v3/bt_factory.h>
#include <behaviortree_cpp_v3/loggers/bt_cout_logger.h>
#include <behaviortree_cpp_v3/loggers/bt_file_logger.h>
#include <behaviortree_cpp_v3/loggers/bt_minitrace_logger.h>
#include <behaviortree_cpp_v3/loggers/bt_zmq_publisher.h>

#include "dev_010_test_behaviour_tree/logger/rosout_logger.h"
#include "dev_010_test_behaviour_tree/nodes/doraiseflag.h"

    
int main(int argc, char **argv)
{
  ros::init(argc, argv, "behavior_tree");
  ros::NodeHandle nh;

  BT::BehaviorTreeFactory factory;

  BT::RegisterRosAction<DoRaiseFlag>(factory, "doRaiseFlag", nh);

  std::string path = ros::package::getPath("dev_010_test_behaviour_tree");
  auto tree = factory.createTreeFromFile(path+"/tree/raise_flag_after_5sec.xml");


  ROS_INFO("#####BEHAVIOR_TREE#####");
  
  // LOGGERS
  printTreeRecursively(tree.rootNode());
  BT::RosoutLogger logger(tree.rootNode());  
  // BT::StdCoutLogger logger_cout(tree);
  // BT::FileLogger logger_file(tree, "raise_flag.fbl");
  // BT::MinitraceLogger logger_minitrace(tree, "raise_flag.json");
  #ifdef ZMQ_FOUND
    BT::PublisherZMQ publisher_zmq(tree);
  #endif
  //END LOGGERS

  BT::NodeStatus status = BT::NodeStatus::RUNNING;
  while(ros::ok() && (status == BT::NodeStatus::IDLE || status == BT::NodeStatus::RUNNING)){
    ros::spinOnce();
    status = tree.tickRoot();
    ros::Duration sleep_time(0.01);
    sleep_time.sleep();
  }


  return 0;
}