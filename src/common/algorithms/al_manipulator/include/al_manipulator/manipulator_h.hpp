#pragma once

#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <al_manipulator/ManipulatorEndPos.h>
#include <al_manipulator/ManipulatorTF.h>
#include <al_manipulator/ManipulatorIK.h>
#include <al_manipulator/ManipulatorH.h>

#include <vector>
#include <string>

#include <eurobot/core/path_utils.hpp>
#include <eurobot/math/linalg.hpp>
#include <al_manipulator/dh_params.hpp>


class ManipulatorH
{
private:
    PathResolver resolve_path;

    std::vector<std::string> motor_names;
    DHParams dh_params;
    double base_height;

    ros::ServiceServer service;

    bool service_callback(al_manipulator::ManipulatorH::Request& req, al_manipulator::ManipulatorH::Response& res);

    void load_dh_params(const ros::NodeHandle& nh, DHParams* dh_params);
    void build_manipulator(const DHParams& dh);

    bool process(const al_manipulator::ManipulatorEndPos& pos, al_manipulator::ManipulatorTF& H);
public:
    static Matrix getMatrixH(const al_manipulator::ManipulatorEndPos& pos, double base_height, const DHParams& dh_params);

    void start_node(ros::NodeHandle& nh);

    void spin();
};