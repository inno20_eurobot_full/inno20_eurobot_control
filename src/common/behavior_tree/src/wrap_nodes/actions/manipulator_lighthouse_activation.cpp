#include <behavior_tree/wrap_nodes/actions/manipulator_lighthouse_activation.h>

BT::PortsList LighthouseActivation::providedPorts() 
{
    return {  };
}

bool LighthouseActivation::sendGoal(GoalType& goal)
{
    return true;
}

BT::NodeStatus LighthouseActivation::onResult( const ResultType& res)
{
    if (res.res == true)
        return BT::NodeStatus::SUCCESS;
    return BT::NodeStatus::FAILURE;
}

BT::NodeStatus LighthouseActivation::onFailedRequest(FailureCause failure)
{
    ROS_ERROR("LighthouseActivation request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}

void LighthouseActivation::halt()
{
    if( status() == BT::NodeStatus::RUNNING ) {
        ROS_WARN("LighthouseActivation aborted(halted)");
        BaseClass::halt();
    }
}