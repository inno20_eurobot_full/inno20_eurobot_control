#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Уточнить координаты буйка, используя данные с realsense
"""

import rospy
from eurobot.core import PathResolver

from ac_refine_buoy_pos.srv import RefineBuoyPos, RefineBuoyPosRequest, RefineBuoyPosResponse


class RefineBuoyPosNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self._server = rospy.Service("command", RefineBuoyPos, self.server_callback)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.logwarn("Mock for action 'refine_buoy_pos' is loaded")

    def server_callback(self, req: RefineBuoyPosRequest) -> RefineBuoyPosResponse:
        """Callback for server."""
        return RefineBuoyPosResponse(pos=req.pos, done=True)


def main():
    """Main entrypoint."""
    rospy.init_node("refine_buoy_pos", anonymous=True)

    main = RefineBuoyPosNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
