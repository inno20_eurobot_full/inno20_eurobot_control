#! /usr/bin/env python3
import rospy
import time
from sensor_msgs.msg import JointState
from std_msgs.msg import Float32

global pub_0_pos
global pub_0_vel
global cur_speed
global start_speed
global finish_speed
global start_time
global flag


def callback1(msg: JointState):
    """Callback for JointState"""
    global pub_0_pos
    global pub_0_vel
    global cur_speed
    global start_speed
    global finish_speed
    global start_time
    global flag
    a = 0
    for x in range(len(msg.name)):
        if msg.name[x] == "motor4":
            a = x
            break
    else:
        print("no motor")
        return
    pub_0_pos.publish(msg.position[a])
    cur_speed = msg.velocity[a]
    pub_0_vel.publish(cur_speed)

    if flag == 0:
        return

    if finish_speed > start_speed:
        inv = -1
    else:
        inv = 1

    if inv * (finish_speed - cur_speed) <= 0.1:
        print("the motor has finished accelerating")
        print("Velocity from :", start_speed, "to :", finish_speed)
        print("Time is:", time.time() - start_time)
        print("~~~~~~~~~")
        flag = 0


def callback2(msg: JointState):
    """Callback for JointState"""
    global flag
    global finish_speed
    global start_speed
    global start_time
    global cur_speed
    a = 0
    for x in range(len(msg.name)):
        if msg.name[x] == "motor4":
            a = x
            break
    else:
        print("no motor")
        return
    finish_speed = msg.velocity[a]
    start_speed = cur_speed
    start_time = time.time()
    flag = 1


def main():
    """Main entrypoint."""
    rospy.init_node("rqt_test", anonymous=True)
    rospy.Subscriber(
        "/robot1/middleware/manipulator/joint_states", JointState, callback1
    )
    rospy.Subscriber("/robot1/middleware/manipulator/command", JointState, callback2)
    global pub_0_pos
    global pub_0_vel
    global flag
    flag = 0
    pub_0_pos = rospy.Publisher("motor0_pos", Float32, queue_size=1)
    pub_0_vel = rospy.Publisher("motor0_ves", Float32, queue_size=1)

    rospy.spin()


if __name__ == "__main__":
    main()
