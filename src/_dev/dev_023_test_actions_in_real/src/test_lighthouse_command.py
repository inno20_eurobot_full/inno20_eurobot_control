#!/usr/bin/env python3
import rospy
import sys

from ac_lighthouse_command.srv import (
    LighthouseCommand,
    LighthouseCommandRequest as LCReq,
)


def main():
    """Main entrypoint."""
    if len(sys.argv) == 2:
        _, command = sys.argv
        use_motor = "1"
    elif len(sys.argv) == 3:
        _, command, use_motor = sys.argv
    else:
        print(f"Using: {sys.argv[0]} command use_motor")

    client = rospy.ServiceProxy(
        "/robot1/actions/lighthouse_command/command", LighthouseCommand
    )

    print("send_goal")

    result = client(
        LCReq(LCReq.RAISE if command != "0" else LCReq.LOWER, use_motor != "0")
    )
    print(f"Result: {result}")


if __name__ == "__main__":
    rospy.init_node("refine_buoy_pos")
    main()
