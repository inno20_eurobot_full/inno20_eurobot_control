"""Module for calculate and store odometry."""

from math import sin, cos


class OdometryAlgebra:
    """Class for compute and store odometry."""

    def __init__(self, wheel_circle, wheel_offset, left_pos, right_pos):
        """Create class for odometry."""
        self._wheel_circle, self._wheel_offset = wheel_circle, wheel_offset
        self.x = self.y = self.a = 0
        self.lin = self.ang = 0
        self._left_pos, self._right_pos = left_pos, right_pos

    def update(self, left_pos, right_pos, deltatime):
        """Update odometry with given data."""
        left_inc = (left_pos - self._left_pos) * self._wheel_circle
        right_inc = (right_pos - self._right_pos) * self._wheel_circle
        self._left_pos, self._right_pos = left_pos, right_pos

        lin = (left_inc + right_inc) * 0.5
        ang = (right_inc - lin) / self._wheel_offset

        if abs(ang) < 1e-6:
            self.a += ang * 0.5
            self.x += lin * cos(self.a)
            self.y -= lin * sin(self.a)
            self.a += ang * 0.5
        else:
            radius = lin / ang
            angle = self.a + ang
            self.x += radius * (sin(angle) - sin(self.a))
            self.y -= radius * (cos(angle) - cos(self.a))
            self.a = angle

        self.lin = lin / deltatime
        self.ang = ang / deltatime
