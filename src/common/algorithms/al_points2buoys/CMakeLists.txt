cmake_minimum_required(VERSION 3.0.2)
project(al_points2buoys)

## Compile as C++11, supported in ROS Kinetic and newer
add_definitions(-std=c++17 -O2)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
  sensor_msgs
  message_generation
  eurobot
  pcl_ros
)

eurobot_generate_messages(DEPENDENCIES std_msgs sensor_msgs geometry_msgs)


catkin_package(
  DEPENDS roscpp
  CATKIN_DEPENDS message_runtime
)

include_directories(include ${catkin_INCLUDE_DIRS})

file(GLOB_RECURSE SRCS src/*.cpp)
add_executable(points2buoys ${SRCS})
# Do not compile program before compiling all neaded messages/services.
add_dependencies(points2buoys ${PROJECT_NAME}_generate_messages_cpp ${catkin_EXPORTED_TARGETS})
target_link_libraries(points2buoys ${catkin_LIBRARIES})