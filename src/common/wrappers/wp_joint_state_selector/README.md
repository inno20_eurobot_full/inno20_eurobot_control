# wp_joint_state_selector

Обертка позволяющая выбирать и объединять JointState с нескольких топиков.

## Принцип работы

Нода подписывается на указанные через сервер параметров топики. При получении сообщений на них, данные сохраняюся и те joint, на которые пришли сообщения, помечаютя как обновленные. Как только хотя бы PUSH_WHEN процентов контроллеров будет обновлена, сообщение собирается и отправляется в топик.

## Особенности

Имеется возможность указывать путь относительно дома. Для этого нужно передать путь до дома через параметр `home_path` и в пути до топика использовать `~/`.

## Использование

Пример файла конфигурации:
```
motor0a:
  input_name: "motor0"
  joint_state_topic: "~/driver/ros_control_1/joint_states"

motor1a:
  input_name: "motor1"
  joint_state_topic: "~/driver/ros_control_1/joint_states"

motor2a:
  input_name: "motor2"
  joint_state_topic: "~/driver/ros_control_1/joint_states"
```

launch-файл для соответствующиего файла конфигурации:
```
<rosparam file="$(find ...)/config/config.yaml" command="load"/>
<include file="$(find wp_joint_state_selector)/launch/joint_state.xml">
        <arg name="home_path" value="/robot1"/>
        <arg name="controllers" value="motor0a motor1a motor2a"/>
</include>
```