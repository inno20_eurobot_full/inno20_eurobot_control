#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Есть ли сейчас в гриппере буек?
"""

import rospy
from eurobot.core import PathResolver

from con_is_buoy_in_gripper.srv import (
    IsBuoyInGripper,
    IsBuoyInGripperRequest,
    IsBuoyInGripperResponse,
)
from ct_manipulator_gripper.srv import (
    GripperControlState,
    GripperControlStateRequest,
    GripperControlStateResponse,
)


class IsBuoyInGripperNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self.gipper_service_name = self.resolve_path(rospy.get_param("gripper_service"))

        self._service_client = rospy.ServiceProxy(
            self.gipper_service_name, GripperControlState
        )

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.loginfo("Main for condition 'is_buoy_in_gripper' is loaded")
        self._service_client.wait_for_service()
        self._server = rospy.Service("command", IsBuoyInGripper, self.server_callback)

    def server_callback(self, req: IsBuoyInGripperRequest) -> IsBuoyInGripperResponse:
        """Callback for server."""
        gripper_state = self._service_client(GripperControlStateRequest()).state
        if gripper_state == GripperControlStateResponse.STATE_WITH_BUOY:
            return True
        return False


def main():
    """Main entrypoint."""
    rospy.init_node("is_buoy_in_gripper", anonymous=True)

    main = IsBuoyInGripperNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
