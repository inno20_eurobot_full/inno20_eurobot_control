#pragma once

#include <ros/ros.h>
#include <sensor_msgs/JointState.h>

#include <string>
#include <unordered_map>
#include <vector>

#include <eurobot/core/path_utils.hpp>


class JointStateWrapper
{
    // Push message when at least '%f' percent of it is new. Can be changed via ros param
    double PUSH_WHEN = 100;

    using TopicInputName =  std::string;
    using TopicOutputName =  std::string;

    using MotorInputName =  std::string;
    using MotorOutputName =  std::string;

    struct MotorParam
    {
        MotorInputName input_name;
        MotorOutputName output_name;

        TopicInputName input_topic;
        TopicOutputName output_topic;

        double gear_mult;
        double offset;
    };

    struct MotorState
    {   
        bool   is_update = false;
        double effort;
        double velocity;
        double position;
    };

    using RelatedInputMotors = std::unordered_map <MotorInputName, MotorParam>;
    using RelatedOutputMotors = std::unordered_map <MotorOutputName, MotorState>;
    
    PathResolver resolve_path;

    std::unordered_map <TopicInputName,  ros::Subscriber > subscribers;
    std::unordered_map <TopicOutputName, ros::Publisher > publishers;

    std::unordered_map <TopicInputName,  RelatedInputMotors> interdependence;
    std::unordered_map <TopicOutputName, RelatedOutputMotors> packeges; 


public:
    void start_node(ros::NodeHandle& hn, const std::vector<std::string>& controller_names);
    void spin();

private:    
    // callback вызывается когда получено новое сообщение от топика с именем 'topic_name'
    void callback(const TopicInputName& topic_name, const sensor_msgs::JointState::ConstPtr& msg);
    // push_message отправляет сообщения по топику 'topic_name', когда новых данных достаточно
    void push_message(const TopicOutputName& topic_name);
};
