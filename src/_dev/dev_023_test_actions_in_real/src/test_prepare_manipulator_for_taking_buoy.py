#!/usr/bin/env python3
import rospy

import actionlib

from ac_prepare_manipulator_for_take_buoy.msg import PrepareManipulatorForTakeBuoyAction
from ac_prepare_manipulator_for_take_buoy.msg import PrepareManipulatorForTakeBuoyGoal


def main():
    """Main entrypoint."""
    client = actionlib.SimpleActionClient(
        "/robot1/actions/prepare_manipulator_for_take_buoy/command",
        PrepareManipulatorForTakeBuoyAction,
    )

    print("wait_for_server")
    client.wait_for_server()

    print("send_goal")
    goal = PrepareManipulatorForTakeBuoyGoal()
    goal.buoy_pos.pose.position.x = 0.36
    goal.buoy_pos.pose.position.y = 0.00
    goal.buoy_pos.pose.position.z = 0.
    client.send_goal(goal)

    print("wait_for_result")
    client.wait_for_result()

    print(f"Result: {client.get_result()}")


if __name__ == "__main__":
    rospy.init_node("prepare_manipulator_for_taking_buoy")
    main()
