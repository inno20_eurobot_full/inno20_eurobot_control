#include <al_points2buoys/additional_classes/image.hpp>

Image Image::toImage(const PCLPoints::Ptr points, double step){return Image(points,step);}

Image::Image(const PCLPoints::Ptr points, double step)
{
    this->step = step;
    Edges<double> edges(points);
    int ysize = edges.ysize()/step + 1;
    int xsize = edges.xsize()/step + 1;
    shiftX = edges.xmin;
    shiftY = edges.ymin;

    image.resize(xsize, std::vector<bool> (ysize,false));

    for(const auto& point:*points){
        int x = (point.x - edges.xmin)/step;
        int y = (point.y - edges.ymin)/step;
        fill(x,y,1);
    }
}
Image::Image( int xsize, int ysize)
    :image(std::vector<std::vector<bool> > (xsize, std::vector<bool> (ysize,false)))
{}

Image Image::copySize(const Image& image){
    Image tempIm(image.xsize(), image.ysize());
    tempIm.shiftX = image.shiftX;
    tempIm.shiftY = image.shiftY;
    tempIm.step   = image.step;
    return tempIm;
}

std::vector<Image> Image::splitImage(const Image& image, int r) {
    std::vector<Image> image_arr;

    Image temp(image);
    while(temp.count()>0){
        // first point;
        Point init(0,0);
        for(init.x=0; init.x<temp.xsize(); init.x++){
            bool flag=false;
            for(init.y=0; init.y<temp.ysize(); init.y++)
                if(temp.get(init)){
                    flag =true;
                    break;
                }
            if(flag) break;
        }

        // connect closest points
        Image obj = copySize(temp);
        obj.fill(init,true);
        temp.fill(init,false);
        std::queue<Point> q;
        q.push(init);
        while(!q.empty()){
            Point point = q.front();q.pop();
            for(int x = point.x-r; x<=(point.x+r); x++)
                for(int y = point.y-r; y<=(point.y+r); y++){
                    if(x<0 || y<0)continue;
                    if(x>=temp.xsize() || y>=temp.ysize())continue;
                    if(temp.get(x,y)){
                        obj.fill(x,y,true);
                        temp.fill(x,y,false);
                        q.push(Point(x,y));
                    }
                }
        }

        // removing separate standing points
        if(obj.count()>15)
            image_arr.push_back(obj.compress());
    }
    return image_arr;
}

Image Image::compress() const {
    Edges<int> edges(*this);
    Image tempIm(edges.xsize()+1, edges.ysize()+1);
    tempIm.shiftX = this->shiftX + edges.xmin*this->step;
    tempIm.shiftY = this->shiftY + edges.ymin*this->step;
    tempIm.step   = this->step;

    // filling data
    for(int x = 0; x<=edges.xsize(); x++)
        for(int y = 0; y<=edges.ysize(); y++)
            tempIm.fill(x,y, get(x+edges.xmin, y+edges.ymin));
    return tempIm;    
}

Image::Edges<double> Image::getEdges() const {
    Edges<double> e;
    e.xmin = shiftX;
    e.xmax = shiftX + step*xsize();
    e.ymin = shiftY;
    e.ymax = shiftY + step*ysize();
    return e;
}

PCLPoints Image::getPointEdges() const {
    PCLPoints points;
    
    auto e = getEdges();

    for(double x= e.xmin; x<=e.xmax; x+= e.xsize()/25.){
        points.push_back(PointT(x,e.ymin,0));
        points.push_back(PointT(x,e.ymax,0));
    }
    for(double y= e.ymin; y<=e.ymax; y+= e.ysize()/25.){
        points.push_back(PointT(e.xmin,y,0));
        points.push_back(PointT(e.xmax,y,0));
    }
    return points;
}

void Image::fill(const Point& p, bool c){fill(p.x,p.y,c);}
void Image::fill(int x, int y, bool color){
    bool init = image[x][y];
    image[x][y] = color;
    _count += color?1:0;
    _count -= init?1:0;
}

bool Image::get(const Point& p) const {return get(p.x,p.y);}
bool Image::get(int x, int y) const {
    return image[x][y];
}

int Image::count() const{return _count;}

int Image::xsize() const{return image.size();}

int Image::ysize() const{
    if(xsize()==0)return 0;
    return image[0].size();
}

void Image::print() const{
    ROS_INFO("image(%d), %d x %d, shift: %f %f :",count(), xsize(), ysize(), shiftX, shiftY);
    for(const auto& line:image){
        std::string str = "";
        for(bool p:line){
            str+= (p?'*':' ');
            str+=' ';
        }
        ROS_INFO("%s",str.c_str());
    }
}