#pragma once

#include <ros/ros.h>
#include <behavior_tree/node_template/bt_condition_node.h>
#include <con_is_buoy_reachable/IsBuoyReachable.h>

#include <string>

class IsBuoyReachable: public BT::RosServiceNode<con_is_buoy_reachable::IsBuoyReachable>
{
public:
    IsBuoyReachable( ros::NodeHandle& handle, const std::string& node_name, const std::string& server_name, const BT::NodeConfiguration & conf)
        :RosServiceNode<con_is_buoy_reachable::IsBuoyReachable>(handle, node_name, server_name, conf) {}

    static BT::PortsList providedPorts();

    void sendRequest(RequestType& request) override;

    BT::NodeStatus onResponse(const ResponseType& rep) override;

    BT::NodeStatus onFailedRequest(RosServiceNode::FailureCause failure) override;
};
