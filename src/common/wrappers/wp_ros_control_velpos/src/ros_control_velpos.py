#!/usr/bin/env python3

"""Модуль предоставляет интерфес управления мотором и по позиции и по скорости одновременнно.

Принцип работы. Модуль подписывается на топики с состояниями моторов, на топики для
управления мотороми по скорости (предоставляются ros_control), а так же на топик с
управляющими командами. Как только на последний поступает команда, топик смотрит на
текущее положение мотора и его желаемое положение и используя PID регулятор корректирует
скорость, которая будет отправлена на мотор.
Если команды управления поступают реже чем 50 раз в секунду, то топк самостоятельно
пересчитывает скорость и отправляет ее.
"""

import sys
from collections import defaultdict, namedtuple

from typing import List

import rospy
from eurobot.core import PathResolver
from std_msgs.msg import Float64
from sensor_msgs.msg import JointState


AUTO_SEND_FREQUENCY = 50  # msg per second


ControllerType = namedtuple("ControllerType", ("controller_name", "input_name"))


class PID:
    """Class for simple PID controller."""

    def __init__(self, p, i, d, i_min, i_max, cmd_min=1, cmd_max=0):
        """Create PID controller."""
        self.p, self.i, self.d = p, i, d
        self.i_min, self.i_max = i_min, i_max
        self.cmd_min, self.cmd_max = cmd_min, cmd_max
        self.last_time = None

        self.last_u = None
        self.sum_u = 0

    def __call__(self, u):
        """Update and return control value."""
        c_time = rospy.get_rostime()

        res = self.p * u
        if self.last_time:
            dt = (c_time - self.last_time).to_sec()
            self.sum_u = min(max(self.sum_u + u * dt, self.i_min), self.i_max)
            res += self.i * self.sum_u
            if dt > 0:
                res += self.d * (u - self.last_u) / dt
        if self.cmd_min < self.cmd_max:
            res = min(max(res, self.cmd_min), self.cmd_max)

        self.last_time = c_time
        self.last_u = u
        return res


class VelPosControl:
    """Class responsible for evaluation and sending control commands."""

    def __init__(self, control_topic_name, input_name, pid, gear_mult, offset):
        """Create class with given pid controller and topic name."""
        self.position_pid = pid
        self.command_pos = None
        self.command_vel = None
        self.current_pos = None
        self.input_name = input_name
        self.gear_mult = gear_mult
        self.offset = offset
        self.output_topic = rospy.Publisher(control_topic_name, Float64, queue_size=1)

    def is_ok(self):
        """Is this class ready to publish messages?"""
        return (
            (self.command_pos is not None)
            and (self.command_vel is not None)
            and (self.current_pos is not None)
        )

    def push(self):
        """Compute and publish the message."""
        cmd_pos = self.command_pos * self.gear_mult + self.offset
        cmd_vel = self.command_vel * self.gear_mult
        res = cmd_vel + self.position_pid(cmd_pos - self.current_pos)
        self.output_topic.publish(res)


class RosControlVelpos:
    """Main class."""

    def __init__(self, controllers):
        """Create main class."""
        self.resolve_path = PathResolver()

        self.joint_states = defaultdict(list)
        self.controllers = {}  # dict of VelPosControl
        self.last_send_time = None  # Время последней отправики сообщения
        self.has_command = False

        for name in controllers:
            self.controllers[name] = self.create_controller(name)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.Subscriber("command", JointState, self.control_callback)

        def get_joint_state_callback(controllers):
            def wrapper(msg):
                self.joint_state_callback(controllers, msg)

            return wrapper

        for topic_name, controllers in self.joint_states.items():
            rospy.Subscriber(
                topic_name, JointState, get_joint_state_callback(controllers)
            )

    def create_controller(self, controller_name):
        """Create and return new instance of class VelPosControl."""
        if not rospy.has_param(controller_name + "/control_type"):
            rospy.logerr(
                "Parameter 'control_type' for controller '%s' not found. Skiping controller",
                controller_name,
            )
            return
        if not rospy.has_param(controller_name + "/control_topic"):
            rospy.logerr(
                "Parameter 'control_topic' for controller '%s' not found. Skiping controller",
                controller_name,
            )
            return
        if not rospy.has_param(controller_name + "/joint_state"):
            rospy.logerr(
                "Parameter 'joint_state' for controller '%s' not found. Skiping controller",
                controller_name,
            )
            return
        if not rospy.has_param(controller_name + "/position_pid"):
            rospy.logerr(
                "Parameter 'position_pid' for controller '%s' not found. Skiping controller",
                controller_name,
            )
            return
        if not rospy.has_param(controller_name + "/input_name"):
            rospy.logerr(
                "Parameter 'input_name' for controller '%s' not found. Skiping controller",
                controller_name,
            )
            return

        control_type = rospy.get_param(controller_name + "/control_type")
        control_topic = self.resolve_path(
            rospy.get_param(controller_name + "/control_topic")
        )
        joint_state = self.resolve_path(
            rospy.get_param(controller_name + "/joint_state")
        )
        joint_state_name = rospy.get_param(controller_name + "/joint_state_name")
        position_pid = rospy.get_param(controller_name + "/position_pid")
        input_name = rospy.get_param(controller_name + "/input_name")
        gear_mult = rospy.get_param(controller_name + "/gear_mult", 1)
        offset = rospy.get_param(controller_name + "/offset", 0)

        if control_type != "velocity":
            rospy.logerr(
                "Controller type 'velocity' for controller '%s' not yet implemented. Skiping controller",
                controller_name,
            )
            return

        self.joint_states[joint_state].append(
            ControllerType(controller_name, joint_state_name)
        )
        pid = PID(
            position_pid.get("p", 0),
            position_pid.get("i", 0),
            position_pid.get("d", 0),
            position_pid.get("imin", 0),
            position_pid.get("imax", 0),
            position_pid.get("cmdmin", 1),
            position_pid.get("cmdmax", 0),
        )

        return VelPosControl(control_topic, input_name, pid, gear_mult, offset)

    def process_velpos(self, must_send):
        """Tell all controllers to compute and publish control commands."""
        current_time = rospy.get_rostime().to_sec()
        if self.last_send_time:
            must_send |= (current_time - self.last_send_time) * AUTO_SEND_FREQUENCY >= 1
        if not must_send:
            return

        for controller in self.controllers.values():
            if controller.is_ok():
                controller.push()

        self.last_send_time = current_time

    def joint_state_callback(self, controllers: List[ControllerType], msg: JointState):
        """Callback for joint state messages."""
        input_name2controller = {i.input_name: self.controllers[i.controller_name] for i in controllers}

        for i, name in enumerate(msg.name):
            if name not in input_name2controller:
                continue
            controller = input_name2controller[name]
            controller.current_pos = msg.position[i]
        self.process_velpos(must_send=False)

    def control_callback(self, msg: JointState):
        """Callback for control topic."""
        N = len(msg.name)
        if len(msg.velocity) != N or len(msg.position) != N:
            rospy.logwarn("Incorrect control message. Ignoring")
            return

        input_name2controller = {v.input_name: v for k, v in self.controllers.items()}

        for i, name in enumerate(msg.name):
            if name not in input_name2controller:
                rospy.logwarn("Ignoring control command for unknown control '%s'", name)
                continue
            controller = input_name2controller[name]
            controller.command_pos = msg.position[i]
            controller.command_vel = msg.velocity[i]

        self.process_velpos(must_send=True)


def main():
    """Main entrypoint."""
    rospy.init_node("ros_control_velpos", anonymous=True)

    if len(sys.argv) <= 1:
        rospy.logwarn("No controllers specified for ros_control_velpos")

    controllers = rospy.myargv(argv=sys.argv)[1:]
    rospy.loginfo(
        "Start ros_control_velpos with controllers: %s", " ".join(controllers)
    )

    main = RosControlVelpos(controllers)
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
