#!/usr/bin/env python3
import rospy

import actionlib

from ac_manipulator_lighthouse_activation.msg import LighthouseActivationAction, LighthouseActivationGoal


def main():
    """Main entrypoint."""
    client = actionlib.SimpleActionClient(
        "/robot1/actions/manipulator_lighthouse_activation/command",
        LighthouseActivationAction,
    )

    print("wait_for_server")
    client.wait_for_server()

    print("send_goal")
    client.send_goal(LighthouseActivationGoal())

    print("wait_for_result")
    client.wait_for_result()

    print(f"Result: {client.get_result()}")


if __name__ == "__main__":
    rospy.init_node("test_manipulator_lighthouse_activation")
    main()
