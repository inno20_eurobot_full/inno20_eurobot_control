#!/usr/bin/env python
"""Ask Evgenii Maksimychev what this file about. Probablty for test planing."""

import rospy
import tf
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped, Pose
import numpy as np

pub = rospy.Publisher("/base/move_base/TebLocalPlannerROS/via_points", Path)
rospy.init_node("node_name")
while not rospy.is_shutdown():
    command = input("X, Y (m), Theta (deg); q for quit ").split(" ")  # there were raw_input
    if command[0] == "q":
        break
    path = Path()
    pose = PoseStamped()
    goal = Pose()
    goal.position.x = float(command[0])
    goal.position.y = float(command[1])

    quat = tf.transformations.quaternion_from_euler(
        0.0, 0.0, float(command[2]) * np.pi / 180
    )

    goal.orientation.x = quat[0]
    goal.orientation.y = quat[1]
    goal.orientation.z = quat[2]
    goal.orientation.w = quat[3]

    pose.pose = goal
    path.poses.append(pose)

    pub.publish(path)
