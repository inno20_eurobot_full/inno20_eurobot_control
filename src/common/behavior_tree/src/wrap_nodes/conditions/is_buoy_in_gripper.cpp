#include <behavior_tree/wrap_nodes/conditions/is_buoy_in_gripper.h>

BT::PortsList IsBuoyInGripper::providedPorts()
{
    return  { };
}

void IsBuoyInGripper::sendRequest(RequestType& request)
{

}

BT::NodeStatus IsBuoyInGripper::onResponse(const ResponseType& rep)
{
    if (rep.res == false)
        return BT::NodeStatus::FAILURE;
    return BT::NodeStatus::SUCCESS;
}

BT::NodeStatus IsBuoyInGripper::onFailedRequest(RosServiceNode::FailureCause failure)
{
    ROS_ERROR("IsBuoyInGripper request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}