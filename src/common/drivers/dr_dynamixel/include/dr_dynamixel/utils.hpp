#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include <regex>
#include <map>

#include "dynamixel.hpp"

template<typename T>
T getParam(const ros::NodeHandle& node, const std::string &key) {
    T val;
    if (!node.getParam(key, val)) {
        throw std::runtime_error("Wrong config format " + node.getNamespace() + "/" + key);
    }
    return val;
}

template<typename T>
T getParam(const ros::NodeHandle& node, const std::string &key, const T &def) {
    if (!node.hasParam(key)) {
        return def;
    }
    return getParam<T>(node, key);
}

template<>
DynamixelMode getParam(const ros::NodeHandle& node, const std::string &key) {
    const auto mode = getParam<std::string>(node, key);
    if (mode == "velpos") { return MODE_VELPOS; }
    if (mode == "effort") { return MODE_EFFORT; }
    throw std::runtime_error("Wrong config format " + node.getNamespace() + "/" + key);
}

template<>
std::map<uint16_t, std::vector<uint8_t>> getParam(const ros::NodeHandle& node, const std::string &key) {
    static const std::regex numRegex("^\\d+$"), hexRegex("^0[xX]([0-9a-fA-F]{2})+$");
    
    const auto data = getParam<std::map<std::string, std::string>>(node, key, {});
    std::map<uint16_t, std::vector<uint8_t>> config;

    for (auto j : data) {
        if (!std::regex_match(j.first, numRegex) || !std::regex_match(j.second, hexRegex))
            throw std::runtime_error("Wrong config format " + node.getNamespace() + "/" + key + "/" + j.first);
        const uint16_t address = std::stoul(j.first);
        for (int k = j.second.size() - 1; k >= 2; k -= 2)
            config[address].emplace_back(std::stoul(j.second.substr(k, k + 2), 0, 16));
    }

    return config;
}

template<typename T, std::size_t N>
std::array<T, N> getParamArray(const ros::NodeHandle& node, const std::string &key) {
    const auto vec = getParam<std::vector<T>>(node, key);
    if (vec.size() != N) {
        throw std::runtime_error("Wrong config format " + node.getNamespace() + "/" + key);
    }

    std::array<T, N> val{};
    std::copy(vec.begin(), vec.end(), val.begin());
    return val;
}

template<typename T, std::size_t N>
std::array<T, N> getParamArray(const ros::NodeHandle& node, const std::string &key, const std::array<T, N> &def) {
    const auto vec = getParam<std::vector<T>>(node, key, std::vector<T>(def.cbegin(), def.cend()));
    if (vec.size() != N) {
        throw std::runtime_error("Wrong config format " + node.getNamespace() + "/" + key);
    }

    std::array<T, N> val{};
    std::copy(vec.begin(), vec.end(), val.begin());
    return val;
}