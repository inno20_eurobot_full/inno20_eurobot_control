cmake_minimum_required(VERSION 3.0.2)
project(dr_fiducials)

find_package(catkin REQUIRED COMPONENTS rospy)

catkin_package()

catkin_install_python(PROGRAMS
  src/dr_fiducials/node.py
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
