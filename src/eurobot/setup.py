from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=['eurobot'],
    package_dir={'': 'src_py'}
)

setup(**d)