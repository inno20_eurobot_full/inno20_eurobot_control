#pragma once

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/PoseArray.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/fpfh.h>
#include <pcl/registration/ia_ransac.h>
#include <pcl/common/transforms.h>

#include <al_points2buoys/buoy_algorithm.hpp>

struct FeatureCloud
{
    explicit FeatureCloud(PCLPoints::Ptr points_);

    PCLPoints::Ptr points;
    SurfaceNormals::Ptr normals;
    LocalFeatures::Ptr features;
};

class IA_RANSAC : public BuoyAlgorithm
{
    // void start_node(ros::NodeHandle& nh) override;
private:
    geometry_msgs::PoseArray::Ptr findBouy(const PCLPoints::Ptr) override;

    vectorPCLpointsPtr splitPointCloud(const PCLPoints::Ptr) const;
    geometry_msgs::PoseArray::Ptr calculatePoses(const vectorPCLpointsPtr&) const;

};