#include <ct_manipulator_gripper/gripper_control.hpp>


int main(int argc, char** argv)
{
	// initialize ros
	ros::init(argc, argv, "gripper_control");

	ros::NodeHandle nh;

	// start program
	GripperControl main(nh);
	main.start_node(nh);
	main.spin();

	return 0;
}