
macro(my_demo_func)
    message("I was here")
endmacro()

# Генерация msg, srv и action.
# Не забудте добавить ${PROJECT_NAME}_generate_messages_cpp в зависимости цели, использующей эти сообщения
macro(eurobot_generate_messages)
    file(GLOB servises_list RELATIVE "${PROJECT_SOURCE_DIR}/srv" "srv/*.srv")
    file(GLOB messages_list RELATIVE "${PROJECT_SOURCE_DIR}/msg" "msg/*.msg")
    file(GLOB actions_list RELATIVE "${PROJECT_SOURCE_DIR}/action" "action/*.action")

    if(servises_list)
      add_service_files(FILES ${servises_list})
    endif()
    if(messages_list)
      add_message_files(FILES ${messages_list})
    endif()
    if(actions_list)
      add_action_files(DIRECTORY action FILES ${actions_list})
    endif()

    ## Generate added messages and services with any dependencies listed here
    if(servises_list OR messages_list OR actions_list)
      generate_messages(${ARGV})
    endif()
endmacro()