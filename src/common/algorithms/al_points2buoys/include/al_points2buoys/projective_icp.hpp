#pragma once

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/PoseArray.h>
#include <pcl/common/transforms.h>
#include <pcl/registration/registration.h>

#include <cmath>
#include <utility>

#include <al_points2buoys/buoy_algorithm.hpp>
#include <al_points2buoys/additional_classes/image.hpp>
#include <al_points2buoys/additional_classes/buoy_utils.hpp>

class ProjectiveICP : public BuoyAlgorithm
{ 
private:
    void start_node(ros::NodeHandle& nh) override;

    geometry_msgs::PoseArray::Ptr findBouy(const PCLPoints::Ptr) override;

    vectorPCLpointsPtr splitPointCloud(const PCLPoints::Ptr) const;
    geometry_msgs::PoseArray::Ptr calculatePoses(const vectorPCLpointsPtr&) const;

    void toPlane(PCLPoints::Ptr&) const;
    static double getMinHeight(const PCLPoints::Ptr);

    static void translatePoints(const PCLPoints& in, PCLPoints& out, const Eigen::Vector3d& v);
    static void rotatePoints   (const PCLPoints& in, PCLPoints& out, const Eigen::Matrix3d& m);

    static Eigen::Vector3d getAverage(const PCLPoints&);
    std::pair<PCLPoints::Ptr, PCLPoints::Ptr> getPair(const PCLPoints&, const Eigen::Vector3d&) const;
    static double calculateError(const PCLPoints&, const PCLPoints&);
    static std::pair<Eigen::Matrix3d, Eigen::Vector3d> makePCreg(const PCLPoints&, const PCLPoints&);

    // ros::Publisher border_pub;
    // ros::Publisher ReducedPoints;
    // ros::Publisher Init;
};