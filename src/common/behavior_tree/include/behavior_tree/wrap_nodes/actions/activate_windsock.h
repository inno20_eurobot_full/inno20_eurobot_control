#pragma once

#include <ros/ros.h>
#include <behavior_tree/node_template/bt_action_node.h>
#include <ac_activate_windsock/ActivateWindsockAction.h>

#include <string>

class ActivateWindsock : public BT::RosActionNode<ac_activate_windsock::ActivateWindsockAction>
{
public:
    ActivateWindsock( ros::NodeHandle& handle, const std::string& name, const std::string& server_name, const BT::NodeConfiguration & conf)
        :RosActionNode<ac_activate_windsock::ActivateWindsockAction>(handle, name, server_name, conf) {}

    static BT::PortsList providedPorts();

    bool sendGoal(GoalType& goal) override;

    BT::NodeStatus onResult( const ResultType& res) override;
    
    BT::NodeStatus onFailedRequest(FailureCause failure) override;
    
    void halt() override;
};
