#!/bin/bash

realpath() {  # for MAC OS
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

cd "$(dirname $(realpath $0))"
docker build -t markovvn1/ros_deploy_eurobot2021 .