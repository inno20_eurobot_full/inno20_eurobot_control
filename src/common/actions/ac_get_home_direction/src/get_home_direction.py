#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Куда ехать домой (на север или на юг)
(по факту, сервис цепляется к топику и ждёт, когда телефон определит и пришлет угол наклона флюгера.
Если его вызвали до получения сообщения - тогда HOME_UNKNOWN)
"""

import time
# import math
import rospy
from eurobot.core import PathResolver
from std_msgs.msg import Float32
from ac_get_home_direction.srv import (
    GetHomeDirection,
    GetHomeDirectionRequest,
    GetHomeDirectionResponse,
)


class GetHomeDirectionNode:
    """Main class."""

    NORTH_count = 0
    SOUTH_count = 0

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self.input_topic_name = self.resolve_path(rospy.get_param("input_topic"))
        self.quantity_value = rospy.get_param("quantity_value")
        self.skip_time = rospy.get_param("skip_time")

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.loginfo("Main for action 'get_home_direction' is loaded")
        self._sub = rospy.Subscriber(self.input_topic_name, Float32, self.update_place)
        self._server = rospy.Service("command", GetHomeDirection, self.server_callback)

        self._start_time = time.time_ns()

    def server_callback(self, req: GetHomeDirectionRequest) -> GetHomeDirectionResponse:
        """Callback for server."""
        res = GetHomeDirectionResponse()

        if (self.NORTH_count + self.SOUTH_count) < self.quantity_value:
            return res.HOME_UNKNOWN
        if self.SOUTH_count > self.NORTH_count:
            return res.HOME_SOUTH
        else:
            return res.HOME_NORTH

    def update_place(self, angle):
        """Callback for position of vane"""
        rospy.logdebug(f"SOUTH: {self.SOUTH_count} NORTH: {self.NORTH_count}")
        if time.time_ns() - self._start_time < (self.skip_time * 1e9):
            return

        dir_north = angle.data > 0

        self.NORTH_count += (dir_north) and (self.NORTH_count < self.quantity_value)
        self.SOUTH_count += (not dir_north) and (self.SOUTH_count < self.quantity_value)

        if (self.NORTH_count + self.SOUTH_count) > self.quantity_value:
            self.NORTH_count -= not dir_north
            self.SOUTH_count -= dir_north


def main():
    """Main entrypoint."""
    rospy.init_node("get_home_direction", anonymous=True)

    main = GetHomeDirectionNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
