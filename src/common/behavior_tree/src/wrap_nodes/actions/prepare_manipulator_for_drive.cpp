#include <behavior_tree/wrap_nodes/actions/prepare_manipulator_for_drive.h>

BT::PortsList PrepareManipulatorForDrive::providedPorts() {
    return {
        BT::InputPort<std::string>("case")
    };
}

bool PrepareManipulatorForDrive::sendGoal(GoalType& goal){
    auto value = getInput<std::string>("case").value();
   
    if( value == "WITHOUT_BUOY" ){
        goal.position = goal.POSITION_WITHOUT_BUOY;
        return true;
    }

    if( value == "WITH_BUOY" ){
        goal.position = goal.POSITION_WITH_BUOY;
        return true;
    }
    
    ROS_ERROR("PrepareManipulatorForDrive has incorrect port value '%s'", value.c_str());
    return false;
}

BT::NodeStatus PrepareManipulatorForDrive::onResult( const ResultType& res)
{
    if (res.res == true)
        return BT::NodeStatus::SUCCESS;
    return BT::NodeStatus::FAILURE;
}

BT::NodeStatus PrepareManipulatorForDrive::onFailedRequest(FailureCause failure)
{
    ROS_ERROR("PrepareManipulatorForDrive request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}

void PrepareManipulatorForDrive::halt()
{
    if( status() == BT::NodeStatus::RUNNING ) {
        ROS_WARN("PrepareManipulatorForDrive aborted(halted)");
        BaseClass::halt();
    }
}