#include <ros/ros.h>
#include <al_manipulator/ManipulatorAdvancedIK.h>
#include <al_manipulator/ManipulatorAdvancedIKRequest.h>
#include <al_manipulator/ManipulatorAdvancedIKResponse.h>
#include <string>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "is_buoy_reachable");
    ros::NodeHandle nh;
    ros::ServiceClient calc_advik = nh.serviceClient<al_manipulator::ManipulatorAdvancedIK>("/robot1/algorithms/manipulator_advik/calc_advik");

    auto req = al_manipulator::ManipulatorAdvancedIKRequest();
    auto res1 = al_manipulator::ManipulatorAdvancedIKResponse();
    auto res2 = al_manipulator::ManipulatorAdvancedIKResponse();
    for(double h_l= 0.084; h_l <= 0.085; h_l+=0.001){
        std::cout<<"h_l: "<<h_l<<std::endl;
        for(double h_h= 0.18; h_h <= 0.19; h_h+=0.001){
            // std::cout<<"\th_h: "<<h_h<<std::endl;
            std::cout<<"\th_h: "<<h_h<<" ";
            double max = -1;
            std::string text="~";
            for(req.pos.pitch = -0.06; req.pos.pitch <= 0.06; req.pos.pitch +=0.03){
                // std::cout<<"\t\tpitch: "<<req.pos.pitch <<":";
                double start =-1, end = -1;
                for(req.pos.x = 0.344; req.pos.x < 0.391; req.pos.x +=0.0005){
                    req.pos.z = h_l;
                    calc_advik.call(req,res1);
                    req.pos.z = h_h;
                    calc_advik.call(req,res2);
                    if(res1.res.size() > 0 &&  res2.res.size() > 0){
                        end = req.pos.x;
                        start = (start<0)?req.pos.x:start;
                    }
                }
                double r = end - start;
                if(r>max){
                    max = r;
                    text = ", pitch="+std::to_string(req.pos.pitch)+":  {"+std::to_string(start) +" - "+std::to_string(end)+"} = "+std::to_string(r);
                }
                // std::cout<<"  {"<<start<<" - "<<end<<"} = "<<r<<std::endl;
            }
            std::cout<<text<<std::endl;
        }
    }
    return 0;
}