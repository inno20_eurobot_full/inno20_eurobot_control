#pragma once

#include <tuple>
#include <utility>

///TODO 
/*
 *  функция zip объеденяет два контейнера в один, позволяя использовать for range по нескольким контейнерам сразу
 *  прим: 'for (auto& [a, b] : zip(containerA, containerB)) {}'
 * 
 *  Что можно улучшить:
 *      1) добавить возможность получать константные (rvalue) контейнеры
 *      2) сделать variadic для множества контейнеров
 */

template <typename T1, typename T2>
class enumeration {
        T1& a;
        T2& b;

    public:
        enumeration(T1& a_, T2& b_):
            a(a_), b(b_)
        {}

    private:
        struct iterator {
            typename T1::iterator it1;
            typename T2::iterator it2;

            iterator(const typename T1::iterator& it1_, const typename T2::iterator& it2_):
                it1(it1_), it2(it2_) {}

            iterator(const iterator& other):
                it1(other.it1), it2(other.it2) {}

            iterator& operator=(const iterator& other) {
                if (this != &other) {
                    it1 = other.it1;
                    it2 = other.it2;
                }

                return *this;
            }

            iterator& operator++() {
                ++it1, ++it2;
                return *this;
            }

            iterator operator++(int) {
                iterator tmp(*this);
                operator++();
                return tmp;
            }

            auto operator*() {
                return std::tuple{ std::move(*it1), std::move(*it2) };
            }

            bool operator==(const iterator& other) const {
                return (it1 == other.it1) && (it2 == other.it2);
            }

            bool operator!=(const iterator& other) const {
                return (it1 != other.it1) || (it2 != other.it2);
            }
        };

    public:
        iterator begin() {
            return { a.begin(), b.begin() };
        }

        iterator end() {
            return { a.end(), b.end() };
        }

        iterator begin() const {
            return { a.begin(), b.begin() };
        }

        iterator end() const {
            return { a.end(), b.end() };
        }
};


template <typename T1, typename T2>
auto zip(T1& a, T2& b) {
    return enumeration<T1, T2>{a, b};
}