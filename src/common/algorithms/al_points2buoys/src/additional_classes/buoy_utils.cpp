#include <al_points2buoys/additional_classes/buoy_utils.hpp>

#include <cmath>


// bool cross_aabb_buoy(double x, double y, double z, double a, double b, double c)
// {
// 	if (z >= 0 && z <= BUOY_H && x >= -BUOY_BIG_R && x <= BUOY_BIG_R && y >= -BUOY_BIG_R && y <= BUOY_BIG_R) return true;

// 	if (c != 0 && (z < 0 || z > BUOY_H))
// 	{
// 		double k = ((z < 0 ? 0 : BUOY_H)-z)/c;
// 		if (k >= 0)
// 		{
// 			double x0 = x + a * k, y0 = y + b * k;
// 			if (x0 >= -BUOY_BIG_R && x0 <= BUOY_BIG_R && y0 >= -BUOY_BIG_R && y0 <= BUOY_BIG_R) return true;
// 		}
// 	}

// 	if (b != 0 && (y < -BUOY_BIG_R || y > BUOY_BIG_R))
// 	{
// 		double k = ((y < 0 ? -BUOY_BIG_R : BUOY_BIG_R)-y)/b;
// 		if (k >= 0)
// 		{
// 			double x0 = x + a * k, z0 = z + c * k;
// 			if (x0 >= -BUOY_BIG_R && x0 <= BUOY_BIG_R && z0 >= 0 && z0 <= BUOY_H) return true;
// 		}
// 	}

// 	if (a != 0 && (x < -BUOY_BIG_R || x > BUOY_BIG_R))
// 	{
// 		double k = ((x < 0 ? -BUOY_BIG_R : BUOY_BIG_R)-x)/a;
// 		if (k >= 0)
// 		{
// 			double y0 = y + b * k, z0 = z + c * k;
// 			if (y0 >= -BUOY_BIG_R && y0 <= BUOY_BIG_R && z0 >= 0 && z0 <= BUOY_H) return true;
// 		}
// 	}

// 	return false;
// }

// Пересечение с нижней крышкой
double find_candidate1(double x, double y, double z, double a, double b, double c)
{
	if (c == 0) return -1;
	double res = - z / c;
	if (res < 0) return -1;
	double x_c = x + res * a;
	double y_c = y + res * b;
	if (x_c * x_c + y_c * y_c > BUOY_SMALL_R * BUOY_SMALL_R) return -1;
	return res;
}

// Пересечение с основной частью
double find_candidate2(double x, double y, double z, double a, double b, double c)
{
	constexpr double k = (BUOY_BIG_R - BUOY_SMALL_R) / BUOY_H;

	// A * res * res + B * res + C = 0
	double A = a * a + b * b - c * c * k * k;
	double B = 2 * a * x + 2 * b * y - 2 * BUOY_SMALL_R * k * c - 2 * k * k * z * c;
	double C = x * x + y * y - BUOY_SMALL_R * BUOY_SMALL_R - 2 * BUOY_SMALL_R * k * z - k * k * z * z;

	if (4 * A * C > B * B) return -1;
	double d = sqrt(B * B - 4 * A * C);
	double r1 = (-B - d) / 2 / A;
	double r2 = (-B + d) / 2 / A;

	double z1 = z + r1 * c;
	if (0 < z1 && z1 < BUOY_H) return r1;

	double z2 = z + r2 * c;
	if (0 < z2 && z2 < BUOY_H) return r2;
	return -1;
}


double find_nearest_buoy_point(double x, double y, double z, double a, double b, double c)
{
	// Почти не ускоряет
	// if (!cross_aabb_buoy(x, y, z, a, b, c)) return -1;

	double r1 = find_candidate1(x, y, z, a, b, c);
	double r2 = find_candidate2(x, y, z, a, b, c);

	if (r1 < 0) return r2;
	if (r2 < 0) return r1;
	return r1 < r2 ? r1 : r2;
}