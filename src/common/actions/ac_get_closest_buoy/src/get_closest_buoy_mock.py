#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Возвращает координаты бижайшего буйка, если он есть, используя данные с realsense
"""

import rospy
from eurobot.core import PathResolver

from ac_get_closest_buoy.srv import GetClosestBuoy, GetClosestBuoyRequest, GetClosestBuoyResponse


class GetClosestBuoyNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self._server = rospy.Service("command", GetClosestBuoy, self.server_callback)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.logwarn("Mock for action 'get_closest_buoy' is loaded")

    def server_callback(self, req: GetClosestBuoyRequest) -> GetClosestBuoyResponse:
        """Callback for server."""
        return GetClosestBuoyResponse(done=False)


def main():
    """Main entrypoint."""
    rospy.init_node("get_closest_buoy", anonymous=True)

    main = GetClosestBuoyNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
