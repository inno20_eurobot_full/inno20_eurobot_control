#!/usr/bin/env python3

"""Модуль обёртка

смотрит трансформацию аруки относительно поля,
вычисляет угол поворота в радианах из данных и пересылает в выходной топик.
"""


import rospy
import tf2_ros
from tf.transformations import euler_from_quaternion
from eurobot.core import PathResolver
from std_msgs.msg import Float32


class MonitorWeatherVane:
    """Main class."""

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self.source_frame = rospy.get_param("source_frame")
        self.target_frame = rospy.get_param("target_frame")
        self.output_name = "angle"

        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.logdebug("Wrapper 'monitor_weather_vane' is loaded")
        self.pub = rospy.Publisher(self.output_name, Float32, queue_size=1)

    def spin(self):
        """Update angle position spin."""
        rate = rospy.Rate(3)
        while not rospy.is_shutdown():
            rate.sleep()
            try:
                trans = self.tfBuffer.lookup_transform(
                    self.target_frame,
                    self.source_frame,
                    rospy.Time.now(),
                    rospy.Duration(1),
                )
            except (
                tf2_ros.LookupException,
                tf2_ros.ConnectivityException,
                tf2_ros.ExtrapolationException,
            ):
                rospy.logwarn(
                    "Weather Vane: tf2 has no transform\n"
                )  # logdebug после проверки на реальном роботе
                continue
            quat = trans.transform.rotation
            angles = euler_from_quaternion([quat.x, quat.y, quat.z, quat.w])
            rospy.logdebug(f"angles: x {angles[0]} y {angles[1]} z {angles[2]}")
            self.pub.publish(angles[0])


def main():
    """Main entrypoint."""
    rospy.init_node("monitor_weather_vane", anonymous=True)

    main = MonitorWeatherVane()
    main.start()

    main.spin()


if __name__ == "__main__":
    main()
