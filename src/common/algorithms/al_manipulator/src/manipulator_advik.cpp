#include <al_manipulator/manipulator_advik.hpp>

bool ManipulatorAdvIK::service_callback(al_manipulator::ManipulatorAdvancedIK::Request& req, al_manipulator::ManipulatorAdvancedIK::Response& res)
{
	return process(req.pos, res.res);
}

bool ManipulatorAdvIK::start_service(ros::NodeHandle& nh)
{
	service = nh.advertiseService("calc_advik", &ManipulatorAdvIK::service_callback, this);
	return true;
}

bool ManipulatorAdvIK::process(const al_manipulator::ManipulatorEndPos& pos, std::vector<sensor_msgs::JointState>& Resstates)
{
	Resstates.clear();
	
	// расчет матрицы H
	Matrix H = ManipulatorH::getMatrixH(pos, base_height, dh_params);
	al_manipulator::ManipulatorTF Htf;
	for(int i=0; i<16; i++)
		Htf.data[i] = H.get_data()[i];

	// расчет всех стейтов
	std::vector < sensor_msgs::JointState> states;
	bool correct = ManipulatorIK::process(Htf,states);
	if( !correct ) return false;

	if(states.size() != 2 && states.size() != 4){
		ROS_WARN("out of reach of the manipulator");
		return true;
	}

	// отсеевание позиций выходящих за ограничения
	for(const auto& sol: states){
		bool error = false;
		for(int i=0; i< sol.position.size(); i++){
			error |= (sol.position[i] < constrains[i].min);
			error |= (sol.position[i] > constrains[i].max);
			if(error)break;
		}
		if(!error)
			Resstates.push_back(sol);
	}
	if(Resstates.empty())
		ROS_WARN("Error: all solutions are rejected by the constraints");

	return true;
}

bool ManipulatorAdvIK::loadConstrains(const ros::NodeHandle& nh)
{
    std::vector<std::string> motor_names;
	if (!nh.hasParam("motor_names")){
		ROS_FATAL("Parameter 'motor_names' not found.");
		return false;
	}
	nh.getParam("motor_names", motor_names);
	
	std::string motors_param_names;
	if (!nh.hasParam("motors_param")){
		ROS_FATAL("Parameter 'motors_param' not found.");
		return false;
	}
	nh.getParam("motors_param", motors_param_names);
	motors_param_names = resolve_path(motors_param_names);
	
	for(const std::string& motor: motor_names){
		std::string constr1 = join_paths(motors_param_names,motor,"min_angle");
		std::string constr2 = join_paths(motors_param_names,motor,"max_angle");
		if (!nh.hasParam(constr1) || !nh.hasParam(constr2)){
			ROS_FATAL(" %s Parameter '%s' or '%s' not found.",motors_param_names.c_str(), constr1.c_str(), constr2.c_str());
			return false;
		}
		Constrain constrain;
		nh.getParam(constr1, constrain.min);
		nh.getParam(constr2, constrain.max);
		constrains.push_back(constrain);
	}

	return true;
}


bool ManipulatorAdvIK::start_node(ros::NodeHandle& nh)
{
	// get home_path param
	resolve_path.init(nh);
	std::string base_height_name;
	if (!nh.hasParam("path_to_base_height")) {
		ROS_FATAL("Parameter 'path_to_base_height' not found.");
		return false;
	}
	nh.getParam("path_to_base_height", base_height_name);

	base_height_name = resolve_path(base_height_name);
	if (!nh.hasParam(base_height_name)) {
		ROS_FATAL("Parameter '%s' not found.",base_height_name.c_str());
		return false;
	}
	nh.getParam(base_height_name, base_height);

	if(!loadConstrains(nh)) return false;

	return ManipulatorIK::start_node(nh);
}