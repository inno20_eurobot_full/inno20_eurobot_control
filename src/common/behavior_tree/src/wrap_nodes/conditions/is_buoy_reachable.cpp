#include <behavior_tree/wrap_nodes/conditions/is_buoy_reachable.h>
#include <geometry_msgs/PoseStamped.h>

BT::PortsList IsBuoyReachable::providedPorts()
{
    return {
        BT::InputPort<geometry_msgs::PoseStamped>("pos")
    };
}

void IsBuoyReachable::sendRequest(RequestType& request)
{
    auto value = getInput<geometry_msgs::PoseStamped>("pos");
    if (!value)
        ROS_ERROR("IsBuoyReachable has incorrect port value ");

    request.pos = value.value();
}

BT::NodeStatus IsBuoyReachable::onResponse(const ResponseType& rep)
{
    if (rep.res == false)
        return BT::NodeStatus::FAILURE;
    return BT::NodeStatus::SUCCESS;
}

BT::NodeStatus IsBuoyReachable::onFailedRequest(RosServiceNode::FailureCause failure)
{
    ROS_ERROR("IsBuoyReachable request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}