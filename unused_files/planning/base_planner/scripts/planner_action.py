#!/usr/bin/env python
"""RosAction client to test the local planner"""

import rospy
import tf
import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from actionlib_msgs.msg import GoalStatusArray
from time import sleep
import numpy as np


def movebase_client():
    """Function creates a client and sets the goal"""
    client = actionlib.SimpleActionClient("/base/move_base", MoveBaseAction)
    print("Waiting for server")
    client.wait_for_server()
    command = " "
    while True:
        command = input("X, Y (m), Theta (deg); q for quit ").split(" ")  # there were raw_input
        if command[0] == "q":
            return
        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = "map"
        goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.pose.position.x = float(command[0])
        goal.target_pose.pose.position.y = float(command[1])

        quat = tf.transformations.quaternion_from_euler(
            0.0, 0.0, float(command[2]) * np.pi / 180
        )

        goal.target_pose.pose.orientation.x = quat[0]
        goal.target_pose.pose.orientation.y = quat[1]
        goal.target_pose.pose.orientation.z = quat[2]
        goal.target_pose.pose.orientation.w = quat[3]

        client.send_goal(goal)
        print("Goal sent")
        sleep(1)
        status = rospy.wait_for_message("/base/move_base/status", GoalStatusArray)
        print(status.status_list)
        while len(status.status_list) == 0 or status.status_list[-1].status != 3:
            status = rospy.wait_for_message("/base/move_base/status", GoalStatusArray)
            sleep(0.01)
    return client.get_result()


if __name__ == "__main__":
    try:
        rospy.init_node("movebase_client_py")
        result = movebase_client()
        if result:
            rospy.loginfo("Goal execution done!")
    except rospy.ROSInterruptException:
        rospy.loginfo("Navigation test finished.")
