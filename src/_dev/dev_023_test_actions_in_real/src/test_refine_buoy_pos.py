#!/usr/bin/env python3
import rospy

from ac_refine_buoy_pos.srv import RefineBuoyPos
from geometry_msgs.msg import PoseStamped


def main():
    """Main entrypoint."""
    client = rospy.ServiceProxy(
        "/robot1/actions/refine_buoy_pos/command",
        RefineBuoyPos
    )
    print("send_goal")
    goal = PoseStamped()
    goal.header.frame_id = "map"
    goal.pose.position.x = 1.36
    goal.pose.position.y = 1.5
    goal.pose.position.z = 0.
    result = client(goal)

    print(f"Result: {result}")


if __name__ == "__main__":
    rospy.init_node("test_refine_buoy_pos")
    main()
