#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Изменить состояние флага (поднять его или опустить)
"""

import time
import rospy
from eurobot.core import PathResolver

import actionlib
import ac_flag_command.msg


class FlagCommand:
    """Wrapper for action."""
    Action = ac_flag_command.msg.FlagCommandAction
    Feedback = ac_flag_command.msg.FlagCommandFeedback
    Goal = ac_flag_command.msg.FlagCommandGoal
    Result = ac_flag_command.msg.FlagCommandResult


class FlagCommandNode:
    """Main class."""

    _result = FlagCommand.Result()

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self._action_server = actionlib.SimpleActionServer(
            "command",
            FlagCommand.Action,
            execute_cb=self.action_callback,
            auto_start=False)
        self._action_server.start()

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.logwarn("Mock for action 'flag_command' is loaded")

    def action_callback(self, goal: FlagCommand.Goal):
        """Callback for action."""
        time.sleep(4)
        self._result.res = True
        self._action_server.set_succeeded(self._result)


def main():
    """Main entrypoint."""
    rospy.init_node("flag_command", anonymous=True)

    main = FlagCommandNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
