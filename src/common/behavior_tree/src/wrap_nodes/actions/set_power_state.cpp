#include <behavior_tree/wrap_nodes/actions/set_power_state.h>

BT::PortsList SetPowerState::providedPorts() {
    return {
        BT::InputPort<std::string>("base_state"),
        BT::InputPort<std::string>("manip_state")
    };
}

void SetPowerState::sendRequest(RequestType& request)
{
    auto base_value = getInput<std::string>("base_state").value();
    auto manip_value = getInput<std::string>("manip_state").value();

    if( base_value == "ON" && manip_value == "ON"){
        request.base_state = request.STATE_POWER_ON;
        request.manip_state = request.STATE_POWER_ON;
        return;
    }

    if( base_value == "OFF" && manip_value == "OFF" ){
        request.base_state = request.STATE_POWER_OFF;
        request.manip_state = request.STATE_POWER_OFF;
        return;
    }   

    if( base_value == "ON" && manip_value == "OFF"){
        request.base_state = request.STATE_POWER_ON;
        request.manip_state = request.STATE_POWER_OFF;
        return;
    }   


    if( base_value == "OFF" && manip_value == "ON"){
        request.base_state = request.STATE_POWER_OFF;
        request.manip_state = request.STATE_POWER_ON;
        return;
    }   

    ROS_ERROR("SetPowerState has incorrect port value ");
}

BT::NodeStatus SetPowerState::onResponse(const ResponseType& rep)
{
    if (rep.res == false)
        return BT::NodeStatus::FAILURE;
    return BT::NodeStatus::SUCCESS;
}


BT::NodeStatus SetPowerState::onFailedRequest(FailureCause failure)
{
    ROS_ERROR("SetPowerState request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}
