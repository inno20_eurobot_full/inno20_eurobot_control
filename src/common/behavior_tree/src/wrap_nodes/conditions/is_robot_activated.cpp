#include <behavior_tree/wrap_nodes/conditions/is_robot_activated.h>

BT::PortsList IsRobotActivated::providedPorts()
{
    return  { };
}

void IsRobotActivated::sendRequest(RequestType& request)
{

}

BT::NodeStatus IsRobotActivated::onResponse(const ResponseType& rep)
{
    if (rep.res == false)
        return BT::NodeStatus::FAILURE;
    return BT::NodeStatus::SUCCESS;
}

BT::NodeStatus IsRobotActivated::onFailedRequest(RosServiceNode::FailureCause failure)
{
    ROS_ERROR("IsRobotActivated request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}