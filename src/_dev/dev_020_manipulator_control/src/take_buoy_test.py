#!/usr/bin/env python3
import rospy
import sys

# Brings in the SimpleActionClient
import actionlib

# goal message and the result message.
import ac_take_buoy.msg
from geometry_msgs.msg import Pose


def feedback_processing(fb):
    """Функция для обработки сигнала Обратной связи (Action Feadback)"""
    print("Server feedback:", fb)


def example_client(name, index):
    """example_client"""
    client = actionlib.SimpleActionClient(
        "/robot1/actions/take_buoy/take_buoy",
        ac_take_buoy.msg.TakeBuoyAction,
    )

    print(name, ": wait for server")
    client.wait_for_server()  # wait until the action server has been started and is listening for goals

    # creates a goal to send to the action server
    q = [[0.27, 0.21, 0]  # 0
         ]
    pose = Pose()
    # pose.position = q[index]
    pose.position.x = q[index][0]
    pose.position.y = q[index][1]
    pose.position.z = q[index][2]

    print(name, ": connecting")
    client.send_goal(
        ac_take_buoy.msg.TakeBuoyGoal(pose), feedback_cb=feedback_processing
    )
    client.wait_for_result()  # wait for server to finish the action
    return client.get_result()  # print out the result


if __name__ == "__main__":
    name = "take_bouy_test"
    rospy.init_node(name)
    index = int(sys.argv[1]) if len(sys.argv) > 1 else 0
    result = example_client(name, index)
    print(name, ": result = ", result.done)
