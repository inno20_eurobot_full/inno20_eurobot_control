#include <behavior_tree/wrap_nodes/actions/lighthouse_command.h>

BT::PortsList LighthouseCommand::providedPorts()
{
    return {
        BT::InputPort<std::string>("command"),
        BT::InputPort<bool>("use_motor", true, "false - only led spin")
    };
}

void LighthouseCommand::sendRequest(RequestType& request)
{
    auto value = getInput<std::string>("command").value(); 
    bool use_motor = getInput<bool>("use_motor").value(); 

    request.use_motor = use_motor;

    if( value == "RAISE" ){
        request.cmd = request.RAISE;
        return;
    }

    if( value == "LOWER" ){
        request.cmd = request.LOWER;
        return;
    }

    ROS_WARN("LighthouseCommand has incorrect port value '%s'", value.c_str());
}

BT::NodeStatus LighthouseCommand::onResponse(const ResponseType& rep)
{
    if (rep.res == false)
        return BT::NodeStatus::FAILURE;
    return BT::NodeStatus::SUCCESS;
}

BT::NodeStatus LighthouseCommand::onFailedRequest(RosServiceNode::FailureCause failure)
{
    ROS_ERROR("LighthouseCommand request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}