#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Включить/выключить питание робота (по правилу мы должны отключить питание моторов по окончании игры)
"""

import rospy
from eurobot.core import PathResolver

from ac_set_power_state.srv import SetPowerState, SetPowerStateRequest, SetPowerStateResponse
from dr_maxon_base.srv import SetPower as SetPower_maxon, SetPowerRequest as SetPower_maxon_req
from dr_dynamixel.srv import SetPower as SetPower_dynam, SetPowerRequest as SetPower_dynam_req


class SetPowerStateNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        resolve_path = PathResolver()
        maxon_base_name = resolve_path(rospy.get_param("maxon_base"))
        dynamixel_1_name = resolve_path(rospy.get_param("dynamixel_1"))
        dynamixel_2_name = resolve_path(rospy.get_param("dynamixel_2"))
        self.maxon_base = rospy.ServiceProxy(maxon_base_name, SetPower_maxon)
        self.dynamixel_1 = rospy.ServiceProxy(dynamixel_1_name, SetPower_dynam)
        self.dynamixel_2 = rospy.ServiceProxy(dynamixel_2_name, SetPower_dynam)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.loginfo("Main for action 'set_power_state' is loaded")
        self.maxon_base.wait_for_service()
        self.dynamixel_1.wait_for_service()
        self.dynamixel_2.wait_for_service()
        self._server = rospy.Service("command", SetPowerState, self.server_callback)

    def server_callback(self, req: SetPowerStateRequest) -> SetPowerStateResponse:
        """Callback for server."""
        res = True

        if req.base_state == req.STATE_POWER_ON:
            self.maxon_base(SetPower_maxon_req.POWER_ON)
        elif req.base_state == req.STATE_POWER_OFF:
            self.maxon_base(SetPower_dynam_req.POWER_OFF)
        else:
            rospy.logwarn(f"Set power state: Incorrect command '{req}'")
            res = False

        if req.manip_state == req.STATE_POWER_ON:
            res &= self.dynamixel_1(SetPower_maxon_req.POWER_ON).success
            res &= self.dynamixel_2(SetPower_maxon_req.POWER_ON).success
        elif req.manip_state == req.STATE_POWER_OFF:
            res &= self.dynamixel_1(SetPower_dynam_req.POWER_OFF).success
            res &= self.dynamixel_2(SetPower_dynam_req.POWER_OFF).success
        else:
            rospy.logwarn(f"Set power state: Incorrect command '{req}'")
            res = False

        return SetPowerStateResponse(res)


def main():
    """Main entrypoint."""
    rospy.init_node("set_power_state", anonymous=True)

    main = SetPowerStateNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
