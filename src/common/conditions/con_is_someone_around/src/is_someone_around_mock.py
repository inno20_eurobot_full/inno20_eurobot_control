#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Есть ли вражеский робот рядом?
"""

import rospy
from eurobot.core import PathResolver

from con_is_someone_around.srv import (
    IsSomeoneAround,
    IsSomeoneAroundRequest,
    IsSomeoneAroundResponse,
)


class IsSomeoneAroundNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.logwarn("Mock for condition 'is_someone_around' is loaded")
        self._server = rospy.Service("command", IsSomeoneAround, self.server_callback)

    def server_callback(self, req: IsSomeoneAroundRequest) -> IsSomeoneAroundResponse:
        """Callback for server."""
        return IsSomeoneAroundResponse(False)


def main():
    """Main entrypoint."""
    rospy.init_node("is_someone_around", anonymous=True)

    main = IsSomeoneAroundNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
