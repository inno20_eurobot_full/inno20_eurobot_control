#pragma once

#include <ros/ros.h>

#include <std_msgs/Float32.h>
#include <sensor_msgs/JointState.h>

#include <string>

#include <eurobot/core/path_utils.hpp>


class SimpleGripperWrapper
{    
    PathResolver resolve_path;

    ros::Subscriber sub;
    ros::Publisher pub;
    
    // ros parameters
    std::string pub_topic;
    std::string motor_name;
    double gear_mult;
    
public:
    explicit SimpleGripperWrapper(ros::NodeHandle& nh);
    ~SimpleGripperWrapper();

    void takeParameters(ros::NodeHandle& nh);

    void goalCallback(std_msgs::Float32 effort);
 
    void start_node(ros::NodeHandle& hn);
    void spin();
    
    template<typename T>
    static void check_param(ros::NodeHandle& nh, 
                        const char * param_name, 
                        const T& required_param_value);
};

/*
 * checking that the '%param_name' is equal '%required_param_value'
 */
template<typename T>
void SimpleGripperWrapper::check_param(ros::NodeHandle& nh, 
                                const char * param_name, 
                                const T& required_param_value)
{
    bool flag;

    flag = nh.hasParam(param_name);
	ROS_ASSERT_MSG (flag, "Parameter '%s' not found", param_name);

	T param_value;
	nh.getParam(param_name, param_value);

    flag = (param_value == required_param_value);
    std::string output_req(required_param_value); 
    std::string output_curr(param_value); 
	ROS_ASSERT_MSG (flag,
            "Parameter '%s' is not equal to '%s'. Actual value is '%s'.",
            param_name,
            output_req.c_str(),
            output_curr.c_str());
}
