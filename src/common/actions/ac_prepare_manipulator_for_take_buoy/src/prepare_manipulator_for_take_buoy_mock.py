#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Подготовить манипулятор к захвату буйка или его установки (пододвинуть манипулятор так,
чтобы он смог захватить буй или поставить его)
"""

import time
import rospy
from eurobot.core import PathResolver

import actionlib
import ac_prepare_manipulator_for_take_buoy.msg


class PrepareManipulatorForTakeBuoy:
    """Wrapper for action."""
    Action = ac_prepare_manipulator_for_take_buoy.msg.PrepareManipulatorForTakeBuoyAction
    Feedback = ac_prepare_manipulator_for_take_buoy.msg.PrepareManipulatorForTakeBuoyFeedback
    Goal = ac_prepare_manipulator_for_take_buoy.msg.PrepareManipulatorForTakeBuoyGoal
    Result = ac_prepare_manipulator_for_take_buoy.msg.PrepareManipulatorForTakeBuoyResult


class PrepareManipulatorForTakeBuoyNode:
    """Main class."""

    _result = PrepareManipulatorForTakeBuoy.Result()

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self._action_server = actionlib.SimpleActionServer(
            "command",
            PrepareManipulatorForTakeBuoy.Action,
            execute_cb=self.action_callback,
            auto_start=False)
        self._action_server.start()

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.logwarn("Mock for action 'prepare_manipulator_for_take_buoy' is loaded")

    def action_callback(self, goal: PrepareManipulatorForTakeBuoy.Goal):
        """Callback for action."""
        time.sleep(4)
        self._result.res = True
        self._action_server.set_succeeded(self._result)


def main():
    """Main entrypoint."""
    rospy.init_node("prepare_manipulator_for_take_buoy", anonymous=True)

    main = PrepareManipulatorForTakeBuoyNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
