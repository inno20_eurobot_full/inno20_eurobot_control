#include <behavior_tree/wrap_nodes/conditions/is_someone_around.h>

BT::PortsList IsSomeoneAround::providedPorts()
{
    return {
        BT::InputPort<double>("dist")
    };
}

void IsSomeoneAround::sendRequest(RequestType& request)
{
    auto value = getInput<double>("dist");
    if (!value)
        ROS_ERROR("IsSomeoneAround has incorrect port value ");

    request.dist = value.value();
}

BT::NodeStatus IsSomeoneAround::onResponse(const ResponseType& rep)
{
    if (rep.res == false)
        return BT::NodeStatus::FAILURE;
    return BT::NodeStatus::SUCCESS;
}

BT::NodeStatus IsSomeoneAround::onFailedRequest(RosServiceNode::FailureCause failure)
{
    ROS_ERROR("IsSomeoneAround request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}