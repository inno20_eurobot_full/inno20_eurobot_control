#pragma once

#include <ros/ros.h>

#include <vector>
#include <string>
#include <queue>

#include <al_points2buoys/buoy_algorithm.hpp>

class Image{
    struct Point{
        explicit Point(int x=0, int y=0):x(x),y(y){}
        int x,y;
    };
    template<class T> struct Edges;

    std::vector< std::vector<bool> > image;
    int _count=0;
    double step;
    double shiftX, shiftY;

    int count() const;
    int xsize() const;
    int ysize() const;
    void fill(const Point& p, bool);
    void fill(int x, int y, bool c);
    bool get(const Point& p) const;
    bool get(int x, int y) const;


    Image( int xsize, int ysize);
    void toPlane(PCLPoints::Ptr&);
    static Image copySize(const Image&);
    Image compress() const;

public:
    static Image toImage(const PCLPoints::Ptr points, double step=0.005);
    explicit Image(const PCLPoints::Ptr points, double step=0.005);

    static std::vector<Image> splitImage(const Image& image, int r=1);

    Edges<double> getEdges() const;

    PCLPoints getPointEdges() const;
    void print() const;
};

template<class T>
struct Image::Edges{
    Edges():xmax(-1),xmin(-1),ymax(-1),ymin(-1){}

    explicit Edges(const PCLPoints::Ptr cloud)
        :Edges()
    {
        ROS_ASSERT_CMD(cloud->size() > 0, return);
        xmax = xmin = cloud->at(0).x;
        ymax = ymin = cloud->at(0).y;
        for(const auto& point:*cloud){
            if(point.x>xmax) xmax=point.x;
            if(point.x<xmin) xmin=point.x;
            if(point.y>ymax) ymax=point.y;
            if(point.y<ymin) ymin=point.y;
        }
    }
    explicit Edges(const Image& img)
        :Edges()
    {
        bool first = true;
        for(int x=0;x<img.xsize();x++)
            for(int y=0;y<img.ysize();y++){
                if(!img.get(x,y))continue;
                if(first){
                    first = false;
                    xmin = xmax = x;
                    ymin = ymax = y;
                }
                if(x > xmax) xmax=x;
                if(x < xmin) xmin=x;
                if(y > ymax) ymax=y;
                if(y < ymin) ymin=y;
            }
    }
    T xsize(){return xmax-xmin;}
    T ysize(){return ymax-ymin;}
    T xmin,xmax;
    T ymin,ymax;
};