#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Изменить состояние гриппера (закрыть гриппер или открыть его)
"""

import time
import rospy
from eurobot.core import PathResolver

import actionlib
import ac_gripper_command.msg


class GripperCommand:
    """Wrapper for action."""
    Action = ac_gripper_command.msg.GripperCommandAction
    Feedback = ac_gripper_command.msg.GripperCommandFeedback
    Goal = ac_gripper_command.msg.GripperCommandGoal
    Result = ac_gripper_command.msg.GripperCommandResult


class GripperCommandNode:
    """Main class."""

    _result = GripperCommand.Result()

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self._action_server = actionlib.SimpleActionServer(
            "command",
            GripperCommand.Action,
            execute_cb=self.action_callback,
            auto_start=False)
        self._action_server.start()

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.logwarn("Mock for action 'gripper_command' is loaded")

    def action_callback(self, goal: GripperCommand.Goal):
        """Callback for action."""
        time.sleep(4)
        self._result.res = True
        self._action_server.set_succeeded(self._result)


def main():
    """Main entrypoint."""
    rospy.init_node("gripper_command", anonymous=True)

    main = GripperCommandNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
