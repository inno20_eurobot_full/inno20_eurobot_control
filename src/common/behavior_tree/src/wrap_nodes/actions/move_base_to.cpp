#include <behavior_tree/wrap_nodes/actions/move_base_to.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Quaternion.h>

geometry_msgs::Quaternion quatFromYaw(double yaw)
{
    geometry_msgs::Quaternion output;
    output.x = 0;
    output.y = 0;
    output.z = sin(yaw * 0.5);
    output.w = cos(yaw * 0.5);
    return output;
}

BT::PortsList MoveBaseTo::providedPorts() {
    return {
        BT::InputPort<std::string>("frame"),
        BT::InputPort<double>     ("pos_x"),
        BT::InputPort<double>     ("pos_y"),
        BT::InputPort<double>     ("angle")
    };
}

bool MoveBaseTo::sendGoal(GoalType& goal){
    geometry_msgs::PoseStamped value;

    double yaw;
    bool success = true;    
    success &= static_cast<bool>(getInput("frame", value.header.frame_id ));
    success &= static_cast<bool>(getInput("pos_x", value.pose.position.x ));
    success &= static_cast<bool>(getInput("pos_y", value.pose.position.y ));
    success &= static_cast<bool>(getInput("angle", yaw));
    if( !success ){
        ROS_WARN("MoveBaseTo has incorrect port value");
        return false;
    }
    value.pose.orientation = quatFromYaw(yaw);
    value.header.stamp = ros::Time::now();
    value.pose.position.z = 0;

    goal.pos = value;
    return true;
}
    

BT::NodeStatus MoveBaseTo::onResult( const ResultType& res)
{
    if (res.res == true)
        return BT::NodeStatus::SUCCESS;
    return BT::NodeStatus::FAILURE;
}


BT::NodeStatus MoveBaseTo::onFailedRequest(FailureCause failure)
{
    ROS_ERROR("MoveBaseTo request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}

void MoveBaseTo::halt()
{
    if( status() == BT::NodeStatus::RUNNING ) {
        ROS_WARN("MoveBaseTo aborted(halted)");
        BaseClass::halt();
    }
}