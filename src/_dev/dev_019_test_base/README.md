# Dualshock 4 to maxon testing node

Node that connects to dualshock 4 via joy node and sends control to maxon driver. Tested with gamepad connected via bluetooth. With some OS configurations or connection types it may not work because of wrong controller buttons mapping (for example when special driver is installed or connected via usb)

Usage: `roslaunch dev_019_test_base start.launch`

### **WARNING!**
Robot starts driving on half of speed when started. To fix you must press accelerator buttons once. RUN ROBOT WITH WHEELS IN THE AIR.

## Controls
- Right shoulder (R1): enable driving (drive when pressed)
- Left shoulder (L1): enable turbo boost
- Left stick: twist move
