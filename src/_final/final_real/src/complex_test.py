#!/usr/bin/env python3
"""RosAction client to test the local planner"""

import rospy
import tf2_ros

from ac_set_power_state.srv import SetPowerState, SetPowerStateRequest as SetReq

import requests


class GeneralTest:
    """Main class."""

    frames = ["base_link", "fiducial_42"]
    fiducials = [
        "fiducial_1",
        "fiducial_2",
        "fiducial_3",
        "fiducial_4",
        "fiducial_5",
        "fiducial_6",
        "fiducial_7",
        "fiducial_8",
        "fiducial_9",
        "fiducial_10",
    ]

    def __init__(self):
        """Doc str"""
        self.tfBuffer = tf2_ros.Buffer()
        self.list = tf2_ros.TransformListener(self.tfBuffer)

        self.ip = rospy.get_param("robot1/driver/lighthouse_1/ip")
        self.port = rospy.get_param("robot1/driver/lighthouse_1/port")
        self.client = rospy.ServiceProxy(
            "/robot1/actions/set_power_state/command", SetPowerState
        )

    def test_main_frames(self) -> bool:
        """Doc str"""
        for frame in self.frames:
            try:
                self.tfBuffer.lookup_transform("map", frame, rospy.Time(0))
                rospy.loginfo(
                    f"complex_test: transform from 'map' to '{frame}' is exist"
                )
                return True
            except (
                tf2_ros.LookupException,
                tf2_ros.ConnectivityException,
                tf2_ros.ExtrapolationException,
            ):
                rospy.logwarn(f"complex_test: no transform from 'map' to '{frame}'")
                return False

    def test_fiducials_frames(self) -> bool:
        """Doc str"""
        msg = "complex_test: fiducials:"
        have = False
        for frame in self.fiducials:
            try:
                trans = self.tfBuffer.lookup_transform("map", frame, rospy.Time(0))
                if trans.header.stamp < rospy.Time().now() - rospy.Duration(1):
                    continue
                msg += f"{frame},"
                have = True
            except (
                tf2_ros.LookupException,
                tf2_ros.ConnectivityException,
                tf2_ros.ExtrapolationException,
            ):
                continue
        if have:
            rospy.loginfo(msg)
            return True
        else:
            rospy.logwarn(msg)
            return False

    def test_lighthouse(self) -> bool:
        """Doc str"""
        rospy.loginfo(
            f"complex_test: trying to connect to lighthouse({self.ip}:{self.port})"
        )
        try:
            r = requests.post(
                f"http://{self.ip}:{self.port}/task?command=up&use_motor=false",
                timeout=10,
            )
            r.raise_for_status()
            rospy.loginfo("complex_test: lighthouse connected")
            return True

        except requests.exceptions.HTTPError:
            rospy.logwarn("complex_test: no connection to lighthouse 'HTTPError'")
        except requests.exceptions.Timeout:
            rospy.logwarn(
                "complex_test: no connection to lighthouse 'request timeout'"
            )
        except requests.exceptions.TooManyRedirects:
            rospy.logwarn("complex_test: no connection to lighthouse 'bad URL'")
        except requests.exceptions.RequestException:
            rospy.logwarn("complex_test: no connection to lighthouse 'error'")
        return False

    def activate_robot(self) -> bool:
        """Doc str"""
        rospy.loginfo("complex_test: trying to activate motor power")
        try:
            self.client.wait_for_service(timeout=5)
            self.client(SetReq.STATE_POWER_ON)
            rospy.loginfo("complex_test: activate motor power")
            return True

        except Exception:
            rospy.logwarn("complex_test: activate motor not available")
        return False

    def diactivate_all(self) -> bool:
        """Doc str"""
        rospy.loginfo("complex_test: wait 10 seconds")
        rospy.sleep(rospy.Duration(10))
        rospy.loginfo("complex_test: trying to diactivate all")
        try:
            requests.post(
                f"http://{self.ip}:{self.port}/task?command=down&use_motor=false",
                timeout=5,
            )
            self.client(SetReq.STATE_POWER_OFF)
            rospy.loginfo("complex_test: diactivation correct")
            return True
        except Exception:
            pass
        rospy.logwarn("complex_test: diactivation incorrect")
        return False

    def start(self):
        """Doc str"""
        count = 0
        count += self.test_main_frames()
        count += self.test_fiducials_frames()
        count += self.activate_robot()
        count += self.test_lighthouse()
        count += self.diactivate_all()
        rospy.loginfo(f"finish test: {count}/5")


if __name__ == "__main__":
    rospy.init_node("complex_test")
    main = GeneralTest()
    main.start()
