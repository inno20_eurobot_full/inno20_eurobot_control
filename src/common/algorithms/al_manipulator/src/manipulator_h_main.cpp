#include <al_manipulator/manipulator_h.hpp>

int main(int argc, char** argv)
{
	// initialize ros
	ros::init(argc, argv, "manipulator_h");

	ros::NodeHandle nh;

	// start program
	ManipulatorH main;
	try{
		main.start_node(nh);
	} catch(char const* error) {
		ROS_FATAL("%s", error);
		return -1;
	} catch( ... ){
		ROS_FATAL("Unkonwn error");
		return -1;
	}
	main.spin();

	return 0;
}