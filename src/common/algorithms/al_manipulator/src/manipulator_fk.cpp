#include <al_manipulator/manipulator_fk.hpp>

#include <map>

#include <eurobot/core/path_utils.hpp>

/*
	Manual test:
	input: [0.2, -0.1, 0.9, 0.2, 1.0]
	output:
		array([[ 0.801  0.278  0.529  0.341]
			   [-0.389  0.915  0.107  0.054]
			   [-0.455 -0.292  0.842  0.209]
			   [ 0.     0.     0.     1.   ]])
*/


ManipulatorFKStep::ManipulatorFKStep(DHParam p)
{
	transform = AffineMatrix4x4::TranslateXYZ(p.distance_a, 0, p.distance_d) * AffineMatrix4x4::RotateX(p.angle_alpha);
	this->angle_theta_shift = p.angle_theta_shift;
}

Matrix ManipulatorFKStep::get(double angle_theta)
{
	return AffineMatrix4x4::RotateZ(angle_theta + angle_theta_shift) * transform;
}



void ManipulatorFK::topic_callback(const sensor_msgs::JointState::ConstPtr& msg)
{
	al_manipulator::ManipulatorTF res;
	process(*msg, res);
	output_topic.publish(res);
}

bool ManipulatorFK::service_callback(al_manipulator::ManipulatorFK::Request& req, al_manipulator::ManipulatorFK::Response& res)
{
	return process(req.state, res.res);
}

bool ManipulatorFK::load_dh_params(const ros::NodeHandle& nh, DHParams* dh_params)
{
	if (!nh.hasParam("path_to_dh_params"))
	{
		ROS_FATAL("Parameter 'path_to_dh_params' not found.");
		return false;
	}
	std::string dh_params_ns;
	nh.getParam("path_to_dh_params", dh_params_ns);
	dh_params_ns = resolve_path(dh_params_ns);

	if (!DHParams::load_from_ROS_params(nh, dh_params_ns, dh_params))
	{
		ROS_FATAL("Error while reading DH params. Probably incorrect format. Exit");
		return false;
	}
	if (dh_params->size() == 0)
	{
		ROS_FATAL("DH params has zero length. Probably it is not what you want. Exit");
		return false;
	}

	return true;
}

bool ManipulatorFK::build_manipulator(const DHParams& dh)
{
	if (motor_names.size() != dh.size())
	{
		ROS_FATAL("Size of parameter 'motor_names' must be equal to size of DH-params.");
		return false;
	}

	for (const DHParam& d : dh)
		steps.push_back(ManipulatorFKStep(d));

	return true;
}

bool ManipulatorFK::start_topics(ros::NodeHandle& nh)
{
	if (nh.hasParam("use_topic/subscribe_to"))
	{
		std::string input_topic_name;
		nh.getParam("use_topic/subscribe_to", input_topic_name);
		input_topic = nh.subscribe(resolve_path(input_topic_name), 1, &ManipulatorFK::topic_callback, this);
		output_topic = nh.advertise<al_manipulator::ManipulatorTF>("end_effector_state", 1);
		return true;
	}
	else
	{
		ROS_ERROR("Param use_topic/subscribe_to not found. Topic will not work");
		return false;
	}
}

bool ManipulatorFK::start_service(ros::NodeHandle& nh)
{
	service = nh.advertiseService("calc_fk", &ManipulatorFK::service_callback, this);
	return true;
}

bool ManipulatorFK::process(const sensor_msgs::JointState& joint_state, al_manipulator::ManipulatorTF& res)
{
	if (joint_state.name.size() != joint_state.position.size())
	{
		ROS_ERROR("joint_state.name.size() != joint_state.position.size(). Skip message");
		return false;
	}

	std::map<std::string, double> angle_theta;
	for (int i = 0; i < joint_state.name.size(); i++)
		angle_theta[joint_state.name[i]] = joint_state.position[i];

	Matrix m = Matrix::eye(4);
	for (int i = 0; i < motor_names.size(); i++)
	{
		if (angle_theta.count(motor_names[i]) == 0)
		{
			ROS_ERROR("Motor with name '%s' not found in msg. Skip message", motor_names[i].c_str());
			return false;
		}
		m *= steps[i].get(angle_theta[motor_names[i]]);
	}

	for (int y = 0; y < 4; y++)
		for (int x = 0; x < 4; x++)
			res.data[4 * y + x] = m.at(y, x);
	return true;
}

bool ManipulatorFK::start_node(ros::NodeHandle& nh)
{
	// get home_path param
	resolve_path.init(nh);

	if (!nh.hasParam("motor_names"))
	{
		ROS_FATAL("Parameter 'motor_names' not found.");
		return false;
	}
	nh.getParam("motor_names", motor_names);

	bool use_service = false, use_topic = false;

	if (nh.hasParam("use_service/active")) nh.getParam("use_service/active", use_service);
	if (nh.hasParam("use_topic/active")) nh.getParam("use_topic/active", use_topic);

	if (!use_topic && !use_service)
	{
		ROS_WARN("Do not use topic and do not use service. Probably you forgot to load params. Nothing to do");
		return false;
	}


	DHParams dh_params;
	if (!load_dh_params(nh, &dh_params)) return false;
	if (!build_manipulator(dh_params)) return false;

	if (use_topic) start_topics(nh);
	if (use_service) start_service(nh);

	return true;
}

void ManipulatorFK::spin()
{
	ros::spin();
}