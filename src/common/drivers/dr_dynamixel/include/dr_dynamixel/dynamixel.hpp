#pragma once

#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <dr_dynamixel/SetPower.h>

#include <dynamixel_sdk/dynamixel_sdk.h>

#include <mutex>
#include <optional>
#include <string>
#include <vector>
#include <array>
#include <thread>
#include <atomic>
#include <tuple>
#include <map>

enum DynamixelMode {
    MODE_VELPOS,
    MODE_EFFORT,
};

struct Dynamixel {
    DynamixelMode mode;
    int id;
    std::array<double, 2> constrains;
    std::array<double, 3> pid;
    std::array<double, 2> integralLimits;
    std::map<uint16_t, std::vector<uint8_t>> config;

    std::atomic<double> goalVel, goalPos, goalPwm;
    std::atomic_bool hasInput = false;

    double integral = 0.0;
    std::optional<double> pos, prevPos, vel, rawGoal;
};

class DynamixelDriver {
    dynamixel::PortHandler *portHandler;
    dynamixel::PacketHandler *packetHandler;
    std::thread worker_thread;

    std::map<std::string, Dynamixel> motors;
    std::mutex motors_mutex;

    std::atomic_bool enabled = false;
    std::atomic_bool running;

    ros::Subscriber sub;
    ros::Publisher pub;
    ros::ServiceServer srv;

    void worker();
    
    bool configureMotors();
    bool verifyModes();
    bool enableMotors(bool enable);

    void goalCallback(const sensor_msgs::JointStateConstPtr &goal);

    void syncReadPosition(dynamixel::GroupSyncRead &posRead);
    void syncReadVelocity(dynamixel::GroupSyncRead &velRead);
    void syncWriteVelocity(dynamixel::GroupSyncWrite &velWrite);
    void syncWriteEffort(dynamixel::GroupSyncWrite &pwmWrite);

    void approximateVelocity(double dt);
    void ensureInitialGoal();
    void calculateControl(double dt);
    void publishJointState();

    bool setPower(dr_dynamixel::SetPower::Request &req, dr_dynamixel::SetPower::Response &res);

public:
    DynamixelDriver(ros::NodeHandle& node, const std::vector<std::string>& motor_names);
    ~DynamixelDriver();

    bool start();
    bool stop();
};