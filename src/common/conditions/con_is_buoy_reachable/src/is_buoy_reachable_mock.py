#!/usr/bin/env python3

"""Модуль высокоуровневого контроля робота

Сможет ли манипулятор взять стаканчик не двигая базой (дотянется ли?)
"""

import rospy
from eurobot.core import PathResolver

from con_is_buoy_reachable.srv import IsBuoyReachable, IsBuoyReachableRequest, IsBuoyReachableResponse


class IsBuoyReachableNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self._server = rospy.Service("command", IsBuoyReachable, self.server_callback)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.logwarn("Mock for condition 'is_buoy_reachable' is loaded")

    def server_callback(self, req: IsBuoyReachableRequest) -> IsBuoyReachableResponse:
        """Callback for server."""
        return IsBuoyReachableResponse(True)


def main():
    """Main entrypoint."""
    rospy.init_node("is_buoy_reachable", anonymous=True)

    main = IsBuoyReachableNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
