# Maxon driver

Actuator for robot base with maxon epos4 motors

### Subscribes to
- `/cmd_vel` [`geometry_msgs/Twist`]: cmd_vel to drive robot. Sends update to epos4 controller on every message, use carefull. Seems that it works ok with 200 updates per second.

### Publishes to
- `/odom` [`nav_msgs/Odometry`]: robot base odometry calculated from encoders data `rate` times per second (by default 500)
