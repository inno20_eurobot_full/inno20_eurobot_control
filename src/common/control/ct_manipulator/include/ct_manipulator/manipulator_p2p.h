#pragma once

#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <actionlib/server/simple_action_server.h>
#include <ct_manipulator/ManipulatorP2PAction.h>

#include <string>
#include <vector>
#include <memory>
#include <unordered_map>

#include <eurobot/core/path_utils.hpp>



struct MotorControl
{
    std::string control_name;
    std::string joint_states_name;
    double min_angle, max_angle;
    double max_vel, max_accel;
    double current_pos;
    bool is_received = false;
    // double current_vel;
};

struct Variable
{
    double T; 	    // time duration
    double L; 	    // displacement
    double Ta; 	    // acceleration time
    double maxA;    // max acceleration
    double maxV;    // max velocity
    double endP;    // finish position
    double initP;   // start position
    std::string motor_name;
};

class ManipulatorP2P
{
    PathResolver resolve_path;

    typedef actionlib::SimpleActionServer<ct_manipulator::ManipulatorP2PAction> ActionServerType;
    double RATE;
    double EPSILON;
    double TIME_LIMIT;

    std::unordered_map<std::string, MotorControl> motors;
    ros::Publisher control_topic;
    ros::Subscriber joint_states_topic;
    std::shared_ptr<ActionServerType> action_server;
    // create messages that are used to published feedback/result
    ct_manipulator::ManipulatorP2PFeedback action_feedback;
    ct_manipulator::ManipulatorP2PResult action_result;

public:
    void start_node(ros::NodeHandle& hn, const std::vector<std::string>& motor_list);
    void spin();

private:
    void create_motor(ros::NodeHandle& nh, const std::string& motor_name);
    bool set_variables(std::vector<Variable> &motor_vars, const std::string &name, double goal_pos);
    void action_callback(const ct_manipulator::ManipulatorP2PGoalConstPtr& goal);
    void joint_states_callback(const sensor_msgs::JointState::ConstPtr& msg);

};